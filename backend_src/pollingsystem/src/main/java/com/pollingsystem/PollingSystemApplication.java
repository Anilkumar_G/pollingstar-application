package com.pollingsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * This class consists of main method
 * @author poll star
 *
 */
@SpringBootApplication
public class PollingSystemApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(PollingSystemApplication.class, args);
	}
}	