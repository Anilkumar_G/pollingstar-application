package com.pollingsystem.dto;

import java.util.Date;

/**
 * This class is used to store answer response to return response object
 * 
 * @author IMVIZAG
 *
 */
public class AnswerResponse {

	// declaring the variables
	private int answerId;

	private String answer;

	private Date date;

	private int noOfVotes;

	private int id;

	private String username;
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the answerId
	 */
	public int getAnswerId() {
		return answerId;
	}

	/**
	 * @param answerId the answerId to set
	 */
	public void setAnswerId(int answerId) {
		this.answerId = answerId;
	}

	/**
	 * @return the answer
	 */
	public String getAnswer() {
		return answer;
	}

	/**
	 * @param answer the answer to set
	 */
	public void setAnswer(String answer) {
		this.answer = answer;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the noOfVotes
	 */
	public int getNoOfVotes() {
		return noOfVotes;
	}

	/**
	 * @param noOfVotes the noOfVotes to set
	 */
	public void setNoOfVotes(int noOfVotes) {
		this.noOfVotes = noOfVotes;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
