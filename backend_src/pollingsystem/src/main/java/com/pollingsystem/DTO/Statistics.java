package com.pollingsystem.dto;


public class Statistics {
	

	private long value;
	private String label;
	
	/**
	 * @return the value
	 */
	public long getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(long value) {
		this.value = value;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @param value
	 * @param label
	 */
	public Statistics(String label,long value ) {
		super();
		this.value = value;
		this.label = label;
	}
	

	
}
