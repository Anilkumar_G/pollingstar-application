package com.pollingsystem.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

/**
 * This class is used to store question entity which creates question schema in
 * database
 * 
 * @author IMVIZAG
 *
 */
@Entity
@Table(name = "questions_tbl")
public class Question {

	// declaring the columns in table
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int question_id;
	@Column(length=500)
	private String question;
	@Column
	private int numberOfVotes;
	@Column
	private Date dateOfCreation;
	
	@OneToMany(mappedBy="question",cascade=CascadeType.ALL)
	private List<Voting> voting;

	@OneToMany(mappedBy = "question",orphanRemoval=true)
	private List<Voting> voting;

	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false)
	private UserDetails userDetails;

<<<<<<< HEAD
	
=======
	@OneToMany(mappedBy = "question",orphanRemoval=true)
	@OrderBy("noOfVotes DESC")
	private List<AnswerEntity> answers;
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e

	@Column
	private String category;

	/**
	 * @return the question_id
	 */
	public int getQuestion_id() {
		return question_id;
	}

	/**
	 * @param question_id the question_id to set
	 */
	public void setQuestion_id(int question_id) {
		this.question_id = question_id;
	}

	/**
	 * @return the question
	 */
	public String getQuestion() {
		return question;
	}

	/**
	 * @param question the question to set
	 */
	public void setQuestion(String question) {
		this.question = question;
	}

	/**
	 * @return the numberOfVotes
	 */
	public int getNumberOfVotes() {
		return numberOfVotes;
	}

	/**
	 * @param numberOfVotes the numberOfVotes to set
	 */
	public void setNumberOfVotes(int numberOfVotes) {
		this.numberOfVotes = numberOfVotes;
	}

	/**
	 * @return the dateOfCreation
	 */
	public Date getDateOfCreation() {
		return dateOfCreation;
	}

	/**
	 * @param dateOfCreation the dateOfCreation to set
	 */
	public void setDateOfCreation(Date dateOfCreation) {
		this.dateOfCreation = dateOfCreation;
	}

	/**
	 * @return the voting
	 */
	public List<Voting> getVoting() {
		return voting;
	}

	/**
	 * @param voting the voting to set
	 */
	public void setVoting(List<Voting> voting) {
		this.voting = voting;
	}

	/**
	 * @return the userDetails
	 */
	public UserDetails getUserDetails() {
		return userDetails;
	}

	/**
	 * @param userDetails the userDetails to set
	 */
	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}

<<<<<<< HEAD
	public List<Voting> getVoting() {
		return voting;
	}

	public void setVoting(List<Voting> voting) {
		this.voting = voting;
=======
	/**
	 * @return the answers
	 */
	public List<AnswerEntity> getAnswers() {
		return answers;
	}

	/**
	 * @param answers the answers to set
	 */
	public void setAnswers(List<AnswerEntity> answers) {
		this.answers = answers;
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

}
