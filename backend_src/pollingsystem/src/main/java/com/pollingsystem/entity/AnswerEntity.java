package com.pollingsystem.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

<<<<<<< HEAD


=======
/**
 * This class is used to store answer entity which creates answer schema in
 * database
 * 
 * @author IMVIZAG
 *
 */
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
@Entity
@Table(name = "answer_tbl")
public class AnswerEntity {

	// declaring the columns of table
	@Id
	@Column(name = "answer_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int answerId;

	@Column(name = "answer ", nullable = false)
	private String answer;

	@Column
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "date", nullable = false)
	private Date date;

	@Column(name = "no_of_votes", nullable = false)

	private int noOfVotes;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private UserDetails userDetails;

	@ManyToOne
	@JoinColumn(name = "question_id")
	private Question question;
<<<<<<< HEAD
	
	@OneToMany(mappedBy="answer",cascade=CascadeType.ALL)
	private List<Voting> voting;
	
	public UserDetails getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public List<Voting> getVoting() {
		return voting;
	}

	public void setVoting(List<Voting> voting) {
		this.voting = voting;
	}
=======

	@OneToMany(mappedBy = "answer" ,orphanRemoval=true)
	private List<Voting> voting;
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e

	/**
	 * @return the answerId
	 */
	public int getAnswerId() {
		return answerId;
	}

	/**
	 * @param answerId the answerId to set
	 */
	public void setAnswerId(int answerId) {
		this.answerId = answerId;
	}

	/**
	 * @return the answer
	 */
	public String getAnswer() {
		return answer;
	}

	/**
	 * @param answer the answer to set
	 */
	public void setAnswer(String answer) {
		this.answer = answer;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the noOfVotes
	 */
	public int getNoOfVotes() {
		return noOfVotes;
	}

	/**
	 * @param noOfVotes the noOfVotes to set
	 */
	public void setNoOfVotes(int noOfVotes) {
		this.noOfVotes = noOfVotes;
	}

	/**
	 * @return the userDetails
	 */
	public UserDetails getUserDetails() {
		return userDetails;
	}

	/**
	 * @param userDetails the userDetails to set
	 */
	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}

	/**
	 * @return the question
	 */
	public Question getQuestion() {
		return question;
	}

	/**
	 * @param question the question to set
	 */
	public void setQuestion(Question question) {
		this.question = question;
	}

	/**
	 * @return the voting
	 */
	public List<Voting> getVoting() {
		return voting;
	}

	/**
	 * @param voting the voting to set
	 */
	public void setVoting(List<Voting> voting) {
		this.voting = voting;
	}

}
