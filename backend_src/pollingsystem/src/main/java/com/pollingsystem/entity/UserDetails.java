package com.pollingsystem.entity;


import java.sql.Date;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

/**
 * This class is used to store user entity which creates user schema in database
 * 
 * @author IMVIZAG
 *
 */
@Entity
@Table(name = "user_tbl")
public class UserDetails {

	// declaring columns in table
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int user_id;
	@Column
	private String first_name;
	@Column
	private String last_name;
	@Column(unique = true)
	private String email;
	@Column
	private String password;
<<<<<<< HEAD
	private String oldPassword;
	
=======

	private String oldPassword;
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e

	@Column
	private String phone;
	@Column
	private String city;
	@Column
	private String state;
	@Column
	private String country;
	@Column
	private String religion;
	@Column
	private String securityQuestion;
	@Column
	private String securityAnswer;
	@Column
	private Date dateOfRegistration;
	@Column(nullable = false)
	private String gender;

	@OneToMany(mappedBy = "userDetails",orphanRemoval=true)
	private List<Question> Question_details;

	@OneToMany(mappedBy = "userDetails",orphanRemoval=true)
	private List<AnswerEntity> answer_details;

	@OneToMany(mappedBy = "userdetails",orphanRemoval=true)
	private List<Voting> voting;
	
	@ColumnDefault("0")
	private int loginCount;
	
<<<<<<< HEAD
	@OneToMany(mappedBy="userdetails",cascade=CascadeType.ALL)
	private List<Voting> voting;
=======
	@Column
	private java.util.Date lastAttempt;
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
	


	

	/**
	 * @return the lastAttempt
	 */
	public java.util.Date getLastAttempt() {
		return lastAttempt;
	}

	/**
	 * @param lastAttempt the lastAttempt to set
	 */
	public void setLastAttempt(java.util.Date lastAttempt) {
		this.lastAttempt = lastAttempt;
	}

	/**
	 * @return the loginCount
	 */
	public int getLoginCount() {
		return loginCount;
	}

	/**
	 * @param loginCount the loginCount to set
	 */
	public void setLoginCount(int loginCount) {
		this.loginCount = loginCount;
	}

	/**
	 * @return the user_id
	 */
	public int getUser_id() {
		return user_id;
	}

	/**
	 * @param user_id the user_id to set
	 */
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	/**
	 * @return the first_name
	 */
	public String getFirst_name() {
		return first_name;
	}

	/**
	 * @param first_name the first_name to set
	 */
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	/**
	 * @return the last_name
	 */
	public String getLast_name() {
		return last_name;
	}

	/**
	 * @param last_name the last_name to set
	 */
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the oldPassword
	 */
	public String getOldPassword() {
		return oldPassword;
	}

	/**
	 * @param oldPassword the oldPassword to set
	 */
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the religion
	 */
	public String getReligion() {
		return religion;
	}

	/**
	 * @param religion the religion to set
	 */
	public void setReligion(String religion) {
		this.religion = religion;
	}

	/**
	 * @return the securityQuestion
	 */
	public String getSecurityQuestion() {
		return securityQuestion;
	}

	/**
	 * @param securityQuestion the securityQuestion to set
	 */
	public void setSecurityQuestion(String securityQuestion) {
		this.securityQuestion = securityQuestion;
	}

	/**
	 * @return the securityAnswer
	 */
	public String getSecurityAnswer() {
		return securityAnswer;
	}

	/**
	 * @param securityAnswer the securityAnswer to set
	 */
	public void setSecurityAnswer(String securityAnswer) {
		this.securityAnswer = securityAnswer;
	}

	/**
	 * @return the dateOfRegistration
	 */
	public Date getDateOfRegistration() {
		return dateOfRegistration;
	}

	/**
	 * @param dateOfRegistration the dateOfRegistration to set
	 */
	public void setDateOfRegistration(Date dateOfRegistration) {
		this.dateOfRegistration = dateOfRegistration;
	}

	
	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the question_details
	 */
	public List<Question> getQuestion_details() {
		return Question_details;
	}

	/**
	 * @param question_details the question_details to set
	 */
	public void setQuestion_details(List<Question> question_details) {
		Question_details = question_details;
	}

	/**
	 * @return the answer_details
	 */
	public List<AnswerEntity> getAnswer_details() {
		return answer_details;
	}

	/**
	 * @param answer_details the answer_details to set
	 */
	public void setAnswer_details(List<AnswerEntity> answer_details) {
		this.answer_details = answer_details;
	}

<<<<<<< HEAD
=======
	/**
	 * @return the voting
	 */
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
	public List<Voting> getVoting() {
		return voting;
	}

<<<<<<< HEAD
=======
	/**
	 * @param voting the voting to set
	 */
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
	public void setVoting(List<Voting> voting) {
		this.voting = voting;
	}

<<<<<<< HEAD
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	
=======
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
}
