package com.pollingsystem.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.pollingsystem.entity.AnswerEntity;
import com.pollingsystem.entity.Question;

/**
 * This class is used as a DAO which extends crud repository
 * 
 * @author IMVIZAG
 *
 */
public interface AnswerDao extends CrudRepository<AnswerEntity, Integer> {

	@Query("from AnswerEntity where id=:id and question.question_id=:question_id")
	AnswerEntity findId(int id, int question_id);
	
	List<AnswerEntity> findByQuestion(Question question);
}
