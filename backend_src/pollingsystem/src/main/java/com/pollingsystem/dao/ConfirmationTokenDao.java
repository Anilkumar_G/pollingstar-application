package com.pollingsystem.dao;
import org.springframework.data.repository.CrudRepository;

import com.pollingsystem.entity.ConfirmationToken;

public interface ConfirmationTokenDao extends CrudRepository<ConfirmationToken, Long> {  
    ConfirmationToken findByConfirmationToken(String confirmationToken);
    ConfirmationToken findByEmailIgnoreCase(String Email);
}
