package com.pollingsystem.dao;

import java.util.List;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.pollingsystem.dto.Statistics;
import com.pollingsystem.entity.UserDetails;

/**
 * This class is used as a DAO which extends crud repository
 * 
 * @author IMVIZAG
 *
 */
@Repository
public interface UserDao extends CrudRepository<UserDetails, Integer> {
	@Query("SELECT new com.pollingsystem.dto.Statistics(v.city,COUNT(city)) FROM " +
			"UserDetails v where v.user_id IN(select u.userdetails.user_id from Voting u where u.question.question_id=:question_id)" +
			"GROUP BY v.city")
	List<Statistics> getStatsBycity(int question_id);

	@Query("SELECT new com.pollingsystem.dto.Statistics(v.state,COUNT(state)) FROM " +
			"UserDetails v where v.user_id IN(select u.userdetails.user_id from Voting u where u.question.question_id=:question_id)" +
			"GROUP BY v.state")
	List<Statistics> getStatsByState(int question_id);
	
	@Query("SELECT new com.pollingsystem.dto.Statistics(v.country,COUNT(country)) FROM " +
			"UserDetails v where v.user_id IN(select u.userdetails.user_id from Voting u where u.question.question_id=:question_id)" +
			"GROUP BY v.country")
	List<Statistics> getStatsByCountry(int question_id);
	
	@Query("SELECT new com.pollingsystem.dto.Statistics(v.religion,COUNT(religion)) FROM " +
			"UserDetails v where v.user_id IN(select u.userdetails.user_id from Voting u where u.question.question_id=:question_id)" +
			"GROUP BY v.religion")
	List<Statistics> getStatsByReligion(int question_id);
	
	UserDetails findByEmailIgnoreCase(String email);
}