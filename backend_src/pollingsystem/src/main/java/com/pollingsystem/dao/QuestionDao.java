package com.pollingsystem.dao;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.pollingsystem.entity.Question;
import com.pollingsystem.entity.Voting;

/**
 * This class is used as a DAO which extends crud repository
 * 
 * @author IMVIZAG
 *
 */
public interface QuestionDao extends CrudRepository<Question, Integer> {

	/**
	 * This method finds the user questions based on user id
	 * 
	 * @param user_id
	 * @return
	 */

	@Query("from Question where userDetails.user_id =:user_id order by dateOfCreation desc")
	List<Question> findUserQuestions(int user_id);

	/**
	 * This method finds the question based on date in descending order to retrieve
	 * recent questions
	 * 
	 * @return
	 */
	@Query("from Question order by dateOfCreation desc")
	List<Question> findAllOrderBydateOfCreationDesc();

	/**
	 * this method is used to find the all no of votes
	 * 
	 * @return
	 */

	@Query("from Question order by numberOfVotes desc")
	List<Question> findAllNoOfVotes();

	/**
	 * This method is used to find the trending questions based on the voting on
	 * recent dates
	 * 
	 * @return
	 */

	@Query("FROM Voting WHERE date in (current_date,current_date-1,current_date-2,current_date-3) GROUP BY question_id order by date desc")
	List<Voting> trendingQuestions();

	/**
	 * this method is used to find the questions based on the category
	 * 
	 * @param category
	 * @return
	 */

	@Query("from Question where category =:category")
	List<Question> findAllCategoryQuestions(String category);

	/**
	 * This method is used to search the question based on the user input
	 * 
	 * @param question
	 * @return
	 */
	@Query("from Question where question like %:question% ")
	List<Question> searchQuestions(String question);
	
	@Query("from Question where question=:question")
	List<Question> findByQuestion(String question);
}