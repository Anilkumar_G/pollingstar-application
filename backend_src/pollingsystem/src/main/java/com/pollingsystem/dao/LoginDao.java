package com.pollingsystem.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.pollingsystem.entity.UserDetails;

/**
 * This class is used as a DAO which extends crud repository
 * 
 * @author IMVIZAG
 *
 */
@Repository
public interface LoginDao extends CrudRepository<UserDetails, Integer> {

	/**
	 * this method is to authenticate the user details before login
	 * 
	 * @param email
	 * @param securityQuestion
	 * @param securityAnswer
	 * @return
	 */

	@Query("select a from UserDetails a where a.email =:email and a.securityQuestion =:securityQuestion and a.securityAnswer =:securityAnswer")
	UserDetails securityAuthentication(String email, String securityQuestion, String securityAnswer);
<<<<<<< HEAD
	
	@Query("from UserDetails where email =:email")
	UserDetails resetPassword(String email);

	@Query("select a from UserDetails a where a.email =:email and a.email =:email and a.password =:oldPassword ")
	UserDetails resetNewPassword(String email, String oldPassword);
=======

	/**
	 * This method is used to reset the password of the particular user based on
	 * user correct details
	 * 
	 * @param email
	 * @param oldPassword
	 * @return
	 */

	@Query("select a from UserDetails a where a.email =:email and a.email =:email and a.password =:oldPassword ")
	UserDetails resetNewPassword(String email, String oldPassword);

	Optional<UserDetails> findByEmail(String email);
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
}