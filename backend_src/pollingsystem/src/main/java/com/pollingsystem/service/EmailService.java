package com.pollingsystem.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.pollingsystem.dao.ConfirmationTokenDao;
import com.pollingsystem.entity.ConfirmationToken;
import com.pollingsystem.entity.UserDetails;

@Service
public class EmailService {

  private JavaMailSender mailSender;


  @Autowired
  public EmailService(JavaMailSender mailSender) {
    this.mailSender = mailSender;
  }
  
  @Async
  public void sendEmail(SimpleMailMessage email) {
    mailSender.send(email);
  }
 
}