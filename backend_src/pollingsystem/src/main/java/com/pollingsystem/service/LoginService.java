package com.pollingsystem.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pollingsystem.dao.LoginDao;
import com.pollingsystem.entity.UserDetails;
import com.pollingsystem.util.PasswordEncryption;
 
/**
 * This Class is used to provide services for Login
 * 
 * @author PollingStar
 *
 */
@Service
@Transactional
public class LoginService { 
	
	@Autowired
	private LoginDao loginDao;
<<<<<<< HEAD
	
	public int userLogin(UserDetails userDetails) {
		 Iterable<UserDetails> iAccount = loginDao.findAll();
		 Iterator<UserDetails> iterator = iAccount.iterator();
		 int count = 0;
		
		 while(iterator.hasNext()) {
			 UserDetails userDetails1=iterator.next();
			 
			 if((userDetails.getEmail().equalsIgnoreCase(userDetails1.getEmail()))&&(userDetails.getPassword().equals(userDetails1.getPassword()))) {
				 count = 1;
				 return 1;
			 }
			 else if((userDetails.getEmail().equalsIgnoreCase(userDetails1.getEmail()))){
				count = 2;
			 }
		 }
		 if(count != 2) {
			 return 2;
		 }else {
			 return 3;
		 }
		 
	}
	

=======

	@Autowired
	private UserService userService;

>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
	/**
	 * This method checks the security question before recovering the password
	 * 
	 * @param email
	 * @param securityQuestion
	 * @param securityAnswer
	 * @return integer
	 */
<<<<<<< HEAD
	public boolean securityAuthentication(String email,String securityQuestion,String securityAnswer) {
		UserDetails userDetails = loginDao.securityAuthentication(email,securityQuestion,securityAnswer);
		if(userDetails != null) {
			
			return true;
		} else { 
			return false;
		}
	}

		

=======
	public int securityAuthentication(String email, String securityQuestion, String securityAnswer) {
		// This line sends the fields like security question and security Answer along
		// with the email to check
		// weather the user is valid or not before recovering the password
		List<UserDetails> user=userService.findAll();
		int count=0;
		for(UserDetails userdetails:user) {
			if(email.equalsIgnoreCase(userdetails.getEmail())) {
				count=1;
			}
		}
		if(count==0) {
			return 2;
		}
		UserDetails userDetails = loginDao.securityAuthentication(email, securityQuestion, securityAnswer);
		if (userDetails != null) {

			return 1;
		} else {
			return 3;
		}
	}

	/**
	 *  This method resets the password if user needed
	 * 
	 * @param email
	 * @param password
	 * @return true if updated successfully else false
	 */

	public boolean forgetPassword(String email, String password) {
		// After security credentials validation this method is called and the following
		// password
		// is set to the new password
		System.out.println(email);
		UserDetails resetPassword = findByEmail(email);
		
		
		PasswordEncryption passwordEncryption = new PasswordEncryption();
		String DBPassword = passwordEncryption.getDecryptedPassword(resetPassword.getPassword());
		if (!(DBPassword.equals(password))) {
			resetPassword.setPassword(password);
			loginDao.save(resetPassword);
			return true;
		} else {
			return false;
		}
	}
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e

	/**
	 * This method checks the old password before reseting the password
	 * 
	 * @param email
	 * @param oldPassword
	 * @param password
	 * @return true if updated successfully else false
	 */
	public boolean resetNewPassword(String email, String oldPassword, String password) {
		UserDetails resetPassword = loginDao.resetNewPassword(email, oldPassword);
		PasswordEncryption passwordEncryption = new PasswordEncryption();
		password = passwordEncryption.getEncryptedPassword(password);
		if (resetPassword == null) {
			UserDetails details = findByEmail(email);
			details.setPassword(password);
			loginDao.save(details);
			return true;

		} else {
			return false;
		}

	}

	/**
	 * This method is to fetch user details based on 
	 * @param email
	 * @return
	 */
	public UserDetails findByEmail(String email) {
		// this line calls the dao method to find the user details based on the email
		// input
		Optional<UserDetails> account = loginDao.findByEmail(email);
		return account.get();
	}

<<<<<<< HEAD
	
	/**
	 * This method resets the password if user needed
	 * @param email
	 * @param oldPassword
	 * @param password
	 * @return
	 */
	public boolean resetNewPassword(String email, String oldPassword, String password) {
		UserDetails resetPassword = loginDao.resetNewPassword(email,oldPassword);
		if (resetPassword == null) {
			UserDetails details = loginDao.resetPassword(email);
			details.setPassword(password);
			loginDao.save(details);
			return true;
		
		} else {
			return false;
		}
		// TODO Auto-generated method stub
		
	}
=======
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
}