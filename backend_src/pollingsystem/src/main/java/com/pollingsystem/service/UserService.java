package com.pollingsystem.service;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pollingsystem.dao.ConfirmationTokenDao;
import com.pollingsystem.dao.UserDao;
import com.pollingsystem.dto.Statistics;
import com.pollingsystem.entity.ConfirmationToken;
import com.pollingsystem.entity.UserDetails;
import com.pollingsystem.util.PasswordEncryption;

/**
 * This Class is used to provide services for Users
 * 
 * @author PollingStar
 *
 */
@Service
public class UserService {

	@Autowired
	private UserDao userDao;
	
	@Autowired
	private ConfirmationTokenDao tokenRepository;
	

	/**
	 * this method is used for user registration
	 * 
	 * @param userDetails
	 * @return
	 */

public int userRegistration(ConfirmationToken userDetails) {
	
		
		ConfirmationToken userByEmail = tokenRepository.findByEmailIgnoreCase(userDetails.getEmail());
		
		PasswordEncryption passwordEncryption = new PasswordEncryption();
		String password = passwordEncryption.getEncryptedPassword(userDetails.getPassword());
		
		UserDetails user = new UserDetails();
		
		user.setFirst_name(userDetails.getFirst_name());
		user.setLast_name(userDetails.getLast_name());
		user.setEmail(userDetails.getEmail());
		user.setPassword(password);
		user.setCity(userDetails.getCity());
		user.setState(userDetails.getState());
		user.setCountry(userDetails.getCountry());
		user.setPhone(userDetails.getPhone());
		user.setSecurityAnswer(userDetails.getSecurityAnswer());
		user.setSecurityQuestion(userDetails.getSecurityQuestion());
		user.setGender(userDetails.getGender());
		tokenRepository.delete(userByEmail);
	
		// Here creating the date object and getting the date from the system
		// adding that date to the user details at time of registration
		Date dateOfRegistration = new Date(System.currentTimeMillis());
		user.setDateOfRegistration(dateOfRegistration);
		
		List<UserDetails> userDetailsList = new ArrayList<UserDetails>();
		// Here we are getting the all users details
		Iterable<UserDetails> iAccount = userDao.findAll();
		// Iterating the user details
		Iterator<UserDetails> iterator = iAccount.iterator();

		while (iterator.hasNext()) {
			userDetailsList.add(iterator.next());
		}
		// Here we are validating the email before registration of new answer
		// if it already exits it throws error message
		for (UserDetails details : userDetailsList) {
			if (details.getEmail().equals(user.getEmail())) {
				return -1;
			}
		}

				for(UserDetails details : userDetailsList)
				{
					if (details.getPhone().equals(user.getPhone())) {
						return -3;
					}
				}
		// Here we are saving the user details that is newly registered
		UserDetails details = userDao.save(user);
		
			return 1;
	}

	/**
	 * This method is used to get all users
	 * 
	 * @return user details
	 */
	public List<UserDetails> findAll() {
		List<UserDetails> userDetails = new ArrayList<UserDetails>();
		// Here we are getting the all users details
		Iterable<UserDetails> iAccount = userDao.findAll();
		// Iterating the user details
		Iterator<UserDetails> iterator = iAccount.iterator();

		while (iterator.hasNext()) {
			userDetails.add(iterator.next());
		}
		return userDetails;
	}

	/**
	 * This methos is used to find the user based on id
	 * 
	 * @param user_id
	 * @return user details
	 */

	public UserDetails findById(int user_id) {
		// Here we are getting the user object based on user id
		Optional<UserDetails> user = userDao.findById(user_id);
		return user.get();
	}


	/**
	 * 0
	 * This method is used to update data of a specific user
	 * @param userDetails
	 * @return integer
	 */
	public int updateDetails(UserDetails userDetails) {

		int count = 0;
		UserDetails user=findById(userDetails.getUser_id());

		List<UserDetails> userDetailsList = (List<UserDetails>) userDao.findAll();
		//checking details with database values
		if(user!=null) {
			if(!(user.getFirst_name().equals(userDetails.getFirst_name()))) {

				user.setFirst_name(userDetails.getFirst_name());
				count = 1;
			}if(!(user.getLast_name().equals(userDetails.getLast_name()))) {

				user.setLast_name(userDetails.getLast_name());
				count = 1;
			}if(!(user.getPhone().equals(userDetails.getPhone()))) {

				for(UserDetails details:userDetailsList)
				{
					if(userDetails.getPhone().equals(details.getPhone()))
					{
						return -3;
					}
				}
				user.setPhone(userDetails.getPhone());
				count = 1;
			}if(!(user.getCity().equals(userDetails.getCity()))) {

				user.setCity(userDetails.getCity());
				count = 1;
			}if(!(user.getReligion().equals(userDetails.getReligion()))) {

				user.setReligion(userDetails.getReligion());
				count = 1;
			}if(!(user.getState().equals(userDetails.getState()))) {

				user.setState(userDetails.getState());
				count = 1;
			}if(!(user.getCountry().equals(userDetails.getCountry()))) {

				user.setCountry(userDetails.getCountry());
				count = 1;
			}if(count == 1) {
				userDao.save(user);
				return 1;
			}else {
				return -2;
			}
		}else {
			return -1;
		}

	}
	/**
	 * This method is used to call method from Dao which gives statistics based on city
	 * @param question_id
	 * @return city statistics
	 */
	public  List<Statistics> getStatsBycity(int question_id)
	{
		List<Statistics> stats=userDao.getStatsBycity(question_id);
		return stats;
	}

	/**
	 * This method is used to call method from Dao which gives statistics based on state
	 * @param question_id
	 * @return state statistics
	 */
	public  List<Statistics> getStatsByState(int question_id)
	{
		List<Statistics> stats=userDao.getStatsByState(question_id);
		return stats;
	}
	
	/**
	 * This method is used to call method from Dao which gives statistics based on country
	 * @param question_id
	 * @return country statistics
	 */
	public  List<Statistics> getStatsByCountry(int question_id)
	{
		List<Statistics> stats=userDao.getStatsByCountry(question_id);
		return stats;
	}

	public  List<Statistics> getStatsByReligion(int question_id)
	{
		List<Statistics> stats=userDao.getStatsByReligion(question_id);
		return stats;
	}
	
	public void incrementAttempts(int user_id)
	{
		UserDetails userDetails = findById(user_id);
		if(userDetails.getLoginCount()<=2)
		{
			if(userDetails!=null)
			{
				userDetails.setLoginCount(userDetails.getLoginCount()+1);
			}
		}
		if(userDetails.getLoginCount()==3)
		{
			java.util.Date date=new java.util.Date();
			userDetails.setLastAttempt(date);
			userDao.save(userDetails);
		}
	}
}
