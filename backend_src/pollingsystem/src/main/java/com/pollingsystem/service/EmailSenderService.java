package com.pollingsystem.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.pollingsystem.dao.ConfirmationTokenDao;
import com.pollingsystem.entity.ConfirmationToken;

@Service
public class EmailSenderService {

    private JavaMailSender javaMailSender;
    @Autowired
    private ConfirmationTokenDao tokenRepository;

    @Autowired
    public EmailSenderService(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Async
    public void sendEmail(SimpleMailMessage email) {
        javaMailSender.send(email);
    }
    
    /**
   	 * This method is used to get all users
   	 * 
   	 * @return user details
   	 */
   	public List<ConfirmationToken> findAll() {
   		List<ConfirmationToken> userDetails = new ArrayList<ConfirmationToken>();
   		// Here we are getting the all users details
   		Iterable<ConfirmationToken> iAccount = tokenRepository.findAll();
   		// Iterating the user details
   		Iterator<ConfirmationToken> iterator = iAccount.iterator();

   		while (iterator.hasNext()) {
   			userDetails.add(iterator.next());
   		}
   		return userDetails;
   	}

}
