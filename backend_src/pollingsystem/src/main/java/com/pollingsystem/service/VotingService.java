package com.pollingsystem.service;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
<<<<<<< HEAD
import org.springframework.web.bind.annotation.RequestBody;

=======

import com.pollingsystem.dao.AnswerDao;
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
import com.pollingsystem.dao.VotingDao;
import com.pollingsystem.entity.AnswerEntity;
import com.pollingsystem.entity.Question;
import com.pollingsystem.entity.UserDetails;
import com.pollingsystem.entity.Voting;
<<<<<<< HEAD
=======

/**
 * This Class is used to provide services for Voting
 * 
 * @author PollingStar
 *
 */
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
@Transactional
@Service
public class VotingService {

	@Autowired
	private UserService user;
<<<<<<< HEAD
	
	@Autowired
	private QuestionService questionService;
	
	@Autowired
	private AnswerService answerService;
	
	@Autowired
	private VotingDao votingDao;
	
	public void votingForAnswer(Voting voting) {
			Date dateOfRegistration = new Date(System.currentTimeMillis());
			voting.setDate(dateOfRegistration);
			
			UserDetails userDetails = user.findById(voting.getUserdetails().getUser_id());
			voting.setUserdetails(userDetails);
			Question question	=	questionService.findById(voting.getQuestion().getQuestion_id());
			voting.setQuestion(question);
			
			AnswerEntity answer = answerService.findById(voting.getAnswer().getAnswerId());
			voting.setAnswer(answer);
	
			userDetails.getVoting().add(voting);
			
			question.getVoting().add(voting);
			
			answer.getVoting().add(voting);
			
			List<Voting> votinglist=(List<Voting>) votingDao.findAll();
			int count=0;
			for(Voting vote:votinglist) {
				
				if(voting.getUserdetails().getUser_id()==vote.getUserdetails().getUser_id()&&voting.getQuestion().getQuestion_id()==vote.getQuestion().getQuestion_id()&&voting.getAnswer().getAnswerId()==vote.getAnswer().getAnswerId()) 
					{
						count=1;
						break;
					}
					
					
			
				}
			if(count==0) {
				votingDao.save(voting);
			}
	}
}

	

=======

	@Autowired
	private QuestionService questionService;

	@Autowired
	private AnswerService answerService;

	@Autowired
	private VotingDao votingDao;
	
	@Autowired
	private AnswerDao answerDao;

	/**
	 * This method is used to vote for an answer
	 * 
	 * @param voting
	 * @return integer
	 */
	public int votingForAnswer(Voting voting) {
		// Here we are creating the date object and getting the current system date and
		// storing the date in the Voting object
		Date dateOfRegistration = new Date(System.currentTimeMillis());
		voting.setDate(dateOfRegistration);
		// Getting the all voting data before casting the vote to a particular question
		List<Voting> votinglist = (List<Voting>) votingDao.findAll();
		int count = 0;
		for (Voting vote : votinglist) {
			// if the vote is already casted it removes the vote of particular answer
			// and updates the vote to the new answer in the same question
			if (voting.getUserdetails().getUser_id() == vote.getUserdetails().getUser_id()
					&& voting.getQuestion().getQuestion_id() == vote.getQuestion().getQuestion_id()) {
				count = 1;
				AnswerEntity prev_answer = vote.getAnswer();
				// getting the votes of particular question
				int num_of_votes1 = prev_answer.getNoOfVotes();
				// decrementing the votes of particular answer
				num_of_votes1--;
				prev_answer.setNoOfVotes(num_of_votes1);
				Voting votingFetch = votingDao.findById(vote.getVoting_id()).get();
				AnswerEntity answer = answerDao.findId(voting.getAnswer().getId(),
						voting.getQuestion().getQuestion_id());
				int num_of_votes = answer.getNoOfVotes();
				// adding the vote to new answer of same question
				num_of_votes++;
				answer.setNoOfVotes(num_of_votes);

				votingFetch.setAnswer(answer);
				votingDao.save(votingFetch);
				return -1;
			}
		}

		if (count == 0) {
			UserDetails userDetails = user.findById(voting.getUserdetails().getUser_id());
			voting.setUserdetails(userDetails);

			Question question = questionService.findById(voting.getQuestion().getQuestion_id());
			int num_of_votes = question.getNumberOfVotes();
			// Incrementing the total votes of the question by one
			num_of_votes++;
			question.setNumberOfVotes(num_of_votes);
			voting.setQuestion(question);

			AnswerEntity answer = answerService.findId(voting.getAnswer().getId(),
					voting.getQuestion().getQuestion_id());
			int num_of_votes1 = answer.getNoOfVotes();
			// Adding the new vote to the answer
			num_of_votes1++;
			answer.setNoOfVotes(num_of_votes1);
			voting.setAnswer(answer);

			userDetails.getVoting().add(voting);

			question.getVoting().add(voting);

			answer.getVoting().add(voting);

			votingDao.save(voting);

			return 1;
		}

		else {
			return -2;
		}
	}

	/**
	 * This method is used to find the voting object based on voting id
	 */

	public Voting findById(int voting_id) {
		// getting the voting details by send voting id
		Optional<Voting> optionalObject = votingDao.findById(voting_id);
		return optionalObject.get();
	}
}
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
