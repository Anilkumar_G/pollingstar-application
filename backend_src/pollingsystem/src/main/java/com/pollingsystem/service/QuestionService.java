package com.pollingsystem.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pollingsystem.dao.QuestionDao;
import com.pollingsystem.dao.UserDao;
import com.pollingsystem.dto.AnswerResponse;
import com.pollingsystem.dto.QuestionResponse;
import com.pollingsystem.entity.AnswerEntity;
import com.pollingsystem.entity.Question;
import com.pollingsystem.entity.UserDetails;
import com.pollingsystem.entity.Voting;

/**
 * This Class is used to provide services for Questions
 * 
 * @author PollingStar
 *
 */
@Service
public class QuestionService {

	@Autowired
	private QuestionDao questionDao;
	@Autowired
	private UserService user;

	@Autowired
	private UserDao userDao;
	
	@Transactional

	/**
	 * this method is used to add the question by the user
	 * 
	 * @param question
	 * @return
	 */

	public int addQuestion(Question question) {

		int count=0;
		if (question.getQuestion().equals("") || question.getQuestion().equals(" ")) {
			return -2;
		}
		for(int i=0;i<question.getQuestion().length();i++)
		{
			if(question.getQuestion().charAt(i)!=' ')
			{
				count=1;
			}
		}
		if(count==0)
		{
			return -2;
		}
		//Here we are setting category to anonymous
				if(question.getCategory()==null)
				{
					question.setCategory("Anonymous");
				}
		// Here by using the find by id we are getting the user details and setting them
		// to the question object
		Optional<UserDetails> userDetailsOptional = userDao.findById(question.getUserDetails().getUser_id());
		UserDetails userDetails =  userDetailsOptional.get();
		question.setUserDetails(userDetails);
		// Here we are getting the details of user questions by sending the user_id as
		// input
		List<Question> userQuestionsList = questionDao.findUserQuestions(userDetails.getUser_id());
		// Comparing the Question with list of questions that are presented in the
		// Database
		for (Question questions : userQuestionsList) {
			if (questions.getQuestion().equalsIgnoreCase(question.getQuestion()) && questions.getCategory().equalsIgnoreCase(question.getCategory())) {
				return -1;
			}
		}

		// Here we are adding the question object to the user details of question
		// details
		userDetails.getQuestion_details().add(question);
		// Here we are getting the system current data with time and setting the date to
		// the question object
		Date dateOfRegistration = new Date(System.currentTimeMillis());
		question.setDateOfCreation(dateOfRegistration);
		
		// Here we are saving the question object into the database
		Question userQuestion = questionDao.save(question);

		if (userQuestion != null) {
			return 1;
		}

		else {
			return -3;
		}
	}

	/**
	 * this method displays all questions
	 * 
	 * @return
	 */
	public List<Question> findAll() {
		// This method is used to get the all questions by calling the dao method
		// findall
		List<Question> questions = new ArrayList<Question>();
		Iterable<Question> iAccount = questionDao.findAll();
		Iterator<Question> iterator = iAccount.iterator();

		while (iterator.hasNext()) {
			questions.add(iterator.next());
		}
		return questions;
	}

	/**
	 * This method display the user questions
	 * 
	 * @param user_id
	 * @return
	 */

	public List<Question> diplayUserQuestions(int user_id) {
		// This method is used to get the all user questions by calling the question dao
		// method
		// find user questions
		List<Question> questions = questionDao.findUserQuestions(user_id);
		return questions;
	}

	/**
	 * this method finds the question object based on user id
	 * 
	 * @param question_id
	 * @return
	 */
	public Question findById(int question_id) {
		Optional<Question> account = questionDao.findById(question_id);
		return account.get();
	}

	/**
	 * this method display the recent questions
	 * 
	 * @return
	 */
	public List<Question> diplayRecentQuestions() {
		// Here we are calling the dao method to get the recent questions
		List<Question> questions = questionDao.findAllOrderBydateOfCreationDesc();
		return questions;
	}

	/**
	 * This method displays the questions in categorical way
	 * 
	 * @param category
	 * @return
	 */

	public List<Question> displayCategoryQuestions(String category) {
		// Here we are calling the dao method to get the questions based on category
		return questionDao.findAllCategoryQuestions(category);
	}

	/**
	 * This method is used to get the trending questions based on the votes that are
	 * casted to the question
	 * 
	 * @return
	 */

	public List<QuestionResponse> diplayTrendingQuestions() {

		// Here we are getting the trending questiopns based on Votes that are casted to
		// the question

		List<Voting> trend = questionDao.trendingQuestions();
		// calling the method to find the all votes that are casted to the question
		List<Question> questionTrend = questionDao.findAllNoOfVotes();

		List<QuestionResponse> questionList = new ArrayList<>();

		for (Question question : questionTrend) {

			for (Voting vote : trend) {

				if (vote.getQuestion().getQuestion_id() == question.getQuestion_id()) {
					List<AnswerResponse> answerList = new ArrayList<>();
					QuestionResponse questionResponse = new QuestionResponse();
					// Getting the trending questions and also their respective answers to display
					for (AnswerEntity answer : question.getAnswers()) {

						AnswerResponse answerResponse = new AnswerResponse();
						answerResponse.setAnswer(answer.getAnswer());
						answerResponse.setAnswerId(answer.getAnswerId());
						answerResponse.setId(answer.getId());
						answerResponse.setUsername(answer.getUserDetails().getFirst_name());
						answerResponse.setDate(answer.getDate());
						answerResponse.setNoOfVotes(answer.getNoOfVotes());
						answerList.add(answerResponse);
					}

					questionResponse.setQuestion(question.getQuestion());
					questionResponse.setCategory(question.getCategory());
					questionResponse.setDateOfCreation(question.getDateOfCreation());
					questionResponse.setNumberOfVotes(question.getNumberOfVotes());
					questionResponse.setQuestion_id(question.getQuestion_id());
					questionResponse.setUsername(question.getUserDetails().getFirst_name());
					questionResponse.setAnswers(answerList);
					questionList.add(questionResponse);
				}

			}
		}

		return questionList;
	}

	/**
	 * this method search the question based on user input
	 * 
	 * @param question
	 * @return
	 */
	public List<Question> displaySearchedQuestions(String question) {
		// Calling the method that is presented in the dao class to
		// find the search related questions
		return questionDao.searchQuestions(question);
	}
	
	public int deleteQuestion(Question questionObject)
	{
		int count = 0;
		List<Question> questionList = questionDao.findByQuestion(questionObject.getQuestion());
		if(questionList.isEmpty())
		{
			return -2;
		}
		else
		{
			for(Question questionResult:questionList)
			{
				if(questionResult.getCategory().equalsIgnoreCase(questionObject.getCategory())&&(questionObject.getQuestion().equalsIgnoreCase(questionResult.getQuestion())))
				{
					Optional<Question> account = questionDao.findById(questionResult.getQuestion_id());
					if(account!=null)
					{
						Question question = account.get();
//						
//						question.getVoting().remove(0);
//						
//						List<AnswerEntity> answerEntities = answerDao.findByQuestion(question);
//						question.getAnswers().remove(0);
						
						questionDao.delete(question);
						count=1;
						break;
					}					
				}
			}
			if(count == 1)
			{
				return 1;
			}
			else
			{
				return -1;
			}
			
		}
			
	}

}
