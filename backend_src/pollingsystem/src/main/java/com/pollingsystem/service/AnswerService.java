package com.pollingsystem.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pollingsystem.dao.AnswerDao;
import com.pollingsystem.dao.QuestionDao;
import com.pollingsystem.dao.UserDao;
import com.pollingsystem.entity.AnswerEntity;
import com.pollingsystem.entity.Question;
import com.pollingsystem.entity.UserDetails;

/**
 * This Class is used to provide services for answers
 * 
 * @author PollingStar
 *
 */
@Service
@Transactional
public class AnswerService {

	// Fetching AutoWired Fields
	@Autowired
	private AnswerDao answerDao;
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private QuestionDao questionDao;
	/**
	 * This method is used to add the answer by the user
	 * 
	 * @param answer
	 * @return integer
	 */
	public int addAnswer(AnswerEntity answer) {

		if (answer.getAnswer().equals("") || answer.getAnswer().equals(" ")) {
			return -3;
		}
		int countSpace=0;
		
		for(int i=0;i<answer.getAnswer().length();i++)
		{
			if(answer.getAnswer().charAt(i)!=' ')
			{
				countSpace=1;
			}
		}
		if(countSpace==0)
		{
			return -3;
		}

		// Fetching the user details object from DB
		UserDetails userDetails = userDao.findById(answer.getUserDetails().getUser_id()).get();

		// Fetching the question object from DB
		Question question = questionDao.findById(answer.getQuestion().getQuestion_id()).get();

		// Fetching the answers from question object
		List<AnswerEntity> answers = question.getAnswers();

		answer.setId(0);

		int count = answers.size();

		answer.setId(++count);

		for (AnswerEntity answerSearch : answers) {
			// If the given answer already exists for that question
			if (answerSearch.getAnswer().equalsIgnoreCase(answer.getAnswer())) {
				return -1;
			}
		}

		answer.setUserDetails(userDetails);
<<<<<<< HEAD
		
		Question question=questionService.findById(answer.getQuestion().getQuestion_id());
		
=======

>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
		answer.setQuestion(question);

		userDetails.getAnswer_details().add(answer);
		question.getAnswers().add(answer);

		Date dateOfRegistration = new Date(System.currentTimeMillis());
		answer.setDate(dateOfRegistration);
		AnswerEntity answerEntity = answerDao.save(answer);

		if (answerEntity != null) {
			return 1;
		} else {
			return -3;
		}
	}
	/**
	 * This method is used to find all answers
	 * 
	 * @return answers
	 */
	public List<AnswerEntity> findAll() {
		
		List<AnswerEntity> answers = new ArrayList<AnswerEntity>();
		// Fetching the answer details object from DB
		Iterable<AnswerEntity> allAnswers = answerDao.findAll();
		Iterator<AnswerEntity> iterator = allAnswers.iterator();

		while (iterator.hasNext()) {
			answers.add(iterator.next());
		}
		return answers;
	}

	/**
	 * This method is used to find the answer object based on answer id
	 * 
	 * @param answer_id
	 * @return answer based answer_id
	 */
	public AnswerEntity findById(int answer_id) {
		Optional<AnswerEntity> answer = answerDao.findById(answer_id);

		return answer.get();
	}


	/**
	 * This method is used to find the answer object based on id
	 * 
	 * @param id
	 * @return answer based id
	 */
	
	public AnswerEntity findId(int id, int question_id) {

		AnswerEntity answer = answerDao.findId(id, question_id);

		return answer;
	}

	
	public AnswerEntity findById(int answer_id) {
		Optional<AnswerEntity> account =answerDao.findById(answer_id);
	
		return  account.get();
	}
	
	
}
