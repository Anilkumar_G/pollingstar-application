package com.pollingsystem.util;

/**
 * This class is used send responses
 * 
 * @author Polling Star
 *
 */
public class ResponseObject {

	private Object response;

	private int statuscode;

	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the response
	 */
	public Object getResponse() {
		return response;
	}

	/**
	 * @param response the response to set
	 */
	public void setResponse(Object response) {
		this.response = response;
	}

	/**
	 * @return the statuscode
	 */
	public int getStatuscode() {
		return statuscode;
	}

	/**
	 * @param statuscode the statuscode to set
	 */
	public void setStatuscode(int statuscode) {
		this.statuscode = statuscode;
	}

}
