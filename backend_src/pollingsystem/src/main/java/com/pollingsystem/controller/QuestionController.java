package com.pollingsystem.controller;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
<<<<<<< HEAD
=======
import org.springframework.web.bind.annotation.DeleteMapping;
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pollingsystem.dto.AnswerResponse;
import com.pollingsystem.dto.QuestionResponse;
import com.pollingsystem.entity.AnswerEntity;
import com.pollingsystem.entity.Question;
import com.pollingsystem.entity.UserDetails;
import com.pollingsystem.service.QuestionService;
import com.pollingsystem.util.JwtImpl;
import com.pollingsystem.util.ResponseObject;

/**
 * This class is used to provide api's for Question Functionalities
 * 
 * @author IMVIZAG
 *
 */
@RestController
<<<<<<< HEAD
@CrossOrigin
=======
@CrossOrigin(origins = "*")
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
@Transactional
@RequestMapping("/api/question")
public class QuestionController {

	@Autowired
	private QuestionService questionService;

	/**
	 * This method creates the new question
	 * 
	 * @param question
	 * @return {ResponseEntity}
	 */
	@PostMapping("/create")
	public ResponseEntity<ResponseObject> insertQuestion(@RequestBody Question question,@RequestHeader HttpHeaders headers) {
		String token = "";
		ResponseObject responseObject = new ResponseObject();
		List<String> hList = headers.get("Authorization");
		if (hList == null) {
			responseObject.setResponse("Invalid user");
			responseObject.setStatuscode(-3);

			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		}
		token = hList.get(0);
		// calling method to add question
		int result = questionService.addQuestion(question);

		
<<<<<<< HEAD
		if(result == 1)
		{
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"YOUR QUESTION IS ADDED SUCCESSFULLY\",\"statuscode\":" + 1 + "}");
		}
		else if(result == -1)
		{
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Question Already created by this user\",\"statuscode\":" + -1 + "}");
		}
		else if(result==-2)
		{
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"user does not exist\",\"statuscode\":" + -2 + "}");
=======

		if (JwtImpl.isValidUser(question.getUserDetails().getUser_id(), token)) {
			// Checking conditions to return different responses
			if (result == 1) {
				responseObject.setResponse("YOUR QUESTION IS ADDED SUCCESSFULLY");
				responseObject.setStatuscode(1);

				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			} else if (result == -1) {
				responseObject.setResponse("Question Already created by this user");
				responseObject.setStatuscode(-1);

				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			} else if (result == -2) {
				responseObject.setResponse("Please enter any question");
				responseObject.setStatuscode(-2);

				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			} else {
				responseObject.setResponse("error creating in question");
				responseObject.setStatuscode(-3);

				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			}
		}
		responseObject.setResponse("inavalid user");
		responseObject.setStatuscode(-4);

		return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
	}

	/**
	 * this method display all questions along with answers
	 * 
	 * @return
	 */

	@GetMapping("/allAnswers")
	public ResponseEntity<ResponseObject> displayAllQuestions() {
//		String token = "";
		ResponseObject responseObject = new ResponseObject();
//		List<String> hList = headers.get("Authorization");
//		if (hList == null) {
//			responseObject.setResponse("invalid user");
//			responseObject.setStatuscode(-3);
//
//			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
//		}
//		token = hList.get(0);
		// Calling method to fetch all questions
		List<Question> allQuestions = questionService.findAll();
		List<QuestionResponse> questionList = new ArrayList<>();

//		if (JwtImpl.isValidUser(id, token)) {
			// Setting the values from database to data transfer object if list is not empty
			if (!allQuestions.isEmpty()) {
				for (Question question : allQuestions) {
//					if (question.getUserDetails().getUser_id() != id) {
						QuestionResponse questionResponse = new QuestionResponse();
						List<AnswerResponse> answerList = new ArrayList<>();

						for (AnswerEntity answer : question.getAnswers()) {

							AnswerResponse answerResponse = new AnswerResponse();

							answerResponse.setAnswer(answer.getAnswer());
							answerResponse.setAnswerId(answer.getAnswerId());
							answerResponse.setDate(answer.getDate());
							answerResponse.setNoOfVotes(answer.getNoOfVotes());
							answerResponse.setUsername(answer.getUserDetails().getFirst_name());
							answerResponse.setId(answer.getId());
							answerList.add(answerResponse);
						}
						questionResponse.setQuestion(question.getQuestion());
						questionResponse.setDateOfCreation(question.getDateOfCreation());
						questionResponse.setCategory(question.getCategory());
						questionResponse.setUsername(question.getUserDetails().getFirst_name());
						questionResponse.setNumberOfVotes(question.getNumberOfVotes());
						questionResponse.setQuestion_id(question.getQuestion_id());

						questionResponse.setAnswers(answerList);

						questionList.add(questionResponse);
					}
				
				responseObject.setResponse(questionList);
				responseObject.setStatuscode(1);

				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			} else {
				responseObject.setResponse("NO QUESTIONS EXISTS TO DISPLAY");
				responseObject.setStatuscode(-1);

				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			}
//		}
//		responseObject.setResponse("Invalid User");
//		responseObject.setStatuscode(-2);

//		return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
	}

	/**
	 * this method displays the all user created questions
	 * 
	 * @param userDetails
	 * @return ResponseObject
	 */

	@PostMapping("/userQuestion/answer")
	public ResponseEntity<ResponseObject> diplayUserQuestions(@RequestHeader HttpHeaders headers,
			@RequestBody UserDetails userDetails) {
		String token = "";
		ResponseObject responseObject = new ResponseObject();
		List<String> hList = headers.get("Authorization");
		if (hList == null) {
			responseObject.setResponse("invalid user");
			responseObject.setStatuscode(-3);

			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		}
		token = hList.get(0);
		// Calling method to display user questions
		List<Question> userAllQuestions = questionService.diplayUserQuestions(userDetails.getUser_id());

		responseObject = new ResponseObject();

		if (JwtImpl.isValidUser(userDetails.getUser_id(), token)) {
			List<QuestionResponse> questionList = new ArrayList<>();
			// Setting the values from database to data transfer object if list is not empty
			if (!userAllQuestions.isEmpty()) {
				for (Question question : userAllQuestions) {

					List<AnswerResponse> answerList = new ArrayList<>();
					QuestionResponse questionResponse = new QuestionResponse();

					for (AnswerEntity answer : question.getAnswers()) {

						AnswerResponse answerResponse = new AnswerResponse();

						answerResponse.setAnswer(answer.getAnswer());
						answerResponse.setAnswerId(answer.getAnswerId());
						answerResponse.setDate(answer.getDate());
						answerResponse.setNoOfVotes(answer.getNoOfVotes());
						answerResponse.setId(answer.getId());
						answerResponse.setUsername(answer.getUserDetails().getFirst_name());
						answerList.add(answerResponse);
					}

					questionResponse.setQuestion(question.getQuestion());
					questionResponse.setDateOfCreation(question.getDateOfCreation());
					questionResponse.setCategory(question.getCategory());
					questionResponse.setUsername(question.getUserDetails().getFirst_name());
					questionResponse.setNumberOfVotes(question.getNumberOfVotes());
					questionResponse.setQuestion_id(question.getQuestion_id());
					questionResponse.setAnswers(answerList);
					questionList.add(questionResponse);
				}
				responseObject.setResponse(questionList);
				responseObject.setStatuscode(1);

				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			}

			else {
				responseObject.setResponse("YOU HAVE NOT ENTERED ANY QUESTION");
				responseObject.setStatuscode(-1);

				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			}
		}

		responseObject.setResponse("invalid user");
		responseObject.setStatuscode(-3);

		return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
	}

	/**
	 * This method gets the recent questions along with answers for displaying
	 * 
	 * @return ResponseObject
	 */

	@GetMapping("/recent/{id}")
	public ResponseEntity<ResponseObject> diplayRecentQuestions(@PathVariable int id,@RequestHeader HttpHeaders headers) {

		String token = "";
		ResponseObject responseObject = new ResponseObject();
		List<String> hList = headers.get("Authorization");
		if (hList == null) {
			responseObject.setResponse("invalid user");
			responseObject.setStatuscode(-3);

			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		}
		token = hList.get(0);

		// Calling method to display recent questions
		List<Question> recentQuestions = questionService.diplayRecentQuestions();
		List<QuestionResponse> questionList = new ArrayList<>();

		if (JwtImpl.isValidUser(id, token)) {
			// Setting the values from database to data transfer object if list is not empty
			if (!recentQuestions.isEmpty()) {

				for (Question question : recentQuestions) {

					QuestionResponse questionResponse = new QuestionResponse();
					List<AnswerResponse> answerList = new ArrayList<>();

					for (AnswerEntity answer : question.getAnswers()) {

						AnswerResponse answerResponse = new AnswerResponse();

						answerResponse.setAnswer(answer.getAnswer());
						answerResponse.setAnswerId(answer.getAnswerId());
						answerResponse.setDate(answer.getDate());
						answerResponse.setNoOfVotes(answer.getNoOfVotes());
						answerResponse.setUsername(answer.getUserDetails().getFirst_name());
						answerResponse.setId(answer.getId());
						answerList.add(answerResponse);
					}

					questionResponse.setQuestion(question.getQuestion());
					questionResponse.setDateOfCreation(question.getDateOfCreation());
					questionResponse.setCategory(question.getCategory());
					questionResponse.setUsername(question.getUserDetails().getFirst_name());
					questionResponse.setNumberOfVotes(question.getNumberOfVotes());
					questionResponse.setQuestion_id(question.getQuestion_id());
					questionResponse.setAnswers(answerList);
					questionList.add(questionResponse);
				}
				responseObject.setResponse(questionList);
				responseObject.setStatuscode(1);

				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			} else {
				responseObject.setResponse("NO QUESTIONS EXISTS TO DISPLAY");
				responseObject.setStatuscode(-1);

				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			}
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
		}
		responseObject.setResponse("invalid user");
		responseObject.setStatuscode(-3);

<<<<<<< HEAD
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"error creating in question\",\"statuscode\":" + -3 + "}");
=======
		return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
	}

	/**
	 * this method displays the questions category wise
	 * 
	 * @param questionResponseset
	 * @return ResponseObject
	 */
	@PostMapping("/category/{id}")
	public ResponseEntity<ResponseObject> diplayCategoryQuestions(@RequestHeader HttpHeaders headers,@RequestBody QuestionResponse questionResponseset, @PathVariable int id) {

		String token = "";
		ResponseObject responseObject = new ResponseObject();
		List<String> hList = headers.get("Authorization");
		if (hList == null) {
			responseObject.setResponse("invalid user");
			responseObject.setStatuscode(-3);

			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
		}
		token = hList.get(0);

		// calling method to display questions according to category
		List<Question> recentQuestions = questionService.displayCategoryQuestions(questionResponseset.getCategory());
		List<QuestionResponse> questionList = new ArrayList<>();

		if (JwtImpl.isValidUser(id, token)) {
			// Setting the values from database to data transfer object if list is not empty
			if (!recentQuestions.isEmpty()) {

				for (Question question : recentQuestions) {

					QuestionResponse questionResponse = new QuestionResponse();
					List<AnswerResponse> answerList = new ArrayList<>();

					for (AnswerEntity answer : question.getAnswers()) {

						AnswerResponse answerResponse = new AnswerResponse();

						answerResponse.setAnswer(answer.getAnswer());
						answerResponse.setAnswerId(answer.getAnswerId());
						answerResponse.setDate(answer.getDate());
						answerResponse.setNoOfVotes(answer.getNoOfVotes());
						answerResponse.setUsername(answer.getUserDetails().getFirst_name());
						answerResponse.setId(answer.getId());
						answerList.add(answerResponse);
					}

					questionResponse.setQuestion(question.getQuestion());
					questionResponse.setDateOfCreation(question.getDateOfCreation());
					questionResponse.setCategory(question.getCategory());
					questionResponse.setUsername(question.getUserDetails().getFirst_name());
					questionResponse.setNumberOfVotes(question.getNumberOfVotes());
					questionResponse.setQuestion_id(question.getQuestion_id());
					questionResponse.setAnswers(answerList);
					questionList.add(questionResponse);
				}
				responseObject.setResponse(questionList);
				responseObject.setStatuscode(1);

				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			} else {
				responseObject.setResponse("NO QUESTIONS EXISTS TO DISPLAY");
				responseObject.setStatuscode(-1);

				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			}
		}
		responseObject.setResponse("invalid user");
		responseObject.setStatuscode(-3);

		return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
	}

	/**
	 * This method is used to display trending questions
	 * 
	 * @param question
	 * @return ResponseObject
	 */

	@GetMapping("/trendingQuestions/{id}")
	public ResponseEntity<ResponseObject> TrendingQuestions(@PathVariable int id, @RequestHeader HttpHeaders headers) {
		String token = "";
		ResponseObject responseObject = new ResponseObject();
		List<String> hList = headers.get("Authorization");
		if (hList == null) {
			responseObject.setResponse("invalid user");
			responseObject.setStatuscode(-3);

			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		}
<<<<<<< HEAD
		else
		{
			return  new ResponseEntity("No Questions found!!",HttpStatus.OK);
=======
		token = hList.get(0);

		List<QuestionResponse> trendingQuestions = questionService.diplayTrendingQuestions();
		if (JwtImpl.isValidUser(id, token)) {

			if (!trendingQuestions.isEmpty()) {
				responseObject.setResponse(trendingQuestions);
				responseObject.setStatuscode(1);
				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			} else {
				responseObject.setResponse("NO QUESTIONS EXISTS TO DISPLAY");
				responseObject.setStatuscode(-1);
				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			}
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
		}
		responseObject.setResponse("invalid user");
		responseObject.setStatuscode(-3);

		return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
	}

	/**
	 * This method is used to display questions based on keyword
	 * 
	 * @param question
	 * @return ResponseObject
	 */
	@PostMapping("/searchQuestion/{id}")
	public ResponseEntity<ResponseObject> displayAllSearchedQuestions(@RequestBody QuestionResponse questionResponseset) {
//		String token = "";
		ResponseObject responseObject = new ResponseObject();
//		List<String> hList = headers.get("Authorization");
//		if (hList == null) {
//			responseObject.setResponse("invalid user");
//			responseObject.setStatuscode(-3);
//
//			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
//		}
//		token = hList.get(0);

		// Calling method to display search results of questions
		List<Question> searchAllQuestions = questionService.displaySearchedQuestions(questionResponseset.getQuestion());
		List<QuestionResponse> questionList = new ArrayList<>();

//		if (JwtImpl.isValidUser(id, token)) {
			// Setting the values from database to data transfer object if list is not empty
			if (!searchAllQuestions.isEmpty()) {
				for (Question question : searchAllQuestions) {
					//				System.out.println(question.getAnswers());
					QuestionResponse questionResponse = new QuestionResponse();
					List<AnswerResponse> answerList = new ArrayList<>();

					for (AnswerEntity answer : question.getAnswers()) {

						AnswerResponse answerResponse = new AnswerResponse();

						answerResponse.setAnswer(answer.getAnswer());
						answerResponse.setAnswerId(answer.getAnswerId());
						answerResponse.setDate(answer.getDate());
						answerResponse.setNoOfVotes(answer.getNoOfVotes());
						answerResponse.setUsername(answer.getUserDetails().getFirst_name());
						answerResponse.setId(answer.getId());
						answerList.add(answerResponse);
					}
					questionResponse.setQuestion(question.getQuestion());
					questionResponse.setDateOfCreation(question.getDateOfCreation());
					questionResponse.setCategory(question.getCategory());
					questionResponse.setNumberOfVotes(question.getNumberOfVotes());
					questionResponse.setQuestion_id(question.getQuestion_id());
					questionResponse.setUsername(question.getUserDetails().getFirst_name());
					questionResponse.setAnswers(answerList);

					questionList.add(questionResponse);
				}
				responseObject.setResponse(questionList);
				responseObject.setStatuscode(1);

				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			} else {
				responseObject.setResponse("NO QUESTIONS EXISTS TO DISPLAY");
				responseObject.setStatuscode(-1);

				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			}
	}

	@DeleteMapping("/deleteQuestion/{id}")
	public ResponseEntity<ResponseObject> delteQuestion(@RequestBody Question questionResponseset,
			@PathVariable int id, @RequestHeader HttpHeaders headers) {
		String token = "";
		ResponseObject responseObject = new ResponseObject();
		List<String> hList = headers.get("Authorization");
		if (hList == null) {
			responseObject.setResponse("invalid user");
			responseObject.setStatuscode(-3);

			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		}
		token = hList.get(0);

		if (JwtImpl.isValidUser(id, token)) {
			if(questionService.deleteQuestion(questionResponseset)==1)
			{
				responseObject.setResponse("Question deleted successfully");
				responseObject.setStatuscode(1);

				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			}
			else if(questionService.deleteQuestion(questionResponseset)==-1)
			{
				responseObject.setResponse("Question category doesn't exists");
				responseObject.setStatuscode(-1);

				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			}
			else
			{
				responseObject.setResponse("Question doesn't exists");
				responseObject.setStatuscode(-2);

				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			}
		}
		else
		{
<<<<<<< HEAD
			return  new ResponseEntity("No Questions found!!",HttpStatus.OK);
=======
			responseObject.setResponse("invalid user");
			responseObject.setStatuscode(-3);

			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
		}
	}
}
