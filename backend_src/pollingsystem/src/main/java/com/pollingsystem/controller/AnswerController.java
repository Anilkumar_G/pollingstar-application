package com.pollingsystem.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pollingsystem.entity.AnswerEntity;
import com.pollingsystem.service.AnswerService;
import com.pollingsystem.util.JwtImpl;
import com.pollingsystem.util.ResponseObject;

/**
 * This class is used to provide api's for Answer Functionalities
 * 
 * @author IMVIZAG
 *
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/answer")
public class AnswerController {

	@Autowired
	private AnswerService answerService;

	/**
	 * This method is used to create the answer by the user
	 * 
	 * @param answer
	 * @return ResponseObject
	 */

	@PostMapping("/create")
<<<<<<< HEAD
	public ResponseEntity<?> insertAnswer(@RequestBody AnswerEntity answer)
	{
	
		int result = answerService.addAnswer(answer);
		int question_id=answer.getQuestion().getQuestion_id();
		if(result == 1)
		{
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"YOUR Answer IS ADDED SUCCESSFULLY FOR\""+question_id +",\"statuscode\":" + 1 + "}");
		}
		else if(result == -1)
		{
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Answer Already created for this question\",\"statuscode\":" + -1 + "}");
		}
		else if(result==-2)
		{
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"user does not exist\",\"statuscode\":" + -2 + "}");
=======
	public ResponseEntity<ResponseObject> insertAnswer(@RequestBody AnswerEntity answer,@RequestHeader HttpHeaders headers) {
		ResponseObject responseObject = new ResponseObject();
		String token = "";
		List<String> hList = headers.get("Authorization");
		if (hList == null) {
			responseObject.setResponse("invalid user");
			responseObject.setStatuscode(-3);

			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
		}
		token = hList.get(0);

		if (JwtImpl.isValidUser(answer.getUserDetails().getUser_id(), token)) {
			System.out.println(answer.getUserDetails().getUser_id());
			// calling method to add question
			int result = answerService.addAnswer(answer);
			int question_id = answer.getQuestion().getQuestion_id();
			
			// Checking conditions to return different responses
			if (result == 1) {

				responseObject.setResponse("YOUR Answer IS ADDED SUCCESSFULLY FOR\"" + question_id);
				responseObject.setStatuscode(1);

				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			} else if (result == -1) {
				responseObject.setResponse("Answer Already created for this question. ");
				responseObject.setStatuscode(-1);

				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);

			} else if (result == -2) {
				responseObject.setResponse("error creating in answer");
				responseObject.setStatuscode(-2);

				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			} else {
				responseObject.setResponse("please enter any answer");
				responseObject.setStatuscode(-3);

<<<<<<< HEAD
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"error creating in answer\",\"statuscode\":" + -3 + "}");
=======
				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			}
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
		}
		responseObject.setResponse("invalid user");
		responseObject.setStatuscode(-3);

		return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
	}
}