package com.pollingsystem.controller;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
<<<<<<< HEAD
=======
import org.springframework.mail.SimpleMailMessage;
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

<<<<<<< HEAD
=======
import com.pollingsystem.dao.ConfirmationTokenDao;
import com.pollingsystem.dao.UserDao;
import com.pollingsystem.dto.UserResponse;
import com.pollingsystem.entity.ConfirmationToken;
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
import com.pollingsystem.entity.UserDetails;
import com.pollingsystem.entity.Voting;
import com.pollingsystem.service.EmailSenderService;
import com.pollingsystem.service.UserService;
import com.pollingsystem.util.JwtImpl;
import com.pollingsystem.util.ResponseObject;

/**
 * This class is used ton provide api's for User operations
 * 
 * @author IMVIZAG
 *
 */
@RestController
<<<<<<< HEAD
@CrossOrigin
=======
@Transactional
@CrossOrigin(origins = "*")
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
@RequestMapping("/api/user")
public class UserController {

	@Autowired
	private UserService userRegistrationService;
<<<<<<< HEAD
	
	@PostMapping("/create")
	public ResponseEntity<?> userRegistration(@RequestBody UserDetails userDetails)
	{
		
		int result = userRegistrationService.userRegistration(userDetails);
		if(result == 1)
		{
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"User Saved Successfully\",\"statuscode\":" + 1 + "}");
		}
		else if(result == -1)
		{
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"user with this email already exists\",\"statuscode\":" + -1 + "}");
		}
		else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Error saving user\",\"statuscode\":" + -2 + "}");
=======

	@Autowired
	private ConfirmationTokenDao confirmationTokenRepository;

	@Autowired
	private EmailSenderService emailSenderService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserDao userDao;

	/**
	 * This method is used for the user registration by providing the details of
	 * user
	 * 
	 * @param userDetails
	 * @return ResponseObject
	 */
	@RequestMapping(value = "/register", method = RequestMethod.POST)
//    @PostMapping("/register")
	public ResponseEntity<ResponseObject> registerUser(@RequestBody ConfirmationToken user) {

		ResponseObject responseObject = new ResponseObject();
		List<UserDetails> allUsers = userService.findAll();
		if (allUsers.size() != 0) {

			UserDetails existingUser = userDao.findByEmailIgnoreCase(user.getEmail());

			if (existingUser != null) {
				responseObject.setResponse("This email already exists!");
				responseObject.setStatuscode(-1);
				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);

			}

		}
		List<ConfirmationToken> confirm = emailSenderService.findAll();
		if (confirm.size() != 0) {
			ConfirmationToken userByEmail = confirmationTokenRepository.findByEmailIgnoreCase(user.getEmail());
			if (userByEmail != null) {

				if (!(userByEmail.isEnabled())) {

					confirmationTokenRepository.save(user);

					SimpleMailMessage mailMessage = new SimpleMailMessage();
					mailMessage.setTo(user.getEmail());
					mailMessage.setSubject("Complete Registration!");
					mailMessage.setFrom("questainno@gmail.com");
					mailMessage.setText("To confirm your account, please click here : "
							+ "http://localhost:8080/api/user/confirm-account?token=" + user.getConfirmationToken());

					emailSenderService.sendEmail(mailMessage);

					responseObject.setResponse("Email is sent to your mail.To activate your account, please verify your email.");
					responseObject.setStatuscode(1);
					return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
				} else {
					responseObject.setResponse("user profile already verified");
					responseObject.setStatuscode(-4);
					return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
				}

			}
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
		}
		confirmationTokenRepository.save(user);

		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setTo(user.getEmail());
		mailMessage.setSubject("Complete Registration!");
		mailMessage.setFrom("questainno@gmail.com");
		mailMessage.setText("To confirm your account, please click here : "
				+ "http://192.168.150.102:8080/api/user/confirm-account?token=" + user.getConfirmationToken());

		emailSenderService.sendEmail(mailMessage);

		responseObject.setResponse("Email is sent to your mail.To activate your account, please verify your email.");
		responseObject.setStatuscode(1);
		return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);

	}

	@RequestMapping(value = "/confirm-account", method = { RequestMethod.GET, RequestMethod.POST })
//    @PostMapping("/confirm-account")
	public ResponseEntity<ResponseObject> confirmUserAccount(@RequestParam("token") String confirmationToken) {
		ConfirmationToken token = confirmationTokenRepository.findByConfirmationToken(confirmationToken);
		ResponseObject responseObject = new ResponseObject();

		int result = 0;
		if (token != null) {
//            ConfirmationToken user = userDao.findByEmailIgnoreCase(token.getEmail());
			token.setEnabled(true);

			result = userService.userRegistration(token);
			responseObject.setResponse("accountVerified");
			responseObject.setStatuscode(2);
		} else if (result == -3) {
			responseObject.setResponse("user with this phone number already exists");
			responseObject.setStatuscode(-3);

			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		} else {
			responseObject.setResponse("The link is invalid or broken!");
			responseObject.setStatuscode(-2);

		}

		return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);

	}

	/**
	 * this method is used to display all users
	 * 
	 * @return ResponseObject
	 */

	@GetMapping("/getallusers")
	public ResponseEntity<ResponseObject> diplayUsers() {
		// Calling the method to fetch all user details
		List<UserDetails> userAll = userRegistrationService.findAll();

		List<UserResponse> responseList = new ArrayList<>();
		ResponseObject responseObject = new ResponseObject();
		if (!userAll.isEmpty()) {
			for (UserDetails userDetails : userAll) {
				// Setting the user details to the data transfer object
				UserResponse userResponse = new UserResponse();
				userResponse.setUser_id(userDetails.getUser_id());
				userResponse.setFirst_name(userDetails.getFirst_name());
				userResponse.setLast_name(userDetails.getLast_name());
				userResponse.setEmail((userDetails.getEmail()));
				userResponse.setPassword((userDetails.getPassword()));
				userResponse.setCity((userDetails.getCity()));
				userResponse.setState((userDetails.getState()));
				userResponse.setCountry(userDetails.getCountry());
				userResponse.setReligion(userDetails.getReligion());
				userResponse.setPhone(userDetails.getPhone());
				userResponse.setDateOfRegistration(userDetails.getDateOfRegistration());
				userResponse.setSecurityQuestion(userDetails.getSecurityQuestion());
				userResponse.setSecurityAnswer(userDetails.getSecurityAnswer());
				responseList.add(userResponse);
			}
			// setting the object to the response
			responseObject.setResponse(responseList);
			responseObject.setStatuscode(1);
			// returning the response object
			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);

		} else {
			responseObject.setResponse("NO USER TO DISPLAY");
			responseObject.setStatuscode(-1);

			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		}
<<<<<<< HEAD
		else
		{
			return  new ResponseEntity("No Questions found!!",HttpStatus.OK);
=======
	}

	/**
	 * this method is used to update details of particular user
	 * 
	 * @param user Details to update
	 * @return ResponseObject
	 */

	@PutMapping("/updateDetails")
	public ResponseEntity<ResponseObject> updateDetails(@RequestBody UserDetails userDetails,
			@RequestHeader HttpHeaders headers) {

		String token = "";
		ResponseObject responseObject = new ResponseObject();
		List<String> hList = headers.get("Authorization");
		if (hList == null) {
			responseObject.setResponse("invalid user");
			responseObject.setStatuscode(-3);

			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		}
		token = hList.get(0);
		if (JwtImpl.isValidUser(userDetails.getUser_id(), token)) {
			int result = userRegistrationService.updateDetails(userDetails);
			if (result == 1) {
				// setting the object to the response
				responseObject.setResponse(userDetails);
				responseObject.setStatuscode(1);
				// returning the response object
				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);

			}

			else if (result == -3) {
				// setting the object to the response
				responseObject.setResponse("Entered phone number already exists");
				responseObject.setStatuscode(-3);
				// returning the response object
				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			}

			else if (result == -1) {
				// setting the object to the response
				responseObject.setResponse("user details not found");
				responseObject.setStatuscode(-1);
				// returning the response object
				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			} else {
				// setting the object to the response
				responseObject.setResponse("No details to update.Data is same as existing");
				responseObject.setStatuscode(-2);
				// returning the response object
				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			}
		} else {
			responseObject.setResponse("invalid user");
			responseObject.setStatuscode(-3);

			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
		}
	}

	/**
	 * this method is used to display statistics of particular question based on
	 * city
	 * 
	 * @param question_id and voting details
	 * @return ResponseObject
	 */

	@PostMapping("/citystatistics/{id}")
	public ResponseEntity<ResponseObject> getStatsByCity(@RequestBody Voting voting, @RequestHeader HttpHeaders headers,
			@PathVariable int id) {

		// creating object for response object
		ResponseObject responseObject = new ResponseObject();
		String token = "";
		// authorization check
		List<String> hList = headers.get("Authorization");
		if (hList == null) {
			responseObject.setResponse("invalid user");
			responseObject.setStatuscode(-3);

			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		}
		token = hList.get(0);
		// returning response based on different conditions
		if (JwtImpl.isValidUser(id, token)) {
			// calling the method from dao
			List<com.pollingsystem.dto.Statistics> stats = userRegistrationService
					.getStatsBycity(voting.getQuestion().getQuestion_id());
			if (stats.size() != 0) {
				responseObject.setResponse(stats);
				responseObject.setStatuscode(1);
				// returning the response object
				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			} else {
				responseObject.setResponse("list is empty");
				responseObject.setStatuscode(-1);
				// returning the response object
				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			}
		} else {
			responseObject.setResponse("invalid user");
			responseObject.setStatuscode(-3);

			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		}
	}

	/**
	 * this method is used to display statistics of particular question based on
	 * state
	 * 
	 * @param question_id and voting details
	 * @return ResponseObject
	 */

	@PostMapping("/statestatistics/{id}")
	public ResponseEntity<ResponseObject> getStatsByState(@RequestBody Voting voting,
			@RequestHeader HttpHeaders headers, @PathVariable int id) {

		String token = "";
		// creating object for response object
		ResponseObject responseObject = new ResponseObject();
		List<String> hList = headers.get("Authorization");
		if (hList == null) {
			responseObject.setResponse("invalid user");
			responseObject.setStatuscode(-3);

			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		}
		token = hList.get(0);
		if (JwtImpl.isValidUser(id, token)) {
			// calling the method from dao
			List<com.pollingsystem.dto.Statistics> stats = userRegistrationService
					.getStatsByState(voting.getQuestion().getQuestion_id());
			responseObject = new ResponseObject();
			if (stats.size() != 0) {
				responseObject.setResponse(stats);
				responseObject.setStatuscode(1);
				// returning the response object
				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			} else {
				responseObject.setResponse("list is empty");
				responseObject.setStatuscode(-1);
				// returning the response object
				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			}
		} else {
			responseObject.setResponse("invalid user");
			responseObject.setStatuscode(-3);
			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		}
	}

	/**
	 * this method is used to display statistics of particular question based on
	 * country
	 * 
	 * @param question_id and voting details
	 * @return ResponseObject
	 */

	@PostMapping("/countrystatistics/{id}")
	public ResponseEntity<ResponseObject> getStatsByCountry(@RequestBody Voting voting,
			@RequestHeader HttpHeaders headers, @PathVariable int id) {

		// creating object for response object
		ResponseObject responseObject = new ResponseObject();
		String token = "";
		List<String> hList = headers.get("Authorization");
		if (hList == null) {
			responseObject.setResponse("invalid user");
			responseObject.setStatuscode(-3);

			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		}
		token = hList.get(0);
		if (JwtImpl.isValidUser(id, token)) {
			// calling the method from dao
			List<com.pollingsystem.dto.Statistics> stats = userRegistrationService
					.getStatsByCountry(voting.getQuestion().getQuestion_id());
			if (stats.size() != 0) {
				responseObject.setResponse(stats);
				responseObject.setStatuscode(1);
				// returning the response object
				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			} else {
				responseObject.setResponse("list is empty");
				responseObject.setStatuscode(-1);
				// returning the response object
				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			}
		} else {
			responseObject.setResponse("invalid user");
			responseObject.setStatuscode(-3);
			// returning the response object
			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		}
	}

	/**
	 * this method is used to display statistics of particular question based on
	 * country
	 * 
	 * @param question_id and voting details
	 * @return ResponseObject
	 */

	@PostMapping("/religionstatistics/{id}")
	public ResponseEntity<ResponseObject> getStatsByReligion(@RequestBody Voting voting,
			@RequestHeader HttpHeaders headers, @PathVariable int id) {

		// creating object for response object
		ResponseObject responseObject = new ResponseObject();
		String token = "";
		List<String> hList = headers.get("Authorization");
		if (hList == null) {
			responseObject.setResponse("invalid user");
			responseObject.setStatuscode(-3);

			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		}
		token = hList.get(0);
		if (JwtImpl.isValidUser(id, token)) {
			// calling the method from dao
			List<com.pollingsystem.dto.Statistics> stats = userRegistrationService
					.getStatsByReligion(voting.getQuestion().getQuestion_id());
			if (stats.size() != 0) {
				responseObject.setResponse(stats);
				responseObject.setStatuscode(1);
				// returning the response object
				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			} else {
				responseObject.setResponse("list is empty");
				responseObject.setStatuscode(-1);
				// returning the response object
				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			}
		} else {
			responseObject.setResponse("invalid user");
			responseObject.setStatuscode(-3);
			// returning the response object
			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		}
	}

	
}
