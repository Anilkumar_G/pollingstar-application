package com.pollingsystem.controller;

<<<<<<< HEAD

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
=======
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pollingsystem.entity.Voting;
import com.pollingsystem.service.VotingService;
<<<<<<< HEAD

@RestController
@RequestMapping("/api/voting")
public class VotingController {
	
	@Autowired
	private VotingService votingService;
	@PostMapping("/add")
	public void addVoting(@RequestBody Voting voting) {
		votingService.votingForAnswer(voting);
		
	}
	

=======
import com.pollingsystem.util.JwtImpl;
import com.pollingsystem.util.ResponseObject;

/**
 * This class is used to provide api's for voting functionalities
 * 
 * @author IMVIZAG
 *
 */
@CrossOrigin("*")
@RestController
@RequestMapping("/api/voting")
public class VotingController {

	@Autowired
	private VotingService votingService;


	/**
	 * This method is used to cast the vote for particular answer by the user
	 * 
	 * @param voting
	 * @return
	 */
	@PostMapping("/add")
	public ResponseEntity<ResponseObject> addVoting(@RequestBody Voting voting, @RequestHeader HttpHeaders headers) {
		
		//creating object for response object
		ResponseObject responseObject = new ResponseObject();
		String token = "";
		List<String> hList = headers.get("Authorization");
		if (hList == null) {
			responseObject.setResponse("invalid user");
			responseObject.setStatuscode(-3);

			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		}
		token = hList.get(0);

		if (JwtImpl.isValidUser(voting.getUserdetails().getUser_id(), token)) {
			// Checking condition for returning different responses
			if (votingService.votingForAnswer(voting) == 1) {
				responseObject.setResponse("User Voted for answer Successfully");
				responseObject.setStatuscode(1);

				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			}

		 else if (votingService.votingForAnswer(voting) == -1) {
				responseObject.setResponse("User choice for answer updated Successfully");
				responseObject.setStatuscode(2);

				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			}

			else {
				responseObject.setResponse("Voting failed");
				responseObject.setStatuscode(-1);

				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			}

		}
		responseObject.setResponse("invalid user");
		responseObject.setStatuscode(-3);

		return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
	}
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
}
