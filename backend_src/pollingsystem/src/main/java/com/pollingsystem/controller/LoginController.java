package com.pollingsystem.controller;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pollingsystem.dao.UserDao;
import com.pollingsystem.dto.UserResponse;
import com.pollingsystem.entity.UserDetails;
import com.pollingsystem.service.LoginService;
import com.pollingsystem.service.UserService;
import com.pollingsystem.util.JwtImpl;
import com.pollingsystem.util.PasswordEncryption;
import com.pollingsystem.util.ResponseObject;

/**
 * This class is used to provide api's for Login Functionalities
 * 
 * @author IMVIZAG
 *
 */
@RestController
@Transactional
@CrossOrigin(origins = "*")
@RequestMapping("/api/auth")
public class LoginController {

	@Autowired
	private LoginService loginService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserDao userDao;

	/**
	 * This method is used to login by the user using email and passwsord
	 * 
	 * @param userDetails
	 * @return ResponseObject
	 */

	@SuppressWarnings("deprecation")
	@PostMapping("/login")
<<<<<<< HEAD
	public ResponseEntity<?> login(@RequestBody UserDetails userDetails)
	{
		int result=loginService.userLogin(userDetails);
		
		if(result==1)
		{
//			return ResponseEntity.ok().body("Login Successful");
			
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"LOGIN SUCCESSFUL\",\"statuscode\":" + 1 + "}");
					
		}
		else if(result==2)
		{
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"EMAIL_DOES_NOT_EXIST\",\"statuscode\":" + -1+ "}");
		}
		
		return ResponseEntity.status(HttpStatus.OK)
				.body("{\"status\":\"PASSWORD_EMAIL_MISMATCH\",\"statuscode\":" + -2+ "}");
=======
	public ResponseEntity<ResponseObject> login(@RequestBody UserDetails userDetails) {
		int count = 0;
		int userId=0;
		List<UserDetails> result = userService.findAll();
		Iterator<UserDetails> iterator = result.iterator();
		ResponseObject responseObject = new ResponseObject();
		// UserDetails DetailsForStatus =
		// loginService.findByEmail(userDetails.getEmail());
		while (iterator.hasNext()) {

			UserDetails dbUserDetails = (UserDetails) iterator.next();
			PasswordEncryption passwordEncryption = new PasswordEncryption();
			String password = passwordEncryption.getDecryptedPassword(dbUserDetails.getPassword());

			// Setting the values from database to data transfer object if list is not empty
			if ((userDetails.getEmail().equalsIgnoreCase(dbUserDetails.getEmail()))&& (userDetails.getPassword().equals(password))) {

				userId=dbUserDetails.getUser_id();

				Date date = new Date();
				//This method returns the time in millis
				int timeMilli = date.getMinutes();
				int lastAttempt=0;
				if(dbUserDetails.getLastAttempt()!=null)
				{
					lastAttempt = dbUserDetails.getLastAttempt().getMinutes();
				}

				long differenceTime=(timeMilli-lastAttempt);
				if(dbUserDetails.getLoginCount()>=2 && (differenceTime<=10) && (date.getHours()-dbUserDetails.getLastAttempt().getHours())==0)
				{					  
					responseObject.setResponse("Account blocked for 10 minutes");
					responseObject.setStatuscode(-3);
					return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);					
				}

				else
				{
					UserDetails updateUserDetails = userService.findById(dbUserDetails.getUser_id());
					updateUserDetails.setLoginCount(0);
					userDao.save(updateUserDetails);

					UserResponse userResponse = new UserResponse();
					userResponse.setUser_id(dbUserDetails.getUser_id());
					userResponse.setFirst_name(dbUserDetails.getFirst_name());
					userResponse.setLast_name(dbUserDetails.getLast_name());
					userResponse.setEmail((dbUserDetails.getEmail()));
					userResponse.setPassword((dbUserDetails.getPassword()));
					userResponse.setCity((dbUserDetails.getCity()));
					userResponse.setState((dbUserDetails.getState()));
					userResponse.setCountry(dbUserDetails.getCountry());
					userResponse.setReligion(dbUserDetails.getReligion());
					userResponse.setPhone(dbUserDetails.getPhone());
					userResponse.setDateOfRegistration(dbUserDetails.getDateOfRegistration());
					userResponse.setSecurityQuestion(dbUserDetails.getSecurityQuestion());
					userResponse.setSecurityAnswer(dbUserDetails.getSecurityAnswer());
					String id = "" + dbUserDetails.getUser_id();

					// if valid user generating token for that user
					String token = JwtImpl.createJWT(id);
					responseObject.setResponse(userResponse);
					responseObject.setStatuscode(1);
					responseObject.setToken(token);

					return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);

				}				
			} 
			else if ((userDetails.getEmail().equalsIgnoreCase(dbUserDetails.getEmail()))) 
			{
				count = 2;
				userId=dbUserDetails.getUser_id();
			}
		}
		// Checking conditions to return different responses
		if (count != 2) {

			responseObject.setResponse("EMAIL_DOES_NOT_EXIST");
			responseObject.setStatuscode(-1);

			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);

		} else {

			UserDetails details = userService.findById(userId);
			userId=details.getUser_id();
			//Getting the current date
			Date date = new Date();
			//This method returns the time in millis
			long timeMilli = date.getMinutes();
			long lastAttempt=date.getMinutes();
			if(details.getLastAttempt()!=null)
			{
				lastAttempt = details.getLastAttempt().getMinutes();
			}

			long differenceTime=(timeMilli-lastAttempt);			
			
			if(details.getLoginCount()>2 && (differenceTime<=10) && (date.getHours()-details.getLastAttempt().getHours())==0)
			{			
				responseObject.setResponse("Account blocked for 10 minutes");
				responseObject.setStatuscode(-3);
				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			}	
			else
			{
				userService.incrementAttempts(userId);
				responseObject.setResponse("Password_email_mismatch");
				responseObject.setStatuscode(-2);
				return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
			}


		}

>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
	}

	/**
	 * This method is to check security question and answer for the particular user who uses forget password option
	 * 
	 * @param userDetails
	 * @return ResponseObject
	 */
	@PostMapping("/forgetPassword")
<<<<<<< HEAD
	public ResponseEntity<String> forgetPassword(@RequestBody UserDetails userDetails) {
		ResponseEntity<String> result = null;
		if (loginService.securityAuthentication(userDetails.getEmail(), userDetails.getSecurityQuestion(),
				userDetails.getSecurityAnswer())) {
			
			result = ResponseEntity.status(HttpStatus.NOT_MODIFIED)
					.body("{\"status\":\"Your security credentials are correct\",\"statuscode\":" + 1 +userDetails.getEmail()+ "}");

		} else {
			result = ResponseEntity.status(HttpStatus.NOT_MODIFIED)
					.body("{\"status\":\"Security Credentials are Not Matched\",\"statuscode\":" + -1 + "}");
			
		}
		return result;
}
	/**
	 * This method set the password user who forgets the password
	 * @param userDetails
	 * @return
	 */
	
	@PutMapping("/forgetUpdatePassword")
	public ResponseEntity<String> forgetUpdatePassword(@RequestBody UserDetails userDetails) {
		ResponseEntity<String> result = null;
		if(loginService.resetPassword(userDetails.getEmail(),userDetails.getPassword())) {
			result = ResponseEntity.status(HttpStatus.NOT_MODIFIED)
					.body("{\"status\":\"Password updated successfully \",\"statuscode\":" + 1 + "}");
		} else {
			result = ResponseEntity.status(HttpStatus.NOT_MODIFIED)
					.body("{\"status\":\"Password updated\",\"statuscode\":" + -1 + "}");
		}
		return result;
	}
	
=======
	public ResponseEntity<ResponseObject> securityCheck(@RequestBody UserDetails userDetails) {

		ResponseObject responseObject = new ResponseObject();
		int result = loginService.securityAuthentication(userDetails.getEmail(), userDetails.getSecurityQuestion(),
				userDetails.getSecurityAnswer());

		// Checking conditions to return different responses
		if (result == 1) {

			responseObject.setResponse(userDetails.getEmail());
			responseObject.setStatuscode(1);

			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);

		} else if(result == 3){
			responseObject.setResponse("Security Credentials are Not Matched");
			responseObject.setStatuscode(-1);

			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);

		}else {

			responseObject.setResponse("User with this email does not exists");
			responseObject.setStatuscode(-2);

			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);

		}
	}

	/**
	 * This method set the password user who forgets the password
	 * 
	 * @param userDetails
	 * @return ResponseObject
	 */

	@PutMapping("/forgetUpdatePassword")
	public ResponseEntity<ResponseObject> forgetPassword(@RequestBody UserDetails userDetails) {

		ResponseObject responseObject = new ResponseObject();
		// Checking conditions to return different responses
		if (loginService.forgetPassword(userDetails.getEmail(), userDetails.getPassword())) {
			responseObject.setResponse("Password updated successfully");
			responseObject.setStatuscode(1);

			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		} else {
			responseObject.setResponse("Password updation failed");
			responseObject.setStatuscode(-1);

			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		}
	}

>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
	/**
	 * This method reset's the user password
	 * 
	 * @param userDetails
	 * @return ResponseObject
	 */
	@PutMapping("/resetPassword")
<<<<<<< HEAD
	public ResponseEntity<String> resetPassword(@RequestBody UserDetails userDetails) {
		ResponseEntity<String> result = null;
		if(loginService.resetNewPassword(userDetails.getEmail(),userDetails.getOldPassword(),userDetails.getPassword())) {
			result = ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Password Changed SUCCESSFUL\",\"statuscode\":" + 1 + "}");
=======
	public ResponseEntity<ResponseObject> resetPassword(@RequestBody UserDetails userDetails) {

		ResponseObject responseObject = new ResponseObject();
		// Checking conditions to return different responses
		if (loginService.resetNewPassword(userDetails.getEmail(), userDetails.getOldPassword(),
				userDetails.getPassword())) {
			responseObject.setResponse("Password Changed SUCCESSFUL");
			responseObject.setStatuscode(1);

			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
>>>>>>> 7e53cb521ec4e3364770df71e155f54f556d065e
		} else {
			responseObject.setResponse("Enter New Password");
			responseObject.setStatuscode(-1);

			return new ResponseEntity<ResponseObject>(responseObject, HttpStatus.OK);
		}

	}


}
