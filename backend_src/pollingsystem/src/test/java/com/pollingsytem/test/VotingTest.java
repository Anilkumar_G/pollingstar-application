package com.pollingsytem.test;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.pollingsystem.dao.AnswerDao;
import com.pollingsystem.dao.VotingDao;
import com.pollingsystem.entity.AnswerEntity;
import com.pollingsystem.entity.Question;
import com.pollingsystem.entity.UserDetails;
import com.pollingsystem.entity.Voting;
import com.pollingsystem.service.VotingService;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class VotingTest {
	
	@InjectMocks
	private VotingService votingService;
	@Mock
	private VotingDao votingDao;
	
	@Mock
	private AnswerDao answerDao;
	
	@Test
	public void testVoting() {
		
		UserDetails userDetails = new UserDetails();
		userDetails.setUser_id(1);
		
		Question question = new Question();
		question.setQuestion_id(1);
		
		AnswerEntity answer = new AnswerEntity();
		answer.setAnswerId(1);
		answer.setNoOfVotes(1);
		
		Voting voting =  new Voting();
		voting.setVoting_id(1);
		voting.setUserdetails(userDetails);
		voting.setQuestion(question);
		voting.setAnswer(answer);
		
		voting.setDate(new Date(System.currentTimeMillis()));
		
		List<Voting> votingList=new ArrayList<Voting>();
		
		UserDetails userDetails1 = new UserDetails();
		userDetails1.setUser_id(1);
		
		Question question1 = new Question();
		question1.setQuestion_id(1);
		
		AnswerEntity answer1 = new AnswerEntity();
		answer1.setAnswerId(1);
		answer1.setNoOfVotes(1);
		
		Voting voting1 =  new Voting();
		voting1.setVoting_id(1);
		voting1.setUserdetails(userDetails);
		voting1.setQuestion(question);
		voting1.setAnswer(answer);
		
		voting1.setDate(new Date(System.currentTimeMillis()));
		
		votingList.add(voting1);
		
		AnswerEntity answerEntity = new AnswerEntity();
		answerEntity.setAnswerId(1);
		answerEntity.setNoOfVotes(1);
		answerEntity.setQuestion(question1);
		answerEntity.setUserDetails(userDetails1);
		
		when(votingDao.findAll()).thenReturn(votingList);
		
		when(answerDao.findId(voting.getAnswer().getId(),
				voting.getQuestion().getQuestion_id())).thenReturn(answerEntity);
		
		when(votingDao.findById(voting1.getVoting_id())).thenReturn(Optional.of(voting1));
		
		when(votingDao.findById(voting1.getVoting_id())).thenReturn(Optional.of(voting1));
		
		
		assertEquals(-1,votingService.votingForAnswer(voting));
		
	}

}