package com.pollingsytem.test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.pollingsystem.dao.ConfirmationTokenDao;
import com.pollingsystem.dao.UserDao;
import com.pollingsystem.dto.Statistics;
import com.pollingsystem.entity.ConfirmationToken;
import com.pollingsystem.entity.Question;
import com.pollingsystem.entity.UserDetails;
import com.pollingsystem.service.UserService;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class UserTest {

	@InjectMocks
	private UserService service;
	
	@Mock
	private UserDao dao;
	
	@Mock
	private ConfirmationTokenDao token;
	

    @Test
    public void testUserRegistration() {
    	
    	ConfirmationToken confirm=new ConfirmationToken();
    	
    	confirm.setTokenid(1);
    	confirm.setConfirmationToken("11hagsgh");
    	confirm.setEmail("varungowtham12121@gmail.com");
    	confirm.setFirst_name("varun");
    	confirm.setLast_name("t");
    	confirm.setPassword("Varun@123");
    	confirm.setCity("hyd");
    	confirm.setState("ts");
    	confirm.setCountry("india");
    	confirm.setPhone("9700675454");
    	confirm.setSecurityAnswer("what is your pet name");
    	confirm.setSecurityQuestion("varun");
    	
    	UserDetails user = new UserDetails();
		
		user.setFirst_name(confirm.getFirst_name());
		user.setLast_name(confirm.getLast_name());
		user.setEmail(confirm.getEmail());
		user.setPassword(confirm.getPassword());
		user.setCity(confirm.getCity());
		user.setState(confirm.getState());
		user.setCountry(confirm.getCountry());
		user.setPhone(confirm.getPhone());
		user.setSecurityAnswer(confirm.getSecurityAnswer());
		user.setSecurityQuestion(confirm.getSecurityQuestion());
		
		UserDetails user1 = new UserDetails();
		
		user1.setEmail("varungowtham20@gmail.com");
		user1.setPhone("9876543210");
		
		List<UserDetails> userList = new ArrayList<>();
		userList.add(user1);
		
		when(token.findByEmailIgnoreCase(confirm.getEmail())).thenReturn(confirm);
		
		when(dao.findAll()).thenReturn(userList);
		
		assertEquals(1,service.userRegistration(confirm));

    }
    @Test
    public void testFindAll() {
    	List<UserDetails> users=new ArrayList<UserDetails>();
    	
    	UserDetails user = new UserDetails();
		
		user.setFirst_name("varun");
		user.setLast_name("gowtham");
		user.setEmail("varungowtham20@gmail.com");
		user.setPassword("Var@150620");
		user.setCity("Hyderabad");
		user.setState("telangana");
		user.setCountry("india");
		user.setPhone("9700675148");
		user.setSecurityAnswer("agag");
		user.setSecurityQuestion("what");
		users.add(user);
		UserDetails user1 = new UserDetails();
		
		user.setFirst_name("varun");
		user.setLast_name("gowtham");
		user.setEmail("varungowtham201@gmail.com");
		user.setPassword("Var@150620");
		user.setCity("Hyderabad");
		user.setState("telangana");
		user.setCountry("india");
		user.setPhone("9700675148");
		user.setSecurityAnswer("agag");
		user.setSecurityQuestion("what");
		users.add(user1);
		
    	when(dao.findAll()).thenReturn(users);
    	assertFalse(service.findAll().isEmpty());
    	
    }
	@Test
	public void findByIdTest() {
		
		UserDetails user1 = new UserDetails();
		user1.setUser_id(1);
		
		Mockito.<Optional<UserDetails>>when(dao.findById(1)).thenReturn(Optional.of(user1));
		assertNotNull(service.findById(1));
	}
	@Test
	public void updateDetailsTest() {
		List<UserDetails> users=new ArrayList<UserDetails>();
		
		UserDetails user = new UserDetails();
		user.setUser_id(1);
		user.setFirst_name("varun");
		user.setLast_name("gowtham");
		user.setCity("Hyderabad");
		user.setState("telangana");
		user.setCountry("india");
		user.setPhone("9700675148");
		user.setReligion("hindu");
		users.add(user);
		

		UserDetails user1 = new UserDetails();
		user1.setUser_id(1);
		user1.setFirst_name("var");
		user1.setLast_name("gowtham");
		user1.setCity("Hyderabad");
		user1.setState("telangana");
		user1.setCountry("india");
		user1.setPhone("9700675148");
		user.setReligion("hindu");
		
		
		Mockito.<Optional<UserDetails>>when(dao.findById(user1.getUser_id())).thenReturn(Optional.of(user));
		
		when(dao.findAll()).thenReturn(users);

		
		when(dao.save(user)).thenReturn(user);
		
		assertEquals(1,service.updateDetails(user1));
	}
	   @Test
	    public void testStatsByCity() {
		   
			List<Statistics> users=new ArrayList<Statistics>();
			Statistics stats=new Statistics("hyderabad",5);
			users.add(stats);
			when(dao.getStatsBycity(1)).thenReturn(users);
			assertFalse(service.getStatsBycity(1).isEmpty());
			
	   }

	   @Test
	    public void testStatsByState() {
		   
			List<Statistics> users=new ArrayList<Statistics>();
			Statistics stats=new Statistics("telangana",5);
			users.add(stats);
			when(dao.getStatsByState(1)).thenReturn(users);
			assertFalse(service.getStatsByState(1).isEmpty());
			
	   }

	   @Test
	    public void testStatsByCountry() {
		   
			List<Statistics> users=new ArrayList<Statistics>();
			Statistics stats=new Statistics("India",5);
			users.add(stats);
			when(dao.getStatsByCountry(1)).thenReturn(users);
			assertFalse(service.getStatsByCountry(1).isEmpty());
			
	   }
	   @Test
	    public void testStatsByReligion() {
		   
			List<Statistics> users=new ArrayList<Statistics>();
			Statistics stats=new Statistics("hindu",5);
			users.add(stats);
			when(dao.getStatsByReligion(1)).thenReturn(users);
			assertFalse(service.getStatsByReligion(1).isEmpty());
			
	   }

}