package com.pollingsytem.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.pollingsystem.dao.QuestionDao;
import com.pollingsystem.dao.UserDao;
import com.pollingsystem.dto.QuestionResponse;
import com.pollingsystem.entity.AnswerEntity;
import com.pollingsystem.entity.Question;
import com.pollingsystem.entity.UserDetails;
import com.pollingsystem.entity.Voting;
import com.pollingsystem.service.QuestionService;
import com.pollingsystem.service.UserService;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class QuestionTest {

	@InjectMocks
	QuestionService questionService;
	
	@Mock
	QuestionDao questionDao;
	
	@InjectMocks
	UserService userService;
	
	@Mock
	UserDao userDao;
	
	@Test
	public void addQuestionTest()
	{
		UserDetails details = new UserDetails();
		details.setUser_id(1);
		
		List<Question> questions=new ArrayList<Question>();
		Question question = new Question();
		question.setQuestion("What is your name?");
		question.setCategory("Anonymous");
		question.setDateOfCreation(new Date());
		question.setNumberOfVotes(0);
		question.setQuestion_id(1);
		question.setUserDetails(details);
		questions.add(question);
		
		details.setQuestion_details(questions);
		when(userDao.findById(question.getUserDetails().getUser_id())).thenReturn(Optional.of(details));
		when(questionDao.save(question)).thenReturn(question);
		assertEquals(1, questionService.addQuestion(question));
	}

	@Test
	public void findAllTest()
	{
		List<Question> questions = new ArrayList<Question>();
		Question question = new Question();
		question.setQuestion("What is your name?");
		questions.add(question);
		when(questionDao.findAll()).thenReturn(questions);
		assertFalse(questionService.findAll().isEmpty());
	}
	
	@Test
	public void displayUserQuestionsTest()
	{
		List<Question> questions = new ArrayList<Question>();
		Question question = new Question();
		question.setQuestion("What is your name?");
		questions.add(question);
		when(questionDao.findUserQuestions(1)).thenReturn(questions);
		assertFalse(questionService.diplayUserQuestions(1).isEmpty());
	}
	
	@Test
	public void diplayRecentQuestionsTest()
	{
		List<Question> questions = new ArrayList<Question>();
		Question question = new Question();
		question.setQuestion("What is your name?");
		questions.add(question);
		when(questionDao.findAllOrderBydateOfCreationDesc()).thenReturn(questions);
		assertFalse(questionService.diplayRecentQuestions().isEmpty());
	}
	
	@Test
	public void displayCategoryQuestionsTest()
	{
		List<Question> questions = new ArrayList<Question>();
		Question question = new Question();
		question.setQuestion("What is your name?");
		questions.add(question);
		when(questionDao.findAllCategoryQuestions("Anonymous")).thenReturn(questions);
		assertFalse(questionService.displayCategoryQuestions("Anonymous").isEmpty());
	}
	
	@Test
	public void diplayTrendingQuestionsTest()
	{
		List<Voting> votings = new ArrayList<Voting>();
		Voting voting = new Voting();
		voting.setVoting_id(1);
		
		AnswerEntity answerEntity = new AnswerEntity();
		answerEntity.setAnswer("Varun");
		voting.setAnswer(answerEntity);
		Question question = new Question();
		question.setQuestion("What is your name?");
		List<AnswerEntity> answers = new ArrayList<>();
		
		List<Question> questions = new ArrayList<>();
		question.setAnswers(answers);
		voting.setQuestion(question);
		UserDetails userDetails = new UserDetails();
		userDetails.setUser_id(1);
		userDetails.setFirst_name("varun");
		answerEntity.setUserDetails(userDetails);
		question.setUserDetails(userDetails);
		answers.add(answerEntity);
		voting.setUserdetails(userDetails);
		voting.setDate(new java.sql.Date(System.currentTimeMillis()));
		votings.add(voting);
		questions.add(question);
		when(questionDao.trendingQuestions()).thenReturn(votings);
		when(questionDao.findAllNoOfVotes()).thenReturn(questions);
		assertFalse(questionService.diplayTrendingQuestions().isEmpty());
	}
	
	@Test
	public void displaySearchedQuestionsTest()
	{
		List<Question> questions = new ArrayList<Question>();
		Question question = new Question();
		question.setQuestion("What is your name?");
		questions.add(question);
		when(questionDao.searchQuestions("what")).thenReturn(questions);
		assertFalse(questionService.displaySearchedQuestions("what").isEmpty());
	}
	
	@Test
	public void findByIdTest() {
		
		Question question = new Question();
		question.setQuestion("What is your name?");
		question.setCategory("Anonymous");
		question.setDateOfCreation(new Date());
		question.setNumberOfVotes(0);
		question.setQuestion_id(1);
		Mockito.<Optional<Question>>when(questionDao.findById(1)).thenReturn(Optional.of(question));
		assertNotNull(questionService.findById(1));
	}
}
