package com.pollingsytem.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.pollingsystem.dao.AnswerDao;
import com.pollingsystem.dao.QuestionDao;
import com.pollingsystem.dao.UserDao;
import com.pollingsystem.entity.AnswerEntity;
import com.pollingsystem.entity.Question;
import com.pollingsystem.entity.UserDetails;
import com.pollingsystem.service.AnswerService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class AnswerTest {

	@Mock
	public AnswerDao answerDao;
	@InjectMocks
	public AnswerService answerService;

	@Mock
	UserDao userDao;
	@Mock
	public QuestionDao questionDao;
	@Test
	public void addAnswerTest() {
		
		AnswerEntity answerObject = new AnswerEntity();
		answerObject.setAnswer("asha020502");
		answerObject.setAnswerId(1);
		answerObject.setNoOfVotes(3);
		
		UserDetails user = new UserDetails();
		user.setUser_id(101);
		Question question = new Question();
		question.setQuestion_id(1);
		question.setQuestion("who is a donkey?");
		List<AnswerEntity> answers = new ArrayList<>();
		
		answerObject.setQuestion(question);
		answerObject.setUserDetails(user);
		question.setAnswers(answers);
		user.setAnswer_details(answers);
		when(answerDao.save(answerObject)).thenReturn(answerObject);
		when(userDao.findById(answerObject.getUserDetails().getUser_id())).thenReturn(Optional.of(user));
		when(questionDao.findById(answerObject.getQuestion().getQuestion_id())).thenReturn(Optional.of(question));
		assertEquals(1,answerService.addAnswer(answerObject));
	}
	
	@Test
	public void findByIdTest() {
		
		AnswerEntity answerObject = new AnswerEntity();
		answerObject.setAnswer("asha");
		answerObject.setAnswerId(101);
		answerObject.setNoOfVotes(3);
		Question question = new Question();
		question.setQuestion_id(1);
		answerObject.setQuestion(question);
		Mockito.<Optional<AnswerEntity>>when(answerDao.findById(101)).thenReturn(Optional.of(answerObject));
		assertNotNull(answerService.findById(101));
	}
	
	@Test
	public void findAllTest() {
		
		List<AnswerEntity> answers = new ArrayList<AnswerEntity>();
		AnswerEntity answerObject = new AnswerEntity();
		answerObject.setAnswer("asha");
		
		answers.add(answerObject);
		
		when(answerDao.findAll()).thenReturn(answers);
		assertFalse(answerService.findAll().isEmpty());
	}
}
