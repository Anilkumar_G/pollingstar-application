//Component Dependencies
import * as allActions          from '../actions/actionConstants';

export function signUpUser (data) {
    return { 
        type:allActions.SIGNUP_USER,
        payload : data 
    };    
}

export function signUpUserSuccess (data) {
    return {
        type : allActions.SIGNUP_USER_SUCCESS,
        payload : data
    };
}
