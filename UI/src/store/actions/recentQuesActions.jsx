import * as allActions from './actionConstants';

export function fetchRecentQuestions () {
    return { 
        type:allActions.FETCH_RECENT_QUESTIONS
    };
}

export function receiveRecentQuestions (data) {
    return {
        type : allActions.RECEIVE_RECENT_QUESTIONS,
        payload : data
    };
}
