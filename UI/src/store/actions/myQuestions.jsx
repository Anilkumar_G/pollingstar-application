import * as allActions from './actionConstants';

export function fetchUserQuestions () {
    return { 
        type:allActions.FETCH_USER_QUESTIONS,
    };
}

export function receiveUserQuestions (data) {
    return {
        type : allActions.RECEIVE_USER_QUESTIONS,
        payload : data
    };
}