import * as allActions from '../actions/actionConstants';

export function doLogInUser (data) {
    return { 
        type:allActions.DO_LOGIN_USER,
        payload : data 
    };
}

export function logInUserSuccess (data) {
    return {
        type : allActions.LOGIN_USER_SUCCESS,
        payload : data
    };
}

export function logOutUser () {
    return {
        type : allActions.LOGOUT_USER,
        payload : {}
    };
}

/**
 * methods for security question check
 */

export function SecurityQuestionCheck (data) {
    console.log("haii Action")
    return { 
        type:allActions.SECURITY_QUESTION_CHECK,
        payload : data 
    };
}

export function SecurityQuestionSucess(data) {
    return { 
        type:allActions. SECURITY_QUESTION_SUCCES,
        payload : data 
    };
}

export function SecurityQuestionError(data) {
    return { 
        type:allActions.SECURITY_QUESTION_ERROR,
        payload : data 
    };
}


/**
 * forgot update password methods
 */
export function FORGET_UPDATE_PASSWORD (data) {
    return { 
        type:allActions.FORGET_UPDATE_PASSWORD,
        payload : data 
    };
}

export function FORGET_UPDATE_PASSWORD_SUCESS(data) {
    return { 
        type:allActions.FORGET_UPDATE_PASSWORD_SUCESS,
        payload : data 
    };
}
export function FORGET_UPDATE_PASSWORD_ERROR(data) {
    return { 
        type:allActions.FORGET_UPDATE_PASSWORD_ERROR,
        payload : data 
    };
}