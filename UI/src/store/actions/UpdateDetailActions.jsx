import * as allActions              from '../actions/actionConstants';

export function updateDetails (data) {
    return { 
        type:allActions.UPDATE_DETAILS,
        payload : data 
    };
}

export function updateDetailsSuccess(data) {
    return { 
        type:allActions.UPDATE_DETAILS_SUCCESS,
        payload : data 
    };
}
