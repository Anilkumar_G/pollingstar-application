import * as allActions from './actionConstants';

export function fetchQuestions () {
    return { 
        type:allActions.FETCH_ALL_QUESTIONS
    };
}

export function receiveQuestions (data) {
    return {
        type : allActions.RECEIVE_ALL_QUESTIONS,
        payload : data
    };
}
