import * as allActions from './actionConstants';

export function fetchTrendingQuestions () {
    return { 
        type:allActions.FETCH_TRENDING_QUESTIONS
    };
}

export function receiveTrendingQuestions (data) {
    return {
        type : allActions.RECEIVE_TRENDING_QUESTIONS,
        payload : data
    };
}
