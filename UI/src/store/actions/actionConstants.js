
/**
 * Action constants for Login Scenario
*/

export const DO_LOGIN_USER = "DO_LOGIN_USER";
export const LOGIN_USER_SUCCESS = "LOGIN_USER_SUCCESS";
export const LOGIN_USER_DATA_ERROR = 'LOGIN_USER_DATA_ERROR';

/**
 * Action constants for LogOut Scenario
*/

export const LOGOUT_USER  = "LOGOUT_USER";

/**
 * Action constants for SignUp Scenario 
*/

export const SIGNUP_USER = "SIGNUP_USER";
export const SIGNUP_USER_SUCCESS = "SIGNUP_USER_SUCCESS";

/*
 * Action constant for security question checking
 */
export const SECURITY_QUESTION_CHECK = "SECURITY_QUESTION_CHECK";
export const SECURITY_QUESTION_SUCCES = "SECURITY_QUESTION_SUCCESS";
export const SECURITY_QUESTION_ERROR = "SECURITY_QUESTION_ERROR";

/**
 * 
 */

export const FORGET_UPDATE_PASSWORD ="FORGET_UPDATE_PASSWORD";
export const FORGET_UPDATE_PASSWORD_SUCESS="FORGET_UPDATE_PASSWORD_SUCESS";
export const FORGET_UPDATE_PASSWORD_ERROR ="FORGET_UPDATE_PASSWORD_ERROR";


/**
 * Action constants for Posting Questions
 */

 export const ADD_QUESTION = "ADD_QUESTION";
 export const QUESTION_ADDED = "QUESTION_ADDED";

/**
 * Action constants for fetching all Questions
 */

 export const FETCH_ALL_QUESTIONS = "FETCH_ALL_QUESTIONS";
 export const RECEIVE_ALL_QUESTIONS = "RECEIVE_ALL_QUESTIONS";

/**
 * Action constants for fetching Recent Questions
 */

 export const FETCH_RECENT_QUESTIONS = "FETCH_RECENT_QUESTIONS";
 export const RECEIVE_RECENT_QUESTIONS = "RECEIVE_RECENT_QUESTIONS";


/**
 * Action constants for fetching Trendding Questions
 */

export const FETCH_TRENDING_QUESTIONS = "FETCH_TRENDING_QUESTIONS";
export const RECEIVE_TRENDING_QUESTIONS = "RECEIVE_TRENDING_QUESTIONS";

/**
 * Action Constants for Adding answers
 */
export const ADD_ANSWER = "ADD_ANSWER";
export const ADD_ANSWER_SUCCESS = "ADD_ANSWER_SUCCESS"; 

/**
 * Action Constants for Voting answers
 */

export const ADD_VOTE = "ADD_VOTE";
export const ADD_VOTE_SUCCESS = "ADD_VOTE_SUCCESS";

/**
 * Action constants for fetching Trendding Questions
 */

export const FETCH_CATEGORY_QUESTIONS = "FETCH_CATEGORY_QUESTIONS";
export const RECEIVE_CATEGORY_QUESTIONS = "RECEIVE_TRENDING_QUESTIONS";


/**
 * Action constants for fetching USER Questions
 */

export const FETCH_USER_QUESTIONS = "FETCH_USER_QUESTIONS";
export const RECEIVE_USER_QUESTIONS = "RECEIVE_USER_QUESTIONS";

/**
 * Action constants for Seaching Questions
 */

export const SEARCH_QUESTION = "SEARCH_QUESTION";
export const SEARCH_QUESTION_SUCESS = "SEARCH_QUESTION_SUCESS";
export const SEARCH_QUESTION_ERROR = "SEARCH_QUESTION_ERROR";

/**
 * Action Constants for Update User Details
 */
export const UPDATE_DETAILS = "UPDATE_DETAILS";
export const UPDATE_DETAILS_SUCCESS = "UPDATE_DETAILS_SUCCESS";
/**
 * Action constants for Cityanalatics 
 */
export const FETCH_ANALATICS_DATA = "FETCH_ANALATICS_DATA";
export const ANALATICS_DATA_SUCESS = "ANALATICS_DATA_SUCESS";
export const ANALATICS_DATA_ERROR = "ANALATICS_DATA_ERROR";

/**
 * Action constants for StateAnalatics
 */
export const   FETCH_STATE_ANALATICS_DATA = "FETCH_STATE_ANALATICS_DATA";
export const   RECEIVE_STATE_ANALATICS_DATA_SUCESS = "RECEIVE_STATE_ANALATICS_DATA_SUCESS";
export const   RECEIVE_STATE_ANALATICS_DATA_ERROR = "RECEIVE_STATE_ANALATICS_DATA_ERROR";

/**
 * Action constants for countryAnalatics
 */
export const FETCH_COUNTRY_ANALATICS_DATA = "FETCH_COUNTRY_ANALATICS_DATA";
export const RECEIVE_COUNTRY_ANALATICS_DATA_SUCESS = "RECEIVE_COUNTRY_ANALATICS_DATA_SUCESS";
export const RECEIVE_COUNTRY_ANALATICS_DATA_ERROR = "RECEIVE_COUNTRY_ANALATICS_DATA_ERROR";

/**
 * Action Scenarios for deleting a question
 */
export const DELETE_QUESTION = "DELETE_QUESTION";
export const DELETE_QUESTION_SUCCESS = "DELETE_QUESTION_SUCCESS";