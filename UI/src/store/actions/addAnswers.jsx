import * as allActions from './actionConstants';

export function addAnswer (data) {
    console.log(1);
    console.log(data);
    return { 
        type:allActions.ADD_ANSWER,
        payload : data 
    };
}

export function addAnswerSuccess (data) {
    return {
        type : allActions.ADD_ANSWER_SUCCESS,
        payload : data
    };
}