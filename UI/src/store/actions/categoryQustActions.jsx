import * as allActions from './actionConstants';

export function fetchCategoryQuestions (data) {
    return { 
        type:allActions.FETCH_CATEGORY_QUESTIONS,
        payload : data
    };
}

export function receiveCategoryQuestions (data) {
    return {
        type : allActions.RECEIVE_CATEGORY_QUESTIONS,
        payload : data
    };
}
