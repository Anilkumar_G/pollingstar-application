import * as allActions              from '../actions/actionConstants';

export function deleteQues (data) {
    return { 
        type:allActions.DELETE_QUESTION,
        payload : data 
    };
}

export function deleteQuesSuccess(data) {
    return { 
        type:allActions.DELETE_QUESTION_SUCCESS,
        payload : data 
    };
}
