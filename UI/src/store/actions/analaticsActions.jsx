import * as allActions from  '../actions/actionConstants';
/*action methods for city*/
export function doFetchAnalaticsData(data){
    return {
        type:allActions.FETCH_ANALATICS_DATA,
        payload:data
    };

}
export function doAnalaticsDataSucess(data){
    return{
        type:allActions.ANALATICS_DATA_SUCESS,
        payload:data
    }

}

export function doAnalaticsDataError(data){
    return {
        type:allActions.ANALATICS_DATA_ERROR,
        payload:data
    }
}


/*action methods for state*/
export function doFetchStateAnalaticsData(data){
    return {
        type:allActions.FETCH_STATE_ANALATICS_DATA,
        payload:data
    }
}

export function doReceiveStateAnalaticsDataSucess(data){
    return {
        type:allActions.RECEIVE_STATE_ANALATICS_DATA_SUCESS,
        payload:data
    }
}

export function doReceiveStateAnalaticsDataError(data){
    return {
        type:allActions.RECEIVE_STATE_ANALATICS_DATA_ERROR,
        payload:data
    }
}

/* action methods for country */
export function doFetchCountryAnalaticsData(data){
    return {
        type:allActions.FETCH_COUNTRY_ANALATICS_DATA,
        payload : data
    }
}

export function doReceiveCountryAnalaticsDataSucess(data){
    return {
        type:allActions.RECEIVE_COUNTRY_ANALATICS_DATA_SUCESS,
        payload:data
    }
}

export function doReceiveCountryAnalaticsDataError(data){
    return {
        type:allActions.RECEIVE_COUNTRY_ANALATICS_DATA_ERROR,
        payload:data
    }
}