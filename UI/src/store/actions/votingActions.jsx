import * as allActions from './actionConstants';

export function addVote(data) {
    return { 
        type:allActions.ADD_VOTE,
        payload : data 
    };
}

export function addVoteSuccess (data) {
    return {
        type : allActions.ADD_VOTE_SUCCESS,
        payload : data
    };
}