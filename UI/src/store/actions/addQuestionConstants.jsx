import * as allActions from './actionConstants';

export function addQuestion (data) {
    console.log(1)
    return { 
        type:allActions.ADD_QUESTION,
        payload : data 
    };
}

export function questionAdded (data) {
    return {
        type : allActions.QUESTION_ADDED,
        payload : data
    };
}
