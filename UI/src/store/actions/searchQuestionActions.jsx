import * as allActions from '../actions/actionConstants';
export function doSearchQuestion(data) {
    return { 
        type:allActions.SEARCH_QUESTION,
        payload : data 
    };
}

export function doSearchQuestionSucess(data) {
    return { 
        type:allActions.SEARCH_QUESTION_SUCESS,
        payload : data 
    };
}
export function doSearchQuestionError(data) {
    return { 
        type:allActions.SEARCH_QUESTION_ERROR,
        payload : data 
    };
}