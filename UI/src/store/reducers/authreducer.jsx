import * as allActions from '../actions/actionConstants';

const initialState = {
    is_logged_in: false,
    statuscode: 0,
    data: {},
    forgetUpadatePassword : {},
    securityQuestionError : 0,
    count : 0   
}

export default function authReducer(state = initialState, action) {

    switch (action.type) {
        case allActions.LOGIN_USER_SUCCESS:

            return {
                ...state,
                is_logged_in: true,
            };
        case allActions.LOGIN_USER_DATA_ERROR:
            console.log(action.payload);
            return {
                is_logged_in: false,
                statuscode: action.payload
            };


        case allActions.SECURITY_QUESTION_SUCCES:
            console.log("haii reducer")
            console.log(action.payload);
            return {
                ...state,
                data: action.payload,
                statuscode:action.payload
            };

        case allActions.SECURITY_QUESTION_ERROR:
        console.log(action.payload);
        return {
            ...state,
            data : action.payload,
            count : state.count+1
             
        };


        case allActions.FORGET_UPDATE_PASSWORD_SUCESS:
            console.log(action.payload);
            return {
                ...state,
                forgetUpadatePassword : action.payload,
            };





        default:
            return state;
    }
}