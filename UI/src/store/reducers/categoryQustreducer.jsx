import * as allActions from '../actions/actionConstants'

const intialState = {
    questions : [],
    isLoaded : false,
    statuscode:0
}

export default function fetchCategoryQuestionReducer( state = intialState, action) {
    switch (action.type) {
        
        case allActions.FETCH_CATEGORY_QUESTIONS:
            console.log("i am fetch");
            return action;

        case allActions.RECEIVE_CATEGORY_QUESTIONS:
            console.log("i am Receive")
            return {
                ...state,
                questions: action.payload.response,
                isLoaded: true,
                statuscode:action.payload.statuscode
            };
        default:
            return state;
        }
    }