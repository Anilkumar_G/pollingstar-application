import * as allActions from '../actions/actionConstants'

const intialState = {
    questions : [],
    isLoaded : false,
}

export default function recentQuestionReducer( state = intialState, action) {
    switch (action.type) {
        
        case allActions.FETCH_RECENT_QUESTIONS:
            console.log("i am fetch");
            return action;

        case allActions.RECEIVE_RECENT_QUESTIONS:
            console.log("i am Receive")
            return {
                ...state,
                questions: action.payload.response,
                isLoaded: true
            };
        default:
            return state;
        }
    }