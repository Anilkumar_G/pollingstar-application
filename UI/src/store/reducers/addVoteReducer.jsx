import * as allActions from '../actions/actionConstants';

const initialState = {
    is_vote_added: false,
    statuscode:0
}

export default function addVotereducer(state = initialState, action) {

    switch (action.type) {
        case allActions.ADD_VOTE:
            return action;
            
        case allActions.ADD_VOTE_SUCCESS:
            return {
                ...state,
                is_vote_added: true,
                statuscode:action.payload
            };  
        default:
            return state;
    }
}