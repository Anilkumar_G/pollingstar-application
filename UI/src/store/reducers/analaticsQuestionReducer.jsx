//Component Dependencies
import * as allActions          from '../actions/actionConstants';

const initialState = {
    isAnalaticsData : false ,
    is_state_analatics_data : false,
    is_country_analatics_data : false,
    analaticsQuestionData : [],
    analaticsStateQuestionData : [],
    analaticsCountryQuestionData : []
}
export default function analaticsQuestionReducer(state = initialState, action) {
    switch(action.type) {
        case allActions.ANALATICS_DATA_SUCESS :
      
            return {
                ...state ,
                is_analatics_data : true,
                analaticsQuestionData : action.payload.response
            }
        case allActions.ANALATICS_DATA_ERROR :
            return {                
                is_analatics_data  : false ,
                analaticsQuestionData : action.payload.response
            }
        case allActions.RECEIVE_STATE_ANALATICS_DATA_SUCESS:
            return {
                 ...state,
                 is_state_analatics_data : true,
                 analaticsStateQuestionData : action.payload.response
             }
        case allActions.RECEIVE_STATE_ANALATICS_DATA_ERROR:
            return {
                is_state_analatics_data : false,
                analaticsStateQuestionData : action.payload.response
            }      
        
        case allActions.RECEIVE_COUNTRY_ANALATICS_DATA_SUCESS:
            return {
                  ...state,
                is_country_analatics_data : true,
                analaticsCountryQuestionData : action.payload.response
            }  
        
        case allActions.RECEIVE_COUNTRY_ANALATICS_DATA_ERROR:
            return {
                is_country_analatics_data : false,
                analaticsCountryQuestionData : action.payload.response
            }    
        default :
            return state;
    }
}