import * as allActions from '../actions/actionConstants';

const initialState = {
    is_deleted: false,
    statuscode:0
}

export default function deleteQuesReducer(state = initialState, action) {

    switch (action.type) {
        case allActions.DELETE_QUESTION:
            return action;
            
        case allActions.DELETE_QUESTION_SUCCESS:
            return {
                ...state,
                is_deleted: true,
                statuscode:action.payload
            };  
        default:
            return state;
    }
}