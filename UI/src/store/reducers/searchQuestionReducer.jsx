//Component Dependencies
import * as allActions          from '../actions/actionConstants';

const initialState = {
    is_search_question : false ,
    searchQuestionData : [],
    user:[]
}
export default function searchQuestionReducer(state = initialState, action) {
    switch(action.type) {
        case allActions.SEARCH_QUESTION_SUCESS :
    
            return {
                ...state ,
                is_search_question : true,
                searchQuestionData :  action.payload.response   
            }
        case allActions.SEARCH_QUESTION_ERROR :
            return {                
                is_search_question : false ,
                searchQuestionData : action.payload
            }
        default :
            return state;
    }
}