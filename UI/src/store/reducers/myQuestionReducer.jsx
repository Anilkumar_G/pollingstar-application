import * as allActions from '../actions/actionConstants'

const intialState = {
    questions : [],
    isLoaded : false,
    statuscode:0
}

export default function fetchUserQuestionReducer( state = intialState, action) {
    switch (action.type) {
        
        case allActions.FETCH_USER_QUESTIONS:
            console.log("i am fetch");
            return action;

        case allActions.RECEIVE_USER_QUESTIONS:
            console.log("i am Receive");
            console.log(action.payload.response)
            return {
                ...state,
                questions: action.payload.response,
                isLoaded: true,
                statuscode:action.payload.statuscode
            };
        default:
            return state;
        }
    }