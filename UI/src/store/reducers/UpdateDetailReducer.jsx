import * as allActions              from '../actions/actionConstants';

const initialState = {
    is_update: false,
    statuscode: 0
}
export default function updateDetailReducer(state = initialState, action) {
    console.log("update reducer")
    switch (action.type) {
        case allActions.UPDATE_DETAILS_SUCCESS:
            return {
                ...state,
                is_update: true,
                statuscode:action.payload
            };
        case allActions.LOGIN_USER_DATA_ERROR:
            return {
                is_update: false,
                statuscode: action.payload  
            }
        default:
            return state;
    }
}
