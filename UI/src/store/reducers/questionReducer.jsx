//Component Dependencies
import * as allActions          from '../actions/actionConstants';

const initialState = {
    is_add_question : false ,
    statuscode : 0
}
export default function questionReducer(state = initialState, action) {
    switch(action.type) {
        case allActions.ADD_QUESTION_SUCCESS :
            return {
                ...state ,
                is_add_question : true
            }
        case allActions.ADD_QUESTION_DATA_ERROR :
            return {
                is_add_question : false ,
                statuscode : action.payload
            }
        default :
            return state;
    }
}