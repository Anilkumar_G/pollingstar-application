
//npm Dependencies
import  { combineReducers }             from 'redux';

//Component Dependencies
import authReducer                      from "./authreducer";
import signUpReducer                    from './signUpReducer';
import addQuestionReducer               from './addQuestionReducer.';
import fetchQuestionReducer             from './fetchQuestionReducer';
import recentQuestionReducer            from './recentQuestReducer';
import trendingQuestionReducer          from './trendingQuestReducer';    
import addAnswerreducer                 from './addAnswerReducer';
import addVotereducer                   from './addVoteReducer';
import fetchCategoryQuestionReducer     from './categoryQustreducer';
import searchQuestionReducer            from './searchQuestionReducer';
import myQuestionReducer                from './myQuestionReducer';
import updateDetailReducer              from './UpdateDetailReducer';
import analaticsQuestionReducer         from './analaticsQuestionReducer';
import deleteQuestionReducer            from './deleteQuestionReducer';

const rootReducer = combineReducers ({
    authReducer,
    signUpReducer,
    addQuestionReducer,
    fetchQuestionReducer,
    recentQuestionReducer,
    trendingQuestionReducer,
    addAnswerreducer,
    addVotereducer,
    fetchCategoryQuestionReducer,
    searchQuestionReducer,
    myQuestionReducer,
    updateDetailReducer,
    analaticsQuestionReducer,
    deleteQuestionReducer
});

export default rootReducer;