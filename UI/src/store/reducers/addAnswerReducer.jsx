import * as allActions from '../actions/actionConstants';

const initialState = {
    is_answer_added: false,
}

export default function addAnswerreducer(state = initialState, action) {

    switch (action.type) {
        case allActions.ADD_ANSWER:
            return action;
            
        case allActions.ADD_ANSWER_SUCCESS:
            return {
                ...state,
                is_answer_added: true,
            };  
        default:
            return state;
    }
}