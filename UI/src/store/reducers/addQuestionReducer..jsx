import * as allActions from '../actions/actionConstants';

const initialState = {
    is_question_added: false,
    statuscode:0
}

export default function addQuestionreducer(state = initialState, action) {
    console.log("hai")
    switch (action.type) {
        case allActions.ADD_QUESTION:
            return action;
            
        case allActions.QUESTION_ADDED:
            return {
                ...state,
                is_question_added: true,
                statuscode:action.payload
            };
        default:
            return state;
    }
    
}