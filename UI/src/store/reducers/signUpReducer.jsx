//Component Dependencies
import * as allActions          from '../actions/actionConstants';

const initialState = {
    is_sign_up: false,
    statuscode: 0
}
export default function signUpReducer(state = initialState, action) {
    switch (action.type) {
        case allActions.SIGNUP_USER_SUCCESS:
            return {
                ...state,
                is_sign_up: true,
                statuscode:action.payload
            };
        case allActions.LOGIN_USER_DATA_ERROR:
            return {
                is_sign_up: false,
                statuscode: action.payload
            }
        default:
            return state;
    }
}