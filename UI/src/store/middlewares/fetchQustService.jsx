import request from 'superagent';
import * as fetchQuestionActions from '../actions/FetchQuestionActions';
import * as allActions from '../actions/actionConstants';

const fecthQuestionService = (store) => next => action => {
    next(action)
    switch (action.type) {
        case allActions.FETCH_ALL_QUESTIONS:
            request.get('http://192.168.150.102:8080/api/question/allAnswers/')
                .then(res => {
                    const data = JSON.parse(res.text);
                    if(data.statuscode == 1){
                        next(fetchQuestionActions.receiveQuestions(data));
                    }
                    if(data.statuscode == -1){
                        alert("No Questions Available")
                    }
                })
                .catch(err => {
                    next({ type: 'FETCH_QUESTIONS_DATA_ERROR', err });
                });
                break;   
        default:
                break;         
            }
        }

export default fecthQuestionService;