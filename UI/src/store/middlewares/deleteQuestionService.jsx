import * as allActions              from '../actions/actionConstants';
import request                      from 'superagent';

const deleteQuesService = store => next => action => {

    next(action)
    console.log("i am switch")
    switch (action.type) {
        case allActions.DELETE_QUESTION:
            // console.log(2);
            // console.log(action.payload.answer)
            console.log(localStorage.getItem('jwt-token'))
            console.log(action.payload.question_id)
            request.delete ("http://192.168.150.102:8080/api/question/deleteQuestion/"+ `${action.payload.question_id}`)
            .set('Authorization',(localStorage.getItem('jwt-token')))
            .send({
                
                "question_id":action.payload.question_id,
                "userDetails" : {
                    "user_id":localStorage.getItem('user_id')
                },
            })
            .set('Content-Type', 'application/json')
            .then (res =>{
                console.log (action.payload)
                const data = JSON.parse (res.text);
                console.log ("i am status" + data.statuscode)   
                if (data.statuscode == 1){
                    next ({   
                        type : allActions.DELETE_QUESTION,
                        payload : data.statuscode   
                    })
                    
                }
                if (data.statuscode == -3){
                    next ({
                        type : allActions.DELETE_QUESTION,
                        payload : data.statuscode   
                    })
                    
                }
            })
            .catch (err => {
                return next ({ type : 'ADD_QUESTION_DATA_ERROR', err });
            });
            break;
            
    }
}
export default deleteQuesService;