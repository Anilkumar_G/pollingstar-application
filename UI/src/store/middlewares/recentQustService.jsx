import request from 'superagent';
import * as recentQuesActions from '../actions/recentQuesActions';
import * as allActions from '../actions/actionConstants';

const RecentQuestionService = (store) => next => action => {
    next(action)
    switch (action.type) {
        case allActions.FETCH_RECENT_QUESTIONS:
            request.get("http://192.168.150.102:8080/api/question/recent/"+ `${localStorage.getItem('user_id')}`)
            .set('Authorization',(localStorage.getItem('jwt-token')))
                .then(res => {
                    const data = JSON.parse(res.text);
                    next(recentQuesActions.receiveRecentQuestions(data));
                    console.log(data)
                })
                .catch(err => {
                    next({ type: 'FETCH_QUESTIONS_DATA_ERROR', err });
                });
                break;   
        default:
                break;         
            }
        }

export default RecentQuestionService;