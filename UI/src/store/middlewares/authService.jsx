import * as allActions                  from '../actions/actionConstants';
import request                          from 'superagent';

const authService = store => next => action => {

    next(action)
    switch (action.type) {
        case allActions.DO_LOGIN_USER:

            request.post("http://192.168.150.102:8080/api/auth/login", action.payload)
                .set('Content-Type', 'application/json')
                 .then(res => {
                    console.log(action.payload)
                    const data = JSON.parse(res.text);
                    console.log("i am status" + data.statuscode)
                    if (data.statuscode == 1) {
                        next({
                            type: allActions.LOGIN_USER_SUCCESS,
                            payload: data.statuscode
                        })
                    localStorage.setItem('user_id', data.response.user_id);
                    localStorage.setItem('email', data.response.email);
                    localStorage.setItem('first_name', data.response.first_name);
                    localStorage.setItem('last_name', data.response.last_name);
                    localStorage.setItem('city', data.response.city);
                    localStorage.setItem('state', data.response.state);
                    localStorage.setItem('country', data.response.country);
                    localStorage.setItem('religion', data.response.religion);
                    localStorage.setItem('phone', data.response.phone);
                    localStorage.setItem('religion',data.response.religion);
                    localStorage.setItem('securityQues',data.response.securityQuestion);
                    localStorage.setItem('securityAns',data.response.securityAnswer);
                    localStorage.setItem('jwt-token',data.token)
                    }
                    else {
                        console.log(data.statuscode)
                        next({
                            type: allActions.LOGIN_USER_DATA_ERROR,
                            payload: data.statuscode
                        });

                    }
                })
                .catch(err => {
                    return next({ type: 'LOGIN_USER_DATA_ERROR', err });
                });
            break;


            case allActions.SECURITY_QUESTION_CHECK:
            console.log("haiiii service")
            request.post("http://192.168.150.102:8080/api/auth/forgetPassword", action.payload)
                .set('Content-Type', 'application/json')
                .then(res => {
                    const data = JSON.parse(res.text);
               
                    if (data.statuscode == 1) {
                        next({
                            type: allActions.SECURITY_QUESTION_SUCCES,
                            payload: data
                        })
                    }
                    else if(data.statuscode == -2) {
                        next({
                            type : allActions.SECURITY_QUESTION_SUCCES,
                            payload:data.statuscode
                        })
                    }
                    else {
                        next({
                            type: allActions.SECURITY_QUESTION_ERROR,
                            payload: data
                            
                        });

                    }
                })
                .catch(err => {
                    return next({ type: 'SECURITY_QUESTION_ERROR', err });
                });
            break;


        case allActions.FORGET_UPDATE_PASSWORD:
        console.log(action.payload)
            request.put("http://192.168.150.102:8080/api/auth/forgetUpdatePassword",action.payload)
                .set('Content-Type', 'application/json')
                //  request.post ("http://192.168.150.95:8080/myspace/login")
                //  .send({'username':action.payload.email,'password':action.payload.password}) 
                .then(res => {
                    const data = JSON.parse(res.text);

                    if (data.statuscode == 1) {
                        next({
                            type: allActions.FORGET_UPDATE_PASSWORD_SUCESS,
                            payload: data
                        })
                    }
                    
                    else {
                        console.log(data.statuscode)
                        next({
                            type: allActions.FORGET_UPDATE_PASSWORD_ERROR,
                            payload: data
                        });

                    }
                })
                .catch(err => {
                    return next({ type: 'FORGET_UPDATE_PASSWORD_ERROR', err });
                });
            break;

    }
}
export default authService;