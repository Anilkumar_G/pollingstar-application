import *as allActions from '../actions/actionConstants';
import request from 'superagent';

const updateDetailService = store => next => action => {
    next(action)
    switch (action.type) {
        case allActions.UPDATE_DETAILS:



            request.put("http://192.168.150.102:8080/api/user/updateDetails", action.payload)
                .set('Authorization', (localStorage.getItem('jwt-token')))
                // .set("Content-Type","application/json")
                .then(res => {
                    console.log(action.payload.last_name)
                    const data = JSON.parse(res.text);
                    if (data.statuscode === 1) {
                        localStorage.removeItem('email');
                        localStorage.removeItem('first_name');
                        localStorage.removeItem('last_name');
                        localStorage.removeItem('city');
                        localStorage.removeItem('state');
                        localStorage.removeItem('country');
                        localStorage.removeItem('religion');
                        localStorage.removeItem('phone');
                        localStorage.removeItem('securityQues');
                        localStorage.removeItem('securityAns');
                        next({
                            type: allActions.UPDATE_DETAILS_SUCCESS,
                            payload: data.statuscode
                        })
                        localStorage.setItem('email', data.response.email);
                        localStorage.setItem('first_name', data.response.first_name);
                        localStorage.setItem('last_name', data.response.last_name);
                        localStorage.setItem('city', data.response.city);
                        localStorage.setItem('state', data.response.state);
                        localStorage.setItem('country', data.response.country);
                        localStorage.setItem('religion', data.response.religion);
                        localStorage.setItem('phone', data.response.phone);
                        localStorage.setItem('securityQues', data.response.securityQuestion);
                        localStorage.setItem('securityAns', data.response.securityAnswer);
                    }

                    else if (data.statuscode === -1) {
                        next({
                            type: allActions.UPDATE_DETAILS_SUCCESS,
                            payload: data.statuscode
                        })
                    }
                    else if (data.statuscode === -2) {
                        next({
                            type: allActions.UPDATE_DETAILS_SUCCESS,
                            payload: data.statuscode
                        })
                    }
                    else if (data.statuscode === -3) {
                        next({
                            type: allActions.UPDATE_DETAILS_SUCCESS,
                            payload: data.statuscode
                        })
                    }
                    else {
                        next({
                            type: allActions.LOGIN_USER_DATA_ERROR,
                            payload: data.statuscode
                        });

                    }
                })
                .catch(err => {
                    return next({ type: 'SIGNUP_USER_DATA_ERROR', err });
                });
            break;
    }
}
export default updateDetailService;