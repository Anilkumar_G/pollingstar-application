import * as allActions from '../actions/actionConstants';
import request from 'superagent';

const addVoteService = store => next => action => {

    next(action)
    switch (action.type) {
        case allActions.ADD_VOTE:
        console.log(1)
            request.post ("http://192.168.150.102:8080/api/voting/add/")
            // .send({'answer':action.payload.answer,'userDetails': {'user_id':localStorage.getItem('user_id')},'question':{'question_id':localStorage.getItem('question_id')}}) 
            .send({
                "userdetails" : {
                    "user_id":localStorage.getItem('user_id')
                },
                "question":{
                    "question_id":action.payload.question_id
                },
                "answer" : {
                    "id": action.payload.id
                }
            })
            .set('Content-Type', 'application/json')
            .set('Authorization',(localStorage.getItem('jwt-token')))
            .then (res =>{
                console.log (action.payload)
                const data = JSON.parse (res.text);
                if (data.statuscode == 1){
                    next ({
                        type : allActions.ADD_VOTE_SUCCESS,
                        payload : data.statuscode   
                    })
                    // alert("Thanks for Voting Answer")
                    // window.location.reload();
                }
                if (data.statuscode == 2){
                    next ({
                        type : allActions.ADD_VOTE_SUCCESS,
                        payload : data.statuscode   
                    })
                    // alert("You Updated your vote")
                    // window.location.reload();

                }

            })
            .catch (err => {
                return next ({ type : 'ADD_QUESTION_DATA_ERROR', err });
            });
            break;

            default:
                break;
            
    }
}
export default addVoteService;