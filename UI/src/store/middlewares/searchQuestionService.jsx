//npm Dependencies
import request                 from 'superagent';

//Component Dependencies
import * as allActions          from '../actions/actionConstants';

const searchQuestionService = store => next => action => {
    next(action)
    switch(action.type) {
        case allActions.SEARCH_QUESTION: 
                request.post("http://192.168.150.102:8080/api/question/searchQuestion/"+ `${localStorage.getItem('user_id')}`)
                .send({'question':action.payload.question})
                .set('Content-Type','application/json')
                .set('Authorization',(localStorage.getItem('jwt-token')))
                    .then(res => {
                     
                        const data = JSON.parse(res.text)
                        console.log(data);
                        console.log("servicess for searchquestion");
                        if(data.statuscode == 1) {
                            
                            next({
                                type : allActions.SEARCH_QUESTION_SUCESS,
                                payload : data
                            })                           
                        }
                        if(data.statuscode == -1) {
                            alert("No Questions Found..!")          
                        }
                        else {
                            next({
                                type : allActions.SEARCH_QUESTION_ERROR,
                                payload : data
                            })
                        }
                    })
                    .catch(err => {
                        return next({
                            type : 'allActions.SEARCH_QUESTION_ERROR',
                            err
                        })
                    })
            break;
    }
}
export default searchQuestionService;