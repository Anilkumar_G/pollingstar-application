import request from 'superagent';
import * as categoryQuesActions from '../actions/categoryQustActions';
import * as allActions from '../actions/actionConstants';

const categoryQuestionService = (store) => next => action => {
    next(action)
    switch (action.type) {
        case allActions.FETCH_CATEGORY_QUESTIONS:
            request.post("http://192.168.150.102:8080/api/question/category/"+ `${localStorage.getItem('user_id')}`)
                .send({ "category": action.payload.category })
                .set('Content-Type', 'application/json')
                .set('Authorization',(localStorage.getItem('jwt-token')))
                .then(res => {
                    const data = JSON.parse(res.text);
                    if(data.statuscode === 1){
                    next(categoryQuesActions.receiveCategoryQuestions(data));
                    }
                    // if (data.statuscode == -1){
                    //     alert("No Questions found in selected category")
                    // }
                })

                .catch(err => {
                    next({ type: 'FETCH_QUESTIONS_DATA_ERROR', err });
                });
            break;
        default:
            break;
    }
}

export default categoryQuestionService;