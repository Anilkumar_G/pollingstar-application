import React from 'react';
import {Redirect} from 'react-router-dom'
import request                          from 'superagent';
import * as fetchUserActions            from '../actions/myQuestions';
import * as allActions                  from '../actions/actionConstants';

const fecthUserQuestionService = (store) => next => action => { 
    
    next(action)
    switch (action.type) {
        case allActions.FETCH_USER_QUESTIONS:
            request.post("http://192.168.150.102:8080/api/question/userQuestion/answer")
                .send({
                    user_id:localStorage.getItem('user_id')  
                })
                .set('Content-Type','application/json')
                .set('Authorization',(localStorage.getItem('jwt-token')))
                .then(res => {
                    const data = JSON.parse(res.text);
                    console.log(data)
                    // if(data.statuscode == -1){
                    //     // alert("you didn't posted any questions yet..!")
                    //     // return <Redirect to ="/home" />
                    //     next({
                    //         type: allActions.RECEIVE_USER_QUESTIONS,
                    //         payload:data.statuscode
                    //     })
                    // }
                    if(data.statuscode == 1){
                    next({
                        type: allActions.RECEIVE_USER_QUESTIONS,
                        payload: data
                    });
                }
                })
                .catch(err => {
                    next({ type: 'FETCH_QUESTIONS_DATA_ERROR', err });
                });
                break;   
        default:
                break;         
            }
        }

export default fecthUserQuestionService;