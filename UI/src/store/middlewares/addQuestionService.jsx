import * as allActions from '../actions/actionConstants';
import request from 'superagent';

const addQuestionService = store => next => action => {

    next(action)
    console.log("i am switch")
    switch (action.type) {
        case allActions.ADD_QUESTION:
            request.post("http://192.168.150.102:8080/api/question/create")
                .send({ 'question': action.payload.question, 'category':action.payload.category, 'userDetails': { 'user_id': localStorage.getItem('user_id') } })
                .set('Content-Type', 'application/json')
                .set('Authorization',(localStorage.getItem('jwt-token')))
                .then(res => {
                    console.log(action.payload)
                    const data = JSON.parse(res.text);
                    console.log("i am status" + data.statuscode)
                    if (data.statuscode === 1) {
                        next({
                            type: allActions.QUESTION_ADDED,
                            payload: data.statuscode
                        })
                        // alert("Thanks for adding Question"); 
                    }
                    if (data.statuscode === -1) {
                        next({
                            type: allActions.QUESTION_ADDED,
                            payload: data.statuscode
                        })
                        // alert("You have already asked this question..!"); 
                    }
                    if (data.statuscode === -2) {
                        next({
                            type: allActions.QUESTION_ADDED,
                            payload: data.statuscode
                        })
                        // alert("You have already asked this question..!"); 
                    }
                    if (data.statuscode === -3) {
                        next({
                            type: allActions.QUESTION_ADDED,
                            payload: data.statuscode
                        })
                        // alert("You have already asked this question..!"); 
                    }
                })
                .catch(err => {
                    return next({ type: 'ADD_QUESTION_DATA_ERROR', err });
                });
            break;
        default:
            break;

    }
}
export default addQuestionService;