import request from 'superagent';
import * as trendingQuesActions from '../actions/trendingQuesActions';
import * as allActions from '../actions/actionConstants';

const TrendingQuestionService = (store) => next => action => {
    next(action)
    switch (action.type) {
        case allActions.FETCH_TRENDING_QUESTIONS:
            request.get("http://192.168.150.102:8080/api/question/trendingQuestions/"+ `${localStorage.getItem('user_id')}`)
            .set('Authorization',(localStorage.getItem('jwt-token')))
                .then(res => {
                    const data = JSON.parse(res.text);
                    next(trendingQuesActions.receiveTrendingQuestions(data));
                    console.log(data)
                })
                .catch(err => {
                    next({ type: 'FETCH_QUESTIONS_DATA_ERROR', err });
                });
                break;   
        default:
                break;         
            }
        }

export default TrendingQuestionService;