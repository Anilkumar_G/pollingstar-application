
//npm Dependencies
import { createStore, compose, applyMiddleware }          from 'redux';

//Component Dependencies
import rootReducer                                        from '../reducers/rootReducer';
import authService                                        from '../middlewares/authService';
import signUpService                                      from '../middlewares/signUpService';
import addQuestionService                                 from '../middlewares/addQuestionService';
import fecthQuestionService                               from '../middlewares/fetchQustService';
import recentQustService                                  from '../middlewares/recentQustService';
import trendiingQustService                               from '../middlewares/trendingQustService';
import addAnswerService                                   from '../middlewares/addAnswerService';
import addVoteService                                     from '../middlewares/addVoteService';
import categoryQuestionService                            from '../middlewares/categoryQustService';
import myQuestionsService                                 from '../middlewares/myQuestionsService';
import searchQuestionService                              from '../middlewares/searchQuestionService';
import analaticsQuestionService                           from '../middlewares/analaticsQuestionService';
import deleteQuestionService                              from '../middlewares/deleteQuestionService'; 

export default function configureStore() {
    return createStore (
        rootReducer,
        compose(applyMiddleware(
            authService,
            signUpService,
            addQuestionService,
            fecthQuestionService,
            recentQustService,
            trendiingQustService,
            addAnswerService,
            addVoteService,
            categoryQuestionService,
            myQuestionsService,
            searchQuestionService,
            analaticsQuestionService,
            deleteQuestionService
        ))
    );
};