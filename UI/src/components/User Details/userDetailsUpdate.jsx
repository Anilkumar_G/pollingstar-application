import React, { Component } from 'react';

import { bindActionCreators } from 'redux';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import MainHeader from '../MainPage/mainHeader';
import SideNav from '../MainPage/sideNav';
import '../../Assets/css/userDetailsUpdate.css'

//Store Dependencies
import * as updateActions from '../../store/actions/UpdateDetailActions';

/**
 * Purpose of this Component is to provide
 * the SignUp form to the user
 */

class UpdateDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fields: {
                first_name : "",
                last_name : "",
                email : "",
                phone : "",
                city : "",
                state : "",
                country : "",
                password : "",
                religion : "",
                securityQuestion : "",
                securityAnswer : "",
            },
            errors: {},
            isSubmitted: false,
            doCancel: false
        }

        this.handleChange = this.handleChange.bind(this);
        this.doUpdate = this.doUpdate
            .bind(this);
        this.cancel = this.cancel.bind(this);
    }

    handleChange = (event) => {
        let fields = this.state.fields;
        fields[event.target.name] = event.target.value;
        this.setState({
            fields,  
        });

    }
  componentWillMount(){
      this.setState({
       fields :{first_name : localStorage.getItem("first_name"),
      last_name : localStorage.getItem("last_name"),
      email : localStorage.getItem("email"),
      phone : localStorage.getItem("phone"),
      city : localStorage.getItem("city"),
      state : localStorage.getItem("state"),
      country : localStorage.getItem("country"),
      password : localStorage.getItem("password"),
      religion : localStorage.getItem("religion"),
      securityQuestion : localStorage.getItem("securityQues"),
      securityAnswer : localStorage.getItem("securityAns"),}
    })
  }
   
    doUpdate = e => {
        e.preventDefault();
        if (this.validateForm()) {
          
            this.props.updateActions.updateDetails({
                user_id: localStorage.getItem('user_id'),
                first_name: this.state.fields.first_name,
                last_name: this.state.fields.last_name,
                email: this.state.fields.email,
                city: this.state.fields.city,
                state: this.state.fields.state,
                country: this.state.fields.country,
                religion: this.state.fields.religion,
                phone: this.state.fields.phone,
                securityQuestion: this.state.fields.securityQuestion,
                securityAnswer: this.state.fields.securityAnswer,
            })
            console.log(this.state.fields.phone)
            this.setState({
                isSubmitted: true
            })
        }

    }

    validateForm() {

        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;



        if (typeof fields["first_name"] !== "undefined") {
            if (!fields["first_name"].match(/^[a-zA-Z ]*$/)) {
                formIsValid = false;
                errors["first_name"] = "*Please Enter Alphabets Only";
            }
        }



        if (typeof fields["last_name"] !== "undefined") {
            if (!fields["last_name"].match(/^[a-zA-Z ]*$/)) {
                formIsValid = false;
                errors["last_name"] = "*Please Enter Alphabets Only";
            }
        }



        if (typeof fields["email"] !== "undefined") {
            //regular expression for email validation
            var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            if (!pattern.test(fields["email"])) {
                formIsValid = false;
                errors["email"] = "*Please Enter Valid Email Address";
            }
        }


        if (typeof fields["phone"] !== "undefined") {
            if (!fields["phone"].match(/^[0-9]{10}$/)) {
                formIsValid = false;
                errors["phone"] = "*Please Enter Valid phone Number";
            }
        }

        if (typeof fields["city"] !== "undefined") {
            if (!fields["city"].match(/^[a-zA-Z ]*$/)) {
                formIsValid = false;
                errors["city"] = "*Please Enter Alphabets Only";
            }
        }

        if (typeof fields["state"] !== "undefined") {
            if (!fields["state"].match(/^[a-zA-Z ]*$/)) {
                formIsValid = false;
                errors["state"] = "*Please Enter Alphabets Only";
            }
        }


        if (typeof fields["religion"] !== "undefined") {
            if (!fields["religion"].match(/^[a-zA-Z ]*$/)) {
                formIsValid = false;
                errors["religion"] = "*Please Enter Alphabets Only";
            }
        }


        if (typeof fields["country"] !== "undefined") {
            if (!fields["country"].match(/^[a-zA-Z ]*$/)) {
                formIsValid = false;
                errors["country"] = "*Please Enter Alphabets Only.";
            }
        }



        if (typeof fields["securityAnswer"] !== "undefined") {
            if (!fields["securityAnswer"]) {
                formIsValid = false;
                errors["securityAnswer"] = "*Please Enter Your Answer";
            }
        }


        this.setState({
            errors: errors
        });
        return formIsValid;


    }
    cancel = () => {
        var cancel1 = window.confirm("You are redirecting to Profile Page");
        if (cancel1 == true) {
            this.setState({
                doCancel: true
            })
        }
    }
    render() {
        if (this.state.isSubmitted) {
            console.log(this.state.isSubmitted)
            console.log(this.props.is_update);
            console.log(this.props.statuscode.statuscode);
            if (this.props.is_update) {
                console.log(this.props.statuscode.statuscode)
                // console.log(this.props.statuscode.payload)
                if (this.props.statuscode.statuscode == 1) {
                    return (
                        <div>
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={() => { window.location.reload() }}>&times;</a>
                                <strong>Your Details has been Updated..</strong>
                            </div>
                        </div>
                    )
                }

                if (this.props.statuscode.statuscode == -1) {
                    return (
                        <div>
                            <div class="alert alert-danger alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={() => { window.location.reload() }}>&times;</a>
                                <strong>Not a Valid User</strong>
                            </div>

                        </div>
                    )
                }
                if (this.props.statuscode.statuscode == -2) {
                    return (
                        <div>
                            <div class="alert alert-danger alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={() => { window.location.reload() }}>&times;</a>
                                <strong>No Fields are Updated</strong>
                            </div>
                        </div>
                    )
                }
                if (this.props.statuscode.statuscode == -3) {
                    return (
                        <div>
                            <div class="alert alert-danger alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={() => { window.location.reload() }}>&times;</a>
                                <strong>Mobile Number Already Exists</strong>
                            </div>
                        </div>
                    )
                }
            }
        }
        if (this.state.doCancel) {
            return (
                <Redirect to="/home/details"></Redirect>
            )
        }
        return (
            // <div id="forms">
            //     <div class="container">
            //         <p class="field">First Name</p><br />
            //         <input class="text" name="first_name" defaultValue={localStorage.getItem('first_name')}  type="text" onChange={this.handleChange} placeholder="First Name" ></input><br />
            //         <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.first_name}</div>
            //     </div><br />
            //     <div class="container">
            //         <p class="field">Last Name</p><br />
            //         <input class="text" type="text" name="last_name"  defaultValue={localStorage.getItem('last_name')} onChange={this.handleChange} placeholder="Last Name" ></input>
            //         <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.last_name}</div>
            //     </div><br /><br /><br /><br />
            //     <div class="container">
            //         <p class="field">Email Address</p><br />
            //         <input class="text" name="email" type="text" defaultValue={localStorage.getItem('email')} onChange={this.handleChange} placeholder="example@email.com" ></input><br />
            //         <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.email}</div>
            //     </div><br />

            //     <div class="container">
            //         <p class="field">phone Number</p><br />
            //         <input class="text" type="text" name="phone" defaultValue={localStorage.getItem('phone')} onChange={this.handleChange} placeholder="phone Number" ></input>
            //         <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.phone}</div></div><br /><br /><br /><br />
            //     <div class="container">
            //         <p class="field">City</p><br />
            //         <input class="text" name="city" type="text" defaultValue={localStorage.getItem('city')} onChange={this.handleChange} placeholder="City" ></input><br />
            //         <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.city}</div>
            //     </div><br />
            //     <div class="container">
            //         <p class="field">State</p><br />
            //         <input class="text" type="text" name="state" defaultValue={localStorage.getItem('state')} onChange={this.handleChange} placeholder="State" ></input>
            //         <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.state}</div>
            //     </div><br /><br /><br /><br />
            //     <div class="container">
            //         <p class="field">Country</p><br />
            //         <input class="text" name="country" type="text" defaultValue={localStorage.getItem('country')} onChange={this.handleChange} placeholder="Country" ></input><br />
            //         <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.country}</div>
            //     </div><br />
            //     <div class="container">
            //         <p class="field">Religion</p><br />
            //         <input class="text" type="text" name="religion" defaultValue={localStorage.getItem('religion')} onChange={this.handleChange} placeholder="Religion" ></input>
            //         <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.religion}</div>
            //     </div><br /><br /><br /><br />
            //     <div class="container">
            //         <p className="field">Security Question</p><br />
            //         <div class="form-group">
            //             <select className="text" name="securityQuestion" defaultValue ={localStorage.getItem('securityQues')} onChange={this.handleChange}>
            //                 <option value="">Select Security Question</option>
            //                 <option className="field" className="field" value="What is your Pet Name?" >What is your Pet Name?</option>
            //                 <option className="field" value="What is your Childhood SchoolName?" >What is your Childhood SchoolName?</option>
            //                 <option className="field" value="What is your Favourite Dish?">What is your Favourite Dish?</option>
            //             </select>
            //             <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.securityQuestion}</div>
            //         </div>
            //     </div><br />
            //     <div class="container">
            //         <p class="field">Security Answer</p><br />
            //         <input class="text" name="securityAnswer" type="text" defaultValue = {localStorage.getItem('securityAns')} onChange={this.handleChange} placeholder="Enter Answer" ></input><br />
            //         <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.securityAnswer}</div>
            //     </div><br />
            //     <br /><br /><br /><br />
            //     <div class="container" style={{ marginLeft: "10%" }}>
            //         <button type="button" class="btn btn-danger" onClick={this.cancel}>Cancel</button> <span><button type="button" class="btn btn-success" onClick={this.doUpdate}>UPDATE</button></span>
            //     </div>
            // </div>
            <div>
                <MainHeader />
                <div className="container" style={{ marginTop: "5%" }}>
                    <div className="row">
                        <div className="col-sm-5">
                            <SideNav />
                        </div>
                        <div className="col-sm-7">
                            <div >
                                <img className="profile" src="https://qsf.fs.quoracdn.net/-3-images.new_grid.profile_pic_default.png-26-345032f7d91f49f2.png"></img><br />
                                <i class="fa fa-user-o icons"><span style={{ color: "white" }}>&nbsp;&nbsp;First Name:&nbsp;&nbsp;<input class="text" name="first_name" defaultValue={localStorage.getItem('first_name')} type="text" onChange={this.handleChange}  ></input>
                                    <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.first_name}</div></span></i><br />
                                <i class="fa fa-user-o icons"><span style={{ color: "white" }}>&nbsp;&nbsp;Last Name:&nbsp;&nbsp;<input class="text" type="text" name="last_name" defaultValue={localStorage.getItem('last_name')} onChange={this.handleChange} ></input>
                                    <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.last_name}</div></span></i><br />
                                {/* <i class="fa fa-envelope-o icons"><span style={{ color: "white" }}>&nbsp;&nbsp;Email Address:&nbsp;&nbsp; <input class="text" name="email" type="text" defaultValue={localStorage.getItem('email')} onChange={this.handleChange}  ></input><br />
                                    <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.email}</div></span></i><br /> */}
                                <i class="fa fa-phone icons"><span style={{ color: "white" }}>&nbsp;&nbsp;Contact:&nbsp;&nbsp; <input class="text" type="text" name="phone" defaultValue={localStorage.getItem('phone')} onChange={this.handleChange} ></input>
                                    <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.phone}</div></span></i><br />
                                <i class="fa fa-building icons"><span style={{ color: "white" }}>&nbsp;&nbsp;City:&nbsp;&nbsp;<input class="text" name="city" type="text" defaultValue={localStorage.getItem('city')} onChange={this.handleChange} ></input><br />
                                    <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.city}</div></span></i><br />
                                <i class="fa fa-building icons"><span style={{ color: "white" }}>&nbsp;&nbsp;State:&nbsp;&nbsp;<input class="text" type="text" name="state" defaultValue={localStorage.getItem('state')} onChange={this.handleChange}  ></input>
                                    <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.state}</div></span></i><br />
                                <i class="fa fa-globe icons"><span style={{ color: "white" }}>&nbsp;&nbsp;Country:&nbsp;&nbsp;<input class="text" name="country" type="text" defaultValue={localStorage.getItem('country')} onChange={this.handleChange}  ></input><br />
                                    <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.country}</div></span></i><br />
                                <i class="fas fa-gopuram icons"><span style={{ color: "white" }}>&nbsp;&nbsp;Religion:&nbsp;&nbsp;<input class="text" type="text" name="religion" defaultValue={localStorage.getItem('religion')} onChange={this.handleChange} ></input>
                                    <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.religion}</div></span></i><br />
                                {/* <i class="fa fa-lock icons"><span style={{ color: "white" }}>&nbsp;&nbsp;Security Question:&nbsp;&nbsp;<br/><br/><div class="form-group">
                                        <select className="text" name="securityQuestion" defaultValue={localStorage.getItem('securityQues')} onChange={this.handleChange}>
                                            <option value=""></option>
                                            <option className="field" className="field" value="What is your Pet Name?" >What is your Pet Name?</option>
                                            <option className="field" value="What is your Childhood SchoolName?" >What is your Childhood SchoolName?</option>
                                            <option className="field" value="What is your Favourite Dish?">What is your Favourite Dish?</option>
                                        </select>
                                        <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.securityQuestion}</div>
                                    </div></span></i><br />
                                <i class="fa fa-unlock-alt icons"><span style={{ color: "white" }}>&nbsp;&nbsp;Security Answer:&nbsp;&nbsp;<input class="text" name="securityAnswer" type="text" defaultValue={localStorage.getItem('securityAns')} onChange={this.handleChange}  ></input><br />
                                    <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.securityAnswer}</div></span></i><br /> */}
                                <button type="button" class="btn btn-danger" onClick={this.cancel}>Cancel</button> <span><button type="button" class="btn btn-success" onClick={this.doUpdate}>UPDATE</button></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
function mapStateToProps(state) {
    console.log(state.updateDetailReducer.statuscode)
    console.log(state.updateDetailReducer.is_update)
    return {
        is_update: state.updateDetailReducer.is_update,
        statuscode: state.updateDetailReducer
    };

}

function mapDispatchToProps(dispatch) {
    return {
        updateActions: bindActionCreators(updateActions, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(UpdateDetails);
