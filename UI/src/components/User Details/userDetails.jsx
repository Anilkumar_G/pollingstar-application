import React, { Component }             from 'react';
import {Link}                           from 'react-router-dom'
import '../../Assets/css/UserDetails.css';
import MainHeader                       from '../MainPage/mainHeader';
import SideNav                          from '../MainPage/sideNav';

class UserProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    render() {
        return (
            <div>
                <MainHeader />
                <div className="container" style={{ marginTop: "5%" }}>
                    <div className="row">
                        <div className="col-sm-5">
                            <SideNav />
                        </div>
                        <div className="col-sm-7">
                        <div >
                        <img className = "profile" src="https://lh3.googleusercontent.com/Y2WFKTpCszKtYV00aUN2aXqCsd3ZzFFsrW9p0ZRYUCOKWDwVAoKXxC1kw5Us7C2f8rsRlaTtAJH7pFQ=p-s50"/><br/>
                            {/* <img className="profile" src="https://qsf.fs.quoracdn.net/-3-images.new_grid.profile_pic_default.png-26-345032f7d91f49f2.png"></img><br /> */}
                            <i class="fa fa-user-o icons"><span style={{ color: "white" }}>&nbsp;&nbsp;First Name:&nbsp;&nbsp;{localStorage.getItem('first_name')}</span></i><br />
                            <i class="fa fa-user-o icons"><span style={{ color: "white" }}>&nbsp;&nbsp;Last Name:&nbsp;&nbsp;{localStorage.getItem('last_name')}</span></i><br />
                            <i class="fa fa-envelope-o icons"><span style={{ color: "white" }}>&nbsp;&nbsp;Email Address:&nbsp;&nbsp;{localStorage.getItem('email')}</span></i><br />
                            <i class="fa fa-phone icons"><span style={{ color: "white" }}>&nbsp;&nbsp;Contact:&nbsp;&nbsp;{localStorage.getItem('phone')}</span></i><br />
                            <i class="fa fa-building icons" ><span style={{ color: "white" }}>&nbsp;&nbsp;City:&nbsp;&nbsp;{localStorage.getItem('city')}</span></i><br />
                            <i class="fa fa-building icons"><span style={{ color: "white" }}>&nbsp;&nbsp;State:&nbsp;&nbsp;{localStorage.getItem('state')}</span></i><br />
                            <i class="fa fa-globe icons"><span style={{ color: "white" }}>&nbsp;&nbsp;Country:&nbsp;&nbsp;{localStorage.getItem('country')}</span></i><br />
                            <i class="fas fa-gopuram icons" ><span style={{ color: "white" }}>&nbsp;&nbsp;Religion:&nbsp;&nbsp;{localStorage.getItem('religion')}</span></i><br />
                            <i class="fa fa-lock icons"><span style={{ color: "white" }}>&nbsp;&nbsp;Security Question:&nbsp;&nbsp;{localStorage.getItem('securityQues')}</span></i><br />
                            <Link to="/updateDetails" className="login" style={{marginLeft:"15%"}}>Click Here to Update Details</Link>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
                    );
                }
            }      
export default UserProfile;