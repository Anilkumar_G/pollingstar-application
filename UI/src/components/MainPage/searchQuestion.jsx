//npm dependencies
import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

//stylesheets
import '../../Assets/css/HomePage.css';

//Component Dependencies
import *as searchQuestionActions from '../../store/actions/searchQuestionActions';
import MainHeader from './mainHeader';
import SideNav from './sideNav';
import * as VotingActions from '../../store/actions/votingActions';
import Image from '../../Assets/images/questa.gif';
import * as addAnswers from '../../store/actions/addAnswers';



class searchQuestion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            answer: '',
            answerError: '',
            isSubmitted: false,
            is_answer_added: false,
            is_vote_added: false,
        }
        this.handleChange = this.handleChange.bind(this);
    }

    componentWillMount() {

        this.props.searchQuestionActions.doSearchQuestion(
            {
                question: this.props.match.params.search
            }
        );
    }

    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value }, () => {
            this.validateComment();
        });
        e.preventDefault();
    }
    validateComment = () => {
        const { answer } = this.state;
        this.setState({
            answerError:
                answer.length > 30 ? null : 'Comment Should be between 1 to 50'
        });
    }
    addAnswer = (question_id) => {
        if (this.state.answer.length > 30 && this.state.answerError == null) {
            this.setState({ isValidate: false })
        }
        if (this.state.answer.length <= 30) {
            this.props.addAnswers.addAnswer({
                answer: this.state.answer,
                question_id
            });
        }
        console.log(question_id)
        this.setState({
            isSubmitted: true
        });
    }

    voteAdd1 = (question_id, id) => {
        var data = {
            question_id: question_id,
            id: id
        }
        this.props.VotingActions.addVote(data)
        this.setState({
            is_vote_added: true
        })
    }


    render() {

        return (



            <div>
                <MainHeader />
                <div className="container" style={{ marginTop: "5%" }}>
                    <div className="row">
                        <div className="col-sm-5">
                            <SideNav />
                        </div>
                        <div className="col-sm-7">
                            <div className="homeData">
                                <h2 style={{ color: "#00ba9d" }}>Search Results</h2>
                            </div><br />
                            <div >
                                {
                                    (this.props.searchQuestionData) ?
                                        (this.props.searchQuestionData.response) ?
                                            <div>
                                                {this.props.searchQuestionData.response.map((searchQuestion =>
                                                    (<div className="homeData" key={searchQuestion.question_id}>
                                                        <p className="link" href="#" style={{ padding: "10px" }}>{searchQuestion.question}<span style={{ float: "right" }}>Votes::{searchQuestion.numberOfVotes}</span></p>
                                                        <p className="link" style={{ fontSize: "100%", color: "gray" }}>Category:{searchQuestion.category}</p>
                                                        <p className="link" style={{ fontSize: "100%", color: "white" }}>Answers Posted By : </p>

                                                        {/* <i className="fa fa-user-circle" style={{ padding: "15px" }}>&nbsp;&nbsp;&nbsp;<span style={{ color: "white", fontFamily: "monospace", fontSize: "150%" }}>King Cho, Engineering from Alpha</span></i><br /> */}
                                                        <div>{(searchQuestion.answers) ?
                                                            (searchQuestion.answers.length) ?
                                                                // <div> =this.props.answers.map(answer => {
                                                                //     return <p  key={searchQuestion.answerId} style={{ textAlign: "justify", color: "white", padding: "10px" }}>{answer.answer}</p>
                                                                // })}
                                                                // </div>

                                                                <div> {searchQuestion.answers.map(answer => {
                                                                    // return <p style={{ textAlign: "justify", color: "white", padding: "10px" }}>Anser:&nbsp;&nbsp;{answer.answer}<span style={{ marginLeft: "5vw" }}>Votes : {answer.noOfVotes}</span><span><button value={answer.id} onClick={() => this.voteAdd1(searchQuestion.question_id, answer.id)} style={{ color: "#00ba9d", marginLeft: "5vw", backgroundColor: "rgba(0,0,0,0.3)", outlineColor: "reba(0,0,0,0.3)", border: "1px solid", borderRadius: "10px" }}><i className="fa fa-thumbs-up" >Up Vote</i></button></span></p>
                                                                    return (
                                                                        <tr style={{ textAlign: "justify", color: "white" }}>
                                                                            <td style={{ float: "left", color: "#00ba9d" }}><i className="fa fa-user-circle" />{answer.username}:&nbsp;&nbsp;<span style={{ color: "white" }}>{answer.answer}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                                            <td>Votes:&nbsp;&nbsp;{answer.noOfVotes}</td>
                                                                            <td><button value={answer.id} onClick={() => this.voteAdd1(searchQuestion.question_id, answer.id)} style={{ color: "#00ba9d", marginLeft: "5vw", backgroundColor: "rgba(0,0,0,0.3)", border: "1px solid", borderRadius: "10px" }}><i className="fa fa-thumbs-up" >Up Vote</i></button></td>
                                                                        </tr>
                                                                    )
                                                                })}
                                                                </div>



                                                                : <div><p style={{ textAlign: "justify", color: "white", padding: "10px" }}>No Answers Yet. Add your answer</p></div>
                                                            : <div><p style={{ textAlign: "justify", color: "white", padding: "10px" }}>No Answers Yet. Add your answer</p></div>


                                                        }
                                                        </div>
                                                        {/* <p style={{ textAlign: "justify", color: "white", padding: "10px" }}>{searchQuestion.answers.answer}</p> */}
                                                        <input type="text" name="answer" maxLength="30" onChange={this.handleChange} class="comment" placeholder="Post your Answer..."></input><button value={searchQuestion.question_id} onClick={() => this.addAnswer(searchQuestion.question_id)} className="button">POST</button><br />
                                                        {this.state.answerError ? <span style={{ color: "red", marginLeft: "10%" }}>*Answer Length must be 1 to 30 characters</span> : ''}<br />

                                                    </div>
                                                    )
                                                ))};
                                             </div>
                                            : <div >
                                                <img src={Image} alt="Loading" />
                                                <p style={{ color: "white" }}>Loading...</p>
                                            </div> : <div >
                                            <img src={Image} alt="Loading" />
                                            <p style={{ color: "white" }}>Loading...</p>
                                        </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

function mapStateToProps(state) {
    debugger;
    console.log(state.searchQuestionReducer.searchQuestionData);
    console.log("search questions");
    return {
        is_search_question: state.searchQuestionReducer.is_search_question,
        searchQuestionData: state.searchQuestionReducer.searchQuestionData,
        is_answer_added: state.addAnswerReducer,
        is_vote_added: state.addVotereducer.is_vote_added,

    };

}

function mapDispatchToProps(dispatch) {
    return {
        searchQuestionActions: bindActionCreators(searchQuestionActions, dispatch),
        addAnswers: bindActionCreators(addAnswers, dispatch),
        VotingActions: bindActionCreators(VotingActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(searchQuestion);
