//npm Dependencies
import React, { Component } from 'react';
import { Redirect, Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Alert } from 'reactstrap';

//Styling Dependencies
import '../../Assets/css/LoginPage.css'

//Component Dependencies
import * as addQuestionConstants from '../../store/actions/addQuestionConstants';
import MainHeader from './mainHeader';
import SideNav from './sideNav';

class AddQuestion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            question: '',  
            questionError:'', 
            category : '',
            isSubmitted: false,
            is_question_added: false,

        }
        this.handleChange = this.handleChange.bind(this);
        this.addQuestion = this.addQuestion.bind(this);
    }
    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value }, () => {
            this.validateQuestion();
        });
        e.preventDefault();
    }
    validateQuestion = () => {
        const { question } = this.state;
        console.log(this.state.question.length)
        this.setState({
            questionError:
                question.length > 50 ? null : 'Question Should be between 1 to 50'
        });
    }
    addQuestion = (e) => {  
        console.log(this.state.question.length)
        if (this.state.question.length > 50 && this.state.questionError == null) {
            this.setState({ isValidate: false })
            alert("Question Length must be between 1 to 50")
        }    
        this.setState({
            isSubmitted: true
        }); 
        if(this.state.question.length <= 50 && this.state.category != '' && this.state.question != '') {
            this.props.addQuestionConstants.addQuestion({
                question: this.state.question,
                category: this.state.category
            });
           
        }
    }

    render() {
        if (this.state.isSubmitted) {
            if(this.state.question == '') {            
                return (
                    <div>                
                        <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={()=>{window.location.reload()}}>&times;</a>
                        <strong>Question Field Can't be Empty</strong>
                        </div>   
                    </div>                                 
                )
                
            }
            if(this.state.category == '') {            
                return (
                    <div>                
                        <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={()=>{window.location.reload()}}>&times;</a>
                        <strong>Please Select the Question Category</strong>
                        </div>   
                    </div>                                 
                )
                
            }
            console.log(this.props.is_question_added)
       
            if (this.props.is_question_added) {           
            console.log(this.props.statuscode)
            if(this.props.statuscode == 1) {
                return (
                    <div>                
                        <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={()=>{window.location.reload()}}>&times;</a>
                        <strong>Your Question has been Posted</strong>
                        </div>   
                    </div>                                 
               )
            }
            if(this.props.statuscode == -1) {
                return (
                    <div>                
                        <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={()=>{window.location.reload()}}>&times;</a>
                        <strong>You have already asked this question</strong>
                        </div>   
                    </div>                                 
               )
            }
            console.log(this.props.statuscode)
            if(this.props.statuscode == -2) {
                return (
                    <div>                
                        <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={()=>{window.location.reload()}}>&times;</a>
                        <strong>You can't post a question with spaces</strong>
                        </div>   
                    </div>                                 
               )
            }
            if(this.props.statuscode == -3) {
                return (
                    <div>                
                        <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={()=>{window.location.reload()}}>&times;</a>
                        <strong>InValid User..! please SignIn</strong>
                        </div>   
                    </div>                                 
               )
            }
        }
    }
        return (
            <div>
                <MainHeader />
                <div className="container" style={{ marginTop: "5%" }}>
                    <div className="row">
                        <div className="col-sm-5">
                            <SideNav />
                        </div>
                        <div className="col-sm-7">
                            <div id="qusForms">
                                <div class="container">
                                    <p class="field">Enter your Question</p><br />
                                    <input class="textField" name="question" maxLength="50" type="text" onChange={this.handleChange} placeholder="Whats's Your Question" required></input><br /><br />
                                    {this.state.questionError ? <span style={{color:"red"}}>*Question Length must be 1 to 50 characters</span>:''}<br/>

                                </div><br /><br />
                                <div class="container">
                                    <p className="field">Please Select Question Category</p><br />
                                    <div class="form-group">
                                        <select className="textField" name="category" onChange={this.handleChange}>
                                            <option value="">Select Question Category</option>
                                            <option className="field" value="Anonymous">Anonymous</option>
                                            <option className="field" value="Education">Education</option>
                                            <option className="field" value="General knowledge">General knowledge</option>
                                            <option className="field" value="Sports" >Sports</option>
                                            <option className="field" value="Politics">Politics</option>
                                            <option className="field" value="Media">Media</option>
                                            <option className="field" value="Entertainment">Entertainment</option>
                                            <option className="field" value="LifeStyle">LifeStyle</option>
                                            <option className="field" value="Movies">Movies</option>
                                            <option className="field" value="Gadgets">Gadgets</option>
                                        </select>
                                    </div>
                                </div><br /><br /><br /><br/>
                                <div class="container" style={{ marginLeft: "10%" }}>
                                    <button value="Add" onClick={this.addQuestion} className="login" >Add</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
function mapStateToProps(state) {
    console.log(state.statuscode)
    return {
        is_question_added: state.addQuestionReducer.is_question_added,
        statuscode:state.addQuestionReducer.statuscode
    };

}

function mapDispatchToProps(dispatch) {
    return {
        addQuestionConstants: bindActionCreators(addQuestionConstants, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(AddQuestion);