//npm Dependencies
import React, { Component }             from 'react';
import { bindActionCreators }           from 'redux';
import { connect }                      from 'react-redux';


//Styling Dependencies
import '../../Assets/css/QuestionModal.css'

//Component Dependencies
import * as addQuestionConstants        from '../../store/actions/addQuestionConstants';

class QuestionModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            question: '',
            questionError:'',
            category : '',
            isSubmitted: false,
            is_question_added: false
        }
        this.handleChange = this.handleChange.bind(this);
        this.addQuestion = this.addQuestion.bind(this);
    }
    // handleChange = e => {
    //     this.setState({
    //         [e.target.name]: e.target.value
    //     });

    //     e.preventDefault();
    // }

    // addQuestion = e => {
    //     console.log(this.state.question)
    //     this.props.addQuestionConstants.addQuestion({
    //         question: this.state.question
    //     });
    //     this.setState({
    //         isSubmitted: true
    //     });
    // }
    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value }, () => {
            this.validateQuestion();
        });
        e.preventDefault();
    }
    validateQuestion = () => {
        const { question } = this.state;
        console.log(this.state.question.length)
        this.setState({
            questionError:
                question.length > 50 ? null : 'Question Should be between 1 to 50'
        });
    }
    addQuestion = (e) => {  
        console.log(this.state.question.length)
        if (this.state.question.length > 50 && this.state.questionError == null) {
            this.setState({ isValidate: false })
            // alert("Question Length must be between 1 to 50")
        }  
        this.setState({
            isSubmitted: true
        });    
        if(this.state.question.length <= 50 && this.state.category != ''&&this.state.question != '') {
            this.props.addQuestionConstants.addQuestion({
                question: this.state.question,
                category: this.state.category
            });       
        }
    }

    render() {
        if (this.state.isSubmitted) {
            console.log(this.state.isSubmitted)
            console.log(this.state.question)
            if (this.state.question == '') {
                return (                 
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={() => { window.location.reload() }}>&times;</a>
                        <strong>Question Field Can't be Empty</strong>
                    </div>
                )
            }
            else if (this.state.category == '') {
                return (                    
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={() => { window.location.reload() }}>&times;</a>
                        <strong>Please Select One of the Question Categories</strong>
                    </div>

                )

            }
            else if (this.props.is_question_added ) {
                console.log(this.props.statuscode)
                if (this.props.statuscode == 1) {
                    return (                      
                        <div class="alert alert-success alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={() => { window.location.reload() }}>&times;</a>
                            <strong>Your Question has been Posted</strong>
                        </div>                 
                    )
                }
                if (this.props.statuscode == -1) {
                    return (                    
                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={() => { window.location.reload() }}>&times;</a>
                            <strong>You have already asked this question</strong>
                        </div>                       
                    )
                }
            }
        }
            return (
                <div>
                    <div class="modal" id="myModal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <p class="modal-title">Add Question</p>&nbsp;&nbsp;&nbsp;
                                    
                                <button type="button" class="close" data-dismiss="modal" onClick={() => window.location.reload()}>&times;</button>
                                </div>
                                <div class="modal-body">
                                    <input className="questionLink" type="text" maxLength="50" name="question" placeholder="What's Your Question" onChange={this.handleChange}></input><br/><br/>
                                    {this.state.questionError ? <span style={{color:"red"}}>*Question Length must be 1 to 50 characters</span>:''}<br/>
                                    <div class="form-group">
                                        <select className="textField" name="category" onChange={this.handleChange}>
                                            <option value="">Select Question Category</option>
                                            <option className="field" value="Anonymous">Anonymous</option>
                                            <option className="field" value="Education">Education</option>
                                            <option className="field" value="General knowledge">General knowledge</option>
                                            <option className="field" value="Sports" >Sports</option>
                                            <option className="field" value="Politics">Politics</option>
                                            <option className="field" value="Media">Media</option>
                                            <option className="field" value="Entertainment">Entertainment</option>
                                            <option className="field" value="LifeStyle">LifeStyle</option>
                                            <option className="field" value="Movies">Movies</option>
                                            <option className="field" value="Gadgets">Gadgets</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-success" data-dismiss="modal" onClick={this.addQuestion}>Add Question</button>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    }
function mapStateToProps(state) {
    return {
        is_question_added: state.addQuestionReducer,
        statuscode: state.addQuestionReducer.statuscode
    };

}

function mapDispatchToProps(dispatch) {
    return {
        addQuestionConstants: bindActionCreators(addQuestionConstants, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(QuestionModal);