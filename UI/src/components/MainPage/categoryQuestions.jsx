//npm Dependencies
import React, { Component }                     from 'react';
import { Redirect, Link }                       from 'react-router-dom';
import { bindActionCreators }                   from 'redux';
import { connect }                              from 'react-redux';
import { Button, Alert }                        from 'reactstrap';

//Styling Dependencies
import '../../Assets/css/LoginPage.css'

//Component Dependencies
import * as categoryQuestionConstants           from '../../store/actions/categoryQustActions';
import MainHeader                               from './mainHeader';
import * as addAnswers                          from '../../store/actions/addAnswers';
import * as VotingActions                       from '../../store/actions/votingActions';
import Image                                    from '../../Assets/images/questa.gif';
import SideNav                                  from './sideNav';

class Category extends Component {
    constructor(props) {
        super(props);
        this.state = {
            category: '',
            isSubmitted: false,
            answer: '',
            answerError: '',
            is_answer_added: false,
            is_vote_added: false,
            visible : false
        }
        this.handleChange = this.handleChange.bind(this);
        this.categorize = this.categorize.bind(this);
    }
    // handleChange = e => {
    //     this.setState({
    //         [e.target.name]: e.target.value
    //     });
    //     console.log(this.state.category)

    //     e.preventDefault();
    // }
    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value }, () => {
            this.validateComment();
        });
        e.preventDefault();
    }
    validateComment = () => {
        const { answer } = this.state;
        console.log(this.state.answer.length)
        this.setState({
            answerError:
                answer.length > 30 ? null : 'Comment Should be between 1 to 50'
        });
    }
    addAnswer = (question_id) => {
        if (this.state.answer.length > 30 && this.state.answerError == null) {
            this.setState({ isValidate: false })
            // alert("Answer Length must be between 1 to 30")
        }
        if (this.state.answer.length <= 30) {
            this.props.addAnswers.addAnswer({
                answer: this.state.answer,
                question_id
            });
        }
        console.log(question_id)
        this.setState({
            isSubmitted: true
        });
    }
    categorize = e => {
        if (this.state.category == ''){
            // alert("please select any one of the question category..!")
            this.setState({
                visible : true
            })
        }
        else{
        this.props.categoryQuestionConstants.fetchCategoryQuestions({
            category: this.state.category
        })
        this.setState({
            isSubmitted: true
        });
    }
    }

    toggle(){
        this.setState({
            visible:false
        })
    }

    voteAdd1 = (question_id, id) => {
        var data = {
            question_id: question_id,
            id: id
        }
        this.props.VotingActions.addVote(data)
        // this.props.VotingActions.addVote(data);
        this.setState({
            is_vote_added: true,
            isSubmitted: true
        })
    }

    render() {
        if (this.state.isSubmitted) {
            if (this.state.category == '') {
                // return (
                //     <div>
                //         <div class="alert alert-danger alert-dismissable">
                //             <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={() => { window.location.reload() }}>&times;</a>
                //             <strong>Please Select One of the Category</strong>
                //         </div>
                //     </div>
                // )
            }
            if (this.props.is_answer_added) {
                if (this.props.answerStatusCode.payload == 1) {
                    return (
                        <div>
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={() => { window.location.reload() }}>&times;</a>
                                <strong>Your Answer has been Submitted</strong>
                            </div>
                        </div>
                    )
                }
                if (this.props.answerStatusCode.payload == -3) {
                    return (
                        <div>
                            <div class="alert alert-danger alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={() => { window.location.reload() }}>&times;</a>
                                <strong>Answer Field Must be Filled</strong>
                            </div>
                        </div>
                    )
                }
            }

            if (this.props.is_vote_added) {
                if (this.props.votesStatusCode == 1) {
                    return (
                        <div>
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={() => { window.location.reload() }}>&times;</a>
                                <strong>Thanks For Voting..</strong>
                            </div>
                        </div>
                    )
                }
                if (this.props.votesStatusCode == 2) {
                    return (
                        <div>
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={() => { window.location.reload() }}>&times;</a>
                                <strong>Your Vote Has Been Updated</strong>
                            </div>
                        </div>
                    )
                }
            }
        }
        return (
            <div>
                <MainHeader />
                { this.state.visible?
                <Alert> <a class="close" data-dismiss="alert" aria-label="close" onClick = {this.toggle.bind(this)}>&times;</a><strong color = "primary" > Please Select One of the Category</strong></Alert>
                :<div></div>
            }
                <div className="container" style={{ marginTop: "5%" }}>
                    <div className="row">
                        <div className="col-sm-5">
                            <SideNav />
                        </div>
                        <div className="col-sm-7">
                            <div id="forms1">
                                <div class="container">
                                    <p className="field">Get Questions By Category</p><br />
                                    <div class="form-group">
                                        <select className="textField" name="category" onChange={this.handleChange}>
                                            <option value="">Select Question Category</option>
                                            <option className="field" value="Anonymous">Anonymous</option>
                                            <option className="field" value="Education">Education</option>
                                            <option className="field" value="General knowledge">General knowledge</option>
                                            <option className="field" value="Sports" >Sports</option>
                                            <option className="field" value="Politics">Politics</option>
                                            <option className="field" value="Media">Media</option>
                                            <option className="field" value="Entertainment">Entertainment</option>
                                            <option className="field" value="LifeStyle">LifeStyle</option>
                                            <option className="field" value="Movies">Movies</option>
                                            <option className="field" value="Gadgets">Gadgets</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="container" style={{ marginLeft: "10%" }}>
                                    <button value="Add" onClick={this.categorize} className="login" style={{ width: '300px' }} >Get Questions</button>
                                </div>
                            </div>
                            <div >
                                {
                                    (this.props.questions) ?
                                        <div>
                                            {/* if({this.props.questions.category != null}) */}
                                            {this.props.questions.map((question =>
                                                (<div className="homeData" key={question.question_id}>

                                                    <p className="link" href="#" >{question.question}<span style={{ float: "right" }}>Votes::{question.numberOfVotes}</span></p>
                                                    <p style={{ fontSize: "100%", color: "gray" }}>Category:{question.category}</p>
                                                    <p className="link" style={{ fontSize: "100%", color: "white" }}>Answers Posted By : </p>

                                                    {/* <i className="fa fa-user-circle" style={{ padding: "15px" }}>&nbsp;&nbsp;&nbsp;<span style={{ color: "white", fontFamily: "monospace", fontSize: "150%" }}>King Cho, Engineering from Alpha</span></i><br /> */}
                                                    <div>{(question.answers) ?
                                                        (question.answers.length) ?
                                                            <div> {question.answers.map(answer => {
                                                                return (

                                                                    <tr style={{ textAlign: "justify", color: "white" }}>
                                                                        <td style={{ float: "left", color: "#00ba9d" }}><i className="fa fa-user-circle" />{answer.username}:&nbsp;&nbsp;<span style={{ color: "white" }}>{answer.answer}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                                        <td>Votes:&nbsp;&nbsp;{answer.noOfVotes}</td>
                                                                        <td><button value={answer.id} onClick={() => this.voteAdd1(question.question_id, answer.id)} style={{ color: "#00ba9d", marginLeft: "5vw", backgroundColor: "rgba(0,0,0,0.3)", border: "1px solid", cursor: "pointer", borderRadius: "10px" }}><i className="fa fa-thumbs-up" >Up Vote</i></button></td>

                                                                    </tr>

                                                                )
                                                            })}
                                                            </div>


                                                            : <div><p style={{ textAlign: "justify", color: "white", padding: "10px" }}>No Answers Yet. Add your answer</p></div>
                                                        : <div><p style={{ textAlign: "justify", color: "white", padding: "10px" }}>No Answers Yet. Add your answer</p></div>


                                                    }
                                                    </div>
                                                    {/* <p style={{ textAlign: "justify", color: "white", padding: "10px" }}>{question.answers.answer}</p> */}
                                                    {/* <Link to="/home"><i className="fa fa-thumbs-up" style={{ color: "#00ba9d", fontSize: "150%" }}>Up Vote</i></Link><input type="text" name="answer" onChange={this.handleChange} class="comment" maxLength="50" placeholder="Post your Answer..."></input><button onClick={this.addAnswer.bind(this, question.question_id)}  className="button">POST</button> */}
                                                    <input type="text" name="answer" onChange={this.handleChange} class="comment" maxLength="30" placeholder="Post your Answer..." required></input><button value={question.question_id} onClick={() => this.addAnswer(question.question_id)} className="button">POST</button><br />
                                                    {this.state.answerError ? <span style={{ color: "red", marginLeft: "10%" }}>*Answer Length must be 1 to 30 characters</span> : ''}<br />

                                                </div>
                                                )
                                            ))};
                                        </div>
                                        : <div >
                                            <img src={Image} alt="Loading" />
                                            <p style={{ color: "white" }}>Loading...</p>
                                        </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    console.log(state.questionStatusCode)
    return {
        questions: state.fetchCategoryQuestionReducer.questions,
        questionStatusCode: state.fetchCategoryQuestionReducer.statuscode,
        // is_answer_added: state.addAnswerReducer,
        // is_vote_added: state.addVotereducer.is_vote_added,
        is_answer_added: state.addAnswerreducer,
        answerStatusCode: state.addAnswerreducer,
        is_vote_added: state.addVotereducer.is_vote_added,
        votesStatusCode: state.addVotereducer.statuscode
    };

}

function mapDispatchToProps(dispatch) {
    return {
        categoryQuestionConstants: bindActionCreators(categoryQuestionConstants, dispatch),
        VotingActions: bindActionCreators(VotingActions, dispatch),
        addAnswers: bindActionCreators(addAnswers, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Category);