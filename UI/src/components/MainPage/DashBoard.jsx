//npm Dependencies
import React, { Component }                     from 'react';
import { Link, Redirect }                       from 'react-router-dom';
import { bindActionCreators }                   from 'redux';
import { connect }                              from 'react-redux';

//Styling Dependencies
import                                               '../../Assets/css/HomePage.css';

//Component Dependencies
import * as FetchQuestionActions                from '../../store/actions/FetchQuestionActions';
import MainHeader                               from './mainHeader';
import SideNav                                  from './sideNav';
import QuestionModal                            from './QuestionModal';
import * as addAnswers                          from '../../store/actions/addAnswers';
import * as VotingActions                       from '../../store/actions/votingActions';
import Image                                    from '../../Assets/images/questa.gif';




/** 
 * Purpose of this Class is to display the DashBoard 
 * of the User When he/she Logged in
*/
// $(".button").on("keypress", function () {
//     if ($(this).val().length >= 50) {
//         alert("gsdkjfghdkfj")
//     }
// });
class DashBoard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            answer: '',
            answerError: '',
            isSubmitted: false,
            is_answer_added: false,
            is_vote_added: false,
        }
        this.handleComment = this.handleComment.bind(this);
    }

    componentDidMount() {
        this.props.FetchQuestionActions.fetchQuestions();
    }
    handleComment = e => {
        this.setState({ [e.target.name]: e.target.value }, () => {
            this.validateComment();
        });
        e.preventDefault();
    }
    validateComment = () => {
        const { answer } = this.state;
        console.log(this.state.answer.length)
        this.setState({
            answerError:
                answer.length > 30 ? null : 'Comment Should be between 1 to 50'
        });
    }
    addAnswer = (question_id) => {  
        if (this.state.answer.length > 30 && this.state.answerError == null) {
            this.setState({ isValidate: false })
            // alert("Answer Length must be between 1 to 30")
        }     
        if(this.state.answer.length <= 30) {
            this.props.addAnswers.addAnswer({
                answer: this.state.answer,
                question_id
            });
        }
        console.log(question_id)
        this.setState({
            isSubmitted: true
        });
    }

    voteAdd1 = (question_id, id) => {
        var data = {
            question_id: question_id,
            id: id
        }
        this.props.VotingActions.addVote(data)
        // this.props.VotingActions.addVote(data);
        this.setState({
            isSubmitted: true

        })
    }
    doAnalatics = (question_id) =>{
        window.location.href =  '/analatics/' + question_id
    }
    render() {

        if (this.state.isSubmitted) {           
            // console.log(this.props.is_answer_added)
            // console.log(this.props.answerStatusCode.payload)
            console.log(this.props.is_vote_added)
            console.log(this.props.votesStatusCode)
            if (this.props.is_answer_added) {
                if (this.props.answerStatusCode.payload == 1) {
                    return (
                        <div>
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={() => { window.location.reload() }}>&times;</a>
                                <strong>Your Answer has been Submitted</strong>
                            </div>
                        </div>
                    )
                }
                if (this.props.answerStatusCode.payload == -3) {
                    return (
                        <div>
                            <div class="alert alert-danger alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={() => { window.location.reload() }}>&times;</a>
                                <strong>Answer Field Must be Filled</strong>
                            </div>
                        </div>
                    )
                }
            }

            if (this.props.is_vote_added) {
                if (this.props.votesStatusCode == 1) {
                    return (
                        <div>
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={() => { window.location.reload() }}>&times;</a>
                                <strong>Thanks For Voting..</strong>
                            </div>
                        </div>
                    )
                }
                if (this.props.votesStatusCode == 2) {
                    return (
                        <div>
                            <div class="alert alert-success alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={() => { window.location.reload() }}>&times;</a>
                                <strong>Your Vote Has Been Updated</strong>
                            </div>
                        </div>
                    )
                }
            }
        }
        console.log(this.props.questions)
        return (
            <div>
                <MainHeader />
                <div className="container" style={{ marginTop: "5%" }} >
                    <div className="row">
                        <div className="col-sm-5">
                            <SideNav />
                        </div>
                        <div className="col-sm-7">
                            <div className="homeData">
                                <i className="fa fa-user-circle">&nbsp;&nbsp;&nbsp;<span style={{ color: "white", fontFamily: "Cambria, Cochin, Georgia, Times, 'Times New Roman', serif", fontSize: "150%" }}>{localStorage.getItem('first_name')}</span></i><br />
                                <Link className="link" to="/addquestion" data-toggle="modal" data-target="#myModal">What is your Question?</Link>
                                <QuestionModal />
                            </div><br />
                            <div >
                                {
                                    (this.props.questions) ?
                                        <div>
                                            {this.props.questions.map((question =>
                                                (<div className="homeData" key={question.question_id}>
                                                    <p className="link" href="#">{question.question}<span style={{float:"right"}}>Votes::{question.numberOfVotes}</span></p>
                                                    <p className="link" style={{ fontSize: "100%",color:"gray" }}>Category:{question.category}</p>
                                                    <p className="link" style={{ fontSize: "100%", color: "white" }}>Answers Posted By : </p>
                                                    {/* <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>Posted By</th>
                                                                <th>Posted Answers</th>
                                                                <th>Votes</th>
                                                                <th>Click Below to Vote</th>
                                                            </tr>
                                                        </thead>
                                                    </table> */}
                                                    {/* <i className="fa fa-user-circle" style={{ padding: "15px" }}>&nbsp;&nbsp;&nbsp;<span style={{ color: "white", fontFamily: "monospace", fontSize: "150%" }}>King Cho, Engineering from Alpha</span></i><br /> */}
                                                    <div>{(question.answers) ?
                                                        (question.answers.length) ?
                                                            <div> {question.answers.map(answer => {
                                                                return (

                                                                    <tr style={{ textAlign: "justify", color: "white" }}>
                                                                        <td style={{ float: "left", color: "#00ba9d" }}><i className="fa fa-user-circle" />{answer.username}:&nbsp;&nbsp;<span style={{ color: "white" }}>{answer.answer}</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                                        <td>Votes:&nbsp;&nbsp;{answer.noOfVotes}</td>
                                                                        <td><button value={answer.id} onClick={() => this.voteAdd1(question.question_id, answer.id)} style={{ color: "#00ba9d", marginLeft: "5vw", backgroundColor: "rgba(0,0,0,0.3)", border: "1px solid", cursor: "pointer", borderRadius: "10px" }}><i className="fa fa-thumbs-up" >Up Vote</i></button></td>

                                                                    </tr>

                                                                )
                                                            })}
                                                            </div>


                                                            : <div><p style={{ textAlign: "justify", color: "white", padding: "10px" }}>No Answers Yet. Add your answer</p></div>
                                                        : <div><p style={{ textAlign: "justify", color: "white", padding: "10px" }}>No Answers Yet. Add your answer</p></div>

                                                    }
                                                    </div>
                                                    {/* <p style={{ textAlign: "justify", color: "white", padding: "10px" }}>{question.answers.answer}</p> */}
                                                    {/* <Link to="/home"><i className="fa fa-thumbs-up" style={{ color: "#00ba9d", fontSize: "150%" }}>Up Vote</i></Link><input type="text" name="answer" onChange={this.handleChange} class="comment" maxLength="50" placeholder="Post your Answer..."></input><button onClick={this.addAnswer.bind(this, question.question_id)}  className="button">POST</button> */}
                                                    <br />
                                                    <input type="text" aria-describedby="headinghelp" name="answer" onChange={this.handleComment} maxLength="50" onBlur={this.validateComment} class={`comment ${this.state.answerError ? 'is-invalid' : ''}`}  placeholder="Post your Answer..." required></input><button value={question.question_id} onClick={() => this.addAnswer(question.question_id)} className="button">POST</button><button value={question.question_id} onClick={() => this.doAnalatics(question.question_id)}  className="button" style={{borderRadius:"80%"}}>ANALYTICS</button>
                                                    {this.state.answerError ? <span style={{color:"red",marginLeft:"10%"}}>*Answer Length must be 1 to 30 characters</span>:''}<br/>





                                                    

                                                </div>
                                                )
                                            ))}
                                        </div>
                                        : <div >
                                        <img src = {Image} alt = "Loading" />
                                        <p style = {{color : "white"}}>Loading...</p>
                            </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    console.log(state.fetchQuestionReducer.questions)
    return {
        questions: state.fetchQuestionReducer.questions,
        is_answer_added: state.addAnswerreducer,
        answerStatusCode: state.addAnswerreducer,
        is_vote_added: state.addVotereducer.is_vote_added,
        votesStatusCode: state.addVotereducer.statuscode
    };

}

function mapDispatchToProps(dispatch) {
    return {
        FetchQuestionActions: bindActionCreators(FetchQuestionActions, dispatch),
        addAnswers: bindActionCreators(addAnswers, dispatch),
        VotingActions: bindActionCreators(VotingActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(DashBoard);