//npm dependencies
import React, { Component }         from 'react';
import { Link }                     from 'react-router-dom';
import '../../Assets/css/HomePage.css';
import $                            from 'jquery';

// $('.vertical-menu').on('click','ul li',function(){
//     $(this).addClass('active').siblings().removeClass('active')
// })
class SideNav extends Component {
    state = {}
    render() {
        return (
            <div>
                <div>
                    <div class="vertical-menu">
                        <Link to="/home"><li>Home</li></Link>
                        <Link to="/myQuestions"><li>My Questions</li></Link>
                        <Link to="/addquestion"><li>Add Questions</li></Link>
                        <Link to="/recent"><li>Recent Questions</li></Link>
                        <Link to="/trending"><li>Trending Questions</li></Link>
                        <Link to="/category" onClick = {() => window.location.reload()}><li>Categorywise Questions</li></Link>
                    </div>
                </div>

                {/* <div class="list-group vertical-menu">
                    <a class="list-group-item active" href="/home">Home</a>
                    <a class="list-group-item" href="/myQuestions" data-toggle="tab">My Questions</a>
                    <a class="list-group-item" href="/addquestion" data-toggle="tab">Add Questions</a>
                    <a class="list-group-item" href="/recent" data-toggle="tab">Recent Questions</a>
                    <a class="list-group-item" href="/trending" data-toggle="tab">Trending Questions</a>
                    <a class="list-group-item" href="/category" data-toggle="tab">CategoryWise Questions</a>

                </div> */}
            
                {/* <ul class="vertical-menu">
                    <li class="active"><Link to="/home">Home</Link></li>
                    <li><Link to="/home">Home</Link></li>
                    <li><Link to="/home">Home</Link></li>
                    <li><Link to="/home">Home</Link></li>
                    <li><Link to="/home">Home</Link></li>
                    <li><Link to="/home">Home</Link></li>
                </ul> */}
            
            </div>
        );
    }
}

export default SideNav;