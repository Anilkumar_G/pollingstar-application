import React, { Component } from 'react';
import '../../Assets/css/LoginPage.css'
import {Redirect, Link} from 'react-router-dom';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as addAnswers from '../../store/actions/addAnswers';

class AddAnswer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            answer: '',
            isSubmitted: false,
            is_answer_added : false
        }
        this.handleChange = this.handleChange.bind(this);   
        this.addAnswer = this.addAnswer.bind(this);
    }
    handleChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });

        e.preventDefault();
    }
    
    addQuestion = e => {
        console.log(this.state.question)
        this.props.addAnswers.addAnswer({
            answer: this.state.answer
        });
        this.setState({
            isSubmitted: true
        });
    }
   
    render() {
        if (this.state.isSubmitted) {
            console.log(this.props.is_answer_added)
            if(this.props.is_answer_added) {
               
                let response = window.confirm("Do you wanna add more answers")

                if (response == true){

                    return <Redirect to = "/addquestion" />
                }
                else{
                  return  <Redirect to = "/" />
                }
            }
        }
        return (
            <div   id = "forms">
                    <div class="container">
                        <p class="field">Post your Answer your Question</p><br />
                        <input class="textField" name="question" type="text" onChange = {this.handleChange} placeholder="Whats's Your Question" required></input><br /><br />
                    </div><br /><br /><br /><br /><br />
                    <div class="container" style={{ marginLeft: "10%" }}>
                    <button value = "Add" onClick={this.addQuestion} className= "login" >Add</button>
                    </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        is_question_added: state.addQuestionreducer
    };

}

function mapDispatchToProps(dispatch) {
    return {
        addAnswers: bindActionCreators(addAnswers, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(AddAnswer);