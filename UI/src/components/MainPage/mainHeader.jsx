import React, { Component }                 from 'react';
import { Link, Redirect }                   from 'react-router-dom';
import { bindActionCreators }               from 'redux';
import { connect }                          from 'react-redux';
import SearchQuestion                       from './searchQuestion';
import logo                                 from '../../Assets/images/questa.png';
import {Alert}                              from 'reactstrap';

//Styling Dependencies
import '../../Assets/css/HomePage.css';

//Component Dependencies
import *as searchQuestionActions            from '../../store/actions/searchQuestionActions';
import *as authActions                      from '../../store/actions/authActions';
/** 
 * Purpose of this Class is to display the DashBoard 
 * of the User When he/she Logged in
*/
class MainHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoggedOut: false,
            question: '',
            searchQuetion: '',
            searchError : false
        }
        this.handleChange = this.handleChange.bind(this);
        this.doSearch = this.doSearch.bind(this);
        this.doLogout = this.doLogout.bind(this);
    }
    handleChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
        e.preventDefault();
    }
    doSearch = (search) => {
        debugger
     if(this.state.question != ''){   
      window.location.href =  '/searchQuestion/' + search 
     }
     else{
         this.setState({
             searchError : true
         })
     }  
    }

    toggle(){
        this.setState({
            searchError : false
        })
    }

    doLogout() {
        localStorage.removeItem('user_id');
        localStorage.removeItem('email');
        localStorage.removeItem('first_name');
        localStorage.removeItem('last_name');
        localStorage.removeItem('city');
        localStorage.removeItem('state');
        localStorage.removeItem('country');
        localStorage.removeItem('religion');
        localStorage.removeItem('phone');
        localStorage.removeItem('jwt-token');
        localStorage.removeItem('securityQues');
        localStorage.removeItem('securityAns');
        this.setState({
            isLoggedOut: true
        });
    }

    render() {
        if (this.state.isLoggedOut) {
            return <Redirect to="/" ></Redirect>
        }

        return (
            <div>
                <nav className="navbar navbar-expand" id="headerStyle">
                    <div className="navbar-header">
                        <img src={logo} style={{width:"20%",height:"10%",marginTop:"-1%"}}/>
                        <Link className="navbar-brand" to="/home" id="logo" style={{marginLeft:"-5%"}}>Questa</Link>
                    </div>
                    <ul className="navbar-nav" style={{ marginLeft: "28%" }}>
                        <li>
                            <input type="text" style={{color:"white"}} className="searchInput" placeholder="Search.." name="question" onChange={this.handleChange} required/>&nbsp;&nbsp;&nbsp;
                            <button className="searchButton" type="submit" onClick={() => this.doSearch(this.state.question)}><i class="fa fa-search"></i></button>
                            {/* <SearchQuestion searchQuestionData={this.props.searchQuestionData.response}/> */}
                        </li>
                        <li>
                            <Link to="/home/details"><i className="fa fa-user-circle-o" style = {{marginTop : "65%"}}></i></Link>
                        </li>
                        <li>
                            <Link className="nav-link" to="/" onClick={this.doLogout}>LOGOUT</Link>
                        </li>
                    </ul>
                </nav>
                {
                    this.state.searchError ?
                    <Alert>
                        <a class="close" data-dismiss="alert" aria-label="close" onClick={this.toggle.bind(this)}>&times;</a>
                         <strong>Oops!</strong>Please Enter Search input
                    </Alert>
                    :<div></div>
                }
            </div>
        )
    }
}

function mapStateToProps(state) {

    return {
        is_search_question: state.searchQuestionReducer.is_search_question,
        searchQuestionData: state.searchQuestionReducer.searchQuestionData,

    };

}

function mapDispatchToProps(dispatch) {
    return {
        searchQuestionActions: bindActionCreators(searchQuestionActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(MainHeader);
    