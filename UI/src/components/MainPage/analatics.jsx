//npm dependencies
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import FusionCharts from 'fusioncharts';
import Charts from 'fusioncharts/fusioncharts.charts';
import ReactFC from 'react-fusioncharts';
import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
import Column2D from 'fusioncharts/fusioncharts.charts';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as analaticsActions from '../../store/actions/analaticsActions';
ReactFC.fcRoot(FusionCharts, Column2D, FusionTheme);


class Analatics extends Component {
    constructor(props) {
        super(props);
        this.state = {
            analatics: []
        }
    }


    componentWillMount() {
        debugger;
        //calling city analatics  actions
        this.props.analaticsActions.doFetchAnalaticsData({
 
            question: {
                question_id: this.props.match.params.question_id
            }
        });

        //calling state analatics  actions 
        this.props.analaticsActions.doFetchStateAnalaticsData({
            question: {
                question_id: this.props.match.params.question_id
            }
        });


        //calling country analatics actions

        this.props.analaticsActions.doFetchCountryAnalaticsData({
            question: {
                question_id: this.props.match.params.question_id
            }
        });
    }
    doCityAnalatics = (e) => {
        debugger;

        this.setState({ analatics: this.props.analaticsQuestionData });
    }

    doStateAnalatics = (e) => {
        this.setState({ analatics: this.props.analaticsStateQuestionData });
    }
    doCountryAnalatics = (e) => {
        this.setState({ analatics: this.props.analaticsCountryQuestionData });
    }

    doRelegionAnalatics = (e) => {

    }

    render() {

        const dataSource = {

            chart: {
                caption: "ANALATICS",
                subCaption: "based on no of votes",

                xAxisName: "region",
                yAxisName: "no of votes",
                // // decimalSeparator: 1,
                // theme: "fusion",
                // forceYAxisValueDecimals: 0,
                // yAxisValueDecimals: 1,
                // Range: 1,
                // yAxisValueDecimals: 1,
                // showYAxisValues: 1,
                // bgColor: `#FF5904, #FFFFFF`
                "yAxisMaxValue": "100",
                "yAxisMinValue": "0"
            },
            data: this.state.analatics,
        };
        const chartConfigs = {
            type: "column2d",
            renderAt: "chartContainer",
            width: "100%",
            height: 400,
            dataFormat: "json",
            dataSource: dataSource
        };

        return (
            <div>
                <div className="row">
                    <div className="col-md-3">
                        <button value="" onClick={() => this.doCityAnalatics()} className="button">CityAnalatics</button>
                    </div>
                    <div className="col-md-3">
                        <button value="" onClick={() => this.doStateAnalatics()} className="button">StateAnalatics</button>
                    </div>
                    <div className="col-md-3">
                        <button value="" onClick={() => this.doCountryAnalatics()} className="button">CountryAnalatics</button>
                    </div>
                    <div className="col-md-3">
                        <button value="" onClick={() => this.doRelegionAnalatics()} className="button">RelegionAnalatics</button>
                    </div>
                </div>
                <ReactFC {...chartConfigs} />

            </div>
        );
    }
}




function mapStateToProps(state) {
        debugger;
        console.log(state.analaticsQuestionReducer.analaticsQuestionData);
    return {
        analaticsQuestionData: state.analaticsQuestionReducer.analaticsQuestionData,
        analaticsStateQuestionData: state.analaticsQuestionReducer.analaticsStateQuestionData,
        analaticsCountryQuestionData: state.analaticsQuestionReducer.analaticsCountryQuestionData
    };

}

function mapDispatchToProps(dispatch) {
        debugger;
        return {
        analaticsActions: bindActionCreators(analaticsActions, dispatch)
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Analatics);



