import React, { Component } from 'react';
import '../../../Assets/css/LoginPage.css';
import { bindActionCreators } from 'redux';
import {Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

//Store Dependencies
import * as signUpActions from '../../../store/actions/signupActions';

/**
 * Purpose of this Component is to provide
 * the SignUp form to the user
 */

class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fields: {},
            errors: {},
            isSubmitted: false,
            doCancel: false
        }
        localStorage.removeItem('jwt-token');
        localStorage.removeItem('user_id');
        localStorage.removeItem('email');
        localStorage.removeItem('first_name');
        localStorage.removeItem('last_name');
        localStorage.removeItem('city');
        localStorage.removeItem('state');
        localStorage.removeItem('country');
        localStorage.removeItem('religion');
        localStorage.removeItem('phone');

        this.handleChange = this.handleChange.bind(this);
        this.doSignUp = this.doSignUp.bind(this);
        this.cancel = this.cancel.bind(this);
    }

    handleChange = (event) => {
        let fields = this.state.fields;
        fields[event.target.name] = event.target.value;
        this.setState({
            fields
        });

    }
    onKeyDown = (event) => {
        // 'keypress' event misbehaves on mobile so we track 'Enter' key via 'keydown' event
        if (event.key === 'Enter') {
          event.preventDefault();
          event.stopPropagation();
          this.doSignUp();
        }
      }
    doSignUp = e => {
        e.preventDefault();
        if (this.validateForm()) {
            let fields = {};
            fields["first_name"] = "";
            fields["last_name"] = "";
            fields["email"] = "";
            fields["phone"] = "";
            fields["city"] = "";
            fields["state"] = "";
            fields["country"] = "";
            fields["password"] = "";
            fields["religion"] = "";
            fields["securityQues"]="";
            fields["securityAns"]="";
            
            // fields["confirmPassword"] = "";
            console.log("i am sign up")
            this.props.signUpActions.signUpUser({
                first_name: this.state.fields.first_name,
                last_name: this.state.fields.last_name,
                gender:this.state.fields.gender,
                email: this.state.fields.email,
                password: this.state.fields.password,
                city: this.state.fields.city,
                state: this.state.fields.state,
                country: this.state.fields.country,
                religion: this.state.fields.religion,
                phone: this.state.fields.phone,
                securityQuestion: this.state.fields.securityQuestion,
                securityAnswer: this.state.fields.securityAnswer,
                religion: this.state.fields.religion
            })
            this.setState({
                isSubmitted: true
            })
        }

    }

    validateForm() {

        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;

        if (!fields["first_name"]) {
            formIsValid = false;
            errors["first_name"] = "*Please Enter Your first_name";
        }

        if (typeof fields["first_name"] !== "undefined") {
            if (!fields["first_name"].match(/^[a-zA-Z ]*$/)) {
                formIsValid = false;
                errors["first_name"] = "*Please Enter Alphabets Only";
            }
        }

        if (!fields["last_name"]) {
            formIsValid = false;
            errors["last_name"] = "*Please Enter Your last_name";
        }

        if (typeof fields["last_name"] !== "undefined") {
            if (!fields["last_name"].match(/^[a-zA-Z ]*$/)) {
                formIsValid = false;
                errors["last_name"] = "*Please Enter Alphabets Only";
            }
        }

        if (!fields["email"]) {
            formIsValid = false;
            errors["email"] = "*Please Enter Your Email Address";
        }

        if (typeof fields["email"] !== "undefined") {
            //regular expression for email validation
            var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
            if (!pattern.test(fields["email"])) {
                formIsValid = false;
                errors["email"] = "*Please Enter Valid Email Address";
            }
        }

        if (!fields["phone"]) {
            formIsValid = false;
            errors["phone"] = "*Please Enter Your phone Number";
        }

        if (typeof fields["phone"] !== "undefined") {
            if (!fields["phone"].match(/^[0-9]{10}$/)) {
                formIsValid = false;
                errors["phone"] = "*Please Enter Valid phone Number";
            }
        }
        if (!fields["city"]) {
            formIsValid = false;
            errors["city"] = "*Please Enter Your City";
        }
        if (typeof fields["city"] !== "undefined") {
            if (!fields["city"].match(/^[a-zA-Z ]*$/)) {
                formIsValid = false;
                errors["city"] = "*Please Enter Alphabets Only";
            }
        }
        if (!fields["state"]) {
            formIsValid = false;
            errors["state"] = "*Please Enter Your state";
        }
        if (typeof fields["state"] !== "undefined") {
            if (!fields["state"].match(/^[a-zA-Z ]*$/)) {
                formIsValid = false;
                errors["state"] = "*Please Enter Alphabets Only";
            }
        }

        if (!fields["religion"]) {
            formIsValid = false;
            errors["religion"] = "*Please Enter Your religion";
        }
        if (typeof fields["religion"] !== "undefined") {
            if (!fields["religion"].match(/^[a-zA-Z ]*$/)) {
                formIsValid = false;
                errors["religion"] = "*Please Enter Alphabets Only";
            }
        }

        if (!fields["country"]) {
            formIsValid = false;
            errors["country"] = "*Please Enter Your Country";
        }
        if (typeof fields["country"] !== "undefined") {
            if (!fields["country"].match(/^[a-zA-Z ]*$/)) {
                formIsValid = false;
                errors["country"] = "*Please Enter Alphabets Only.";
            }
        }
        if (!fields["password"]) {
            formIsValid = false;
            errors["password"] = "*Please Enter Password";
        }

        if (typeof fields["password"] !== "undefined") {
            if (!fields["password"].match(/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&]).*$/)) {
                formIsValid = false;
                errors["password"] = "*password should contain  atleast min. of 8 chars including one upperCase,lowerCase,digit and special character ";
            }
        }
        if (!fields["confirmPassword"]) {
            formIsValid = false;
            errors["confirmPassword"] = "*Please Enter Password";
        }

        if (fields["confirmPassword"] !== fields['password']) {

            formIsValid = false;
            errors["confirmPassword"] = "*password not matched";

        }
        if (!fields["securityAnswer"]) {
            formIsValid = false;
            errors["securityAnswer"] = "*Please Enter Your Answer";
        }

        if (typeof fields["securityAnswer"] !== "undefined") {
            if (!fields["securityAnswer"]) {
                formIsValid = false;
                errors["securityAnswer"] = "*Please Enter Your Answer";
            }
        }
        if (!fields["securityQuestion"]) {
            formIsValid = false;
            errors["securityQuestion"] = "*Please select a security Question";
        }
        if (!fields["gender"]) {
            formIsValid = false;
            errors["gender"] = "*Please select Gender";
        }
        this.setState({
            errors: errors
        });
        return formIsValid;


    }
    cancel = () => {
        var cancel1 = window.confirm("You are redirecting to Login page");
        if (cancel1 == true) {
            this.setState({
                doCancel: true
            })
        }
    }
    render() {
        if (this.state.isSubmitted) {
            console.log(this.props.is_sign_up);
                console.log(this.props.statuscode);
            if(this.props.is_sign_up) {
                console.log(this.props.statuscode)
                // console.log(this.props.statuscode.payload)
                if (this.props.statuscode == 1) {
                    return (   
                        <div>                
                            <div class="alert alert-success alert-dismissable">
                            <Link to ="/" class="close" data-dismiss="alert" aria-label="close" onClick = {() => {window.location.reload()}} >&times;</Link>
                            <strong>Thanks for Creating an Account in Questa...! You need to login with your account</strong>
                            </div>  
                        </div>                
                    )
                }
                
                if (this.props.statuscode == -1) {
                    return (   
                        <div>                
                            <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={()=>{window.location.reload()}}>&times;</a>
                            <strong>Email ID Already Exists</strong>
                            </div>  
                             
                        </div>                
                    )
                }
                if (this.props.statuscode == -3) {
                    return (   
                        <div>                
                            <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={()=>{window.location.reload()}}>&times;</a>
                            <strong>Mobile Number Already Exists</strong>
                            </div>                           
                        </div>                
                    )
                }
            }
        }
        if (this.state.doCancel) {
            return (
                <Redirect to="/"></Redirect>
            )
        }
        return (
            <div id="forms">
                <div class="container">
                    <p class="field">First Name</p><br />
                    <input class="textField" name="first_name" value={this.state.fields.first_name} type="text" onChange={this.handleChange} placeholder="First Name" required></input><br />
                    <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.first_name}</div>
                </div><br />
                <div class="container">
                    <p class="field">Last Name</p><br />
                    <input class="textField" type="text" name="last_name" value={this.state.fields.last_name} onChange={this.handleChange} placeholder="Last Name" required></input>
                    <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.last_name}</div>
                </div><br /><br /><br /><br />
                <div class="container">
               
                    <p class="field">Gender</p><br />
                    <div class="form-group">
                        <select className="textField" name="gender" onChange={this.handleChange}>
                            <option className="field" value="">Select Gender</option>
                            <option className="field" value="Male" >Male</option>
                            <option className="field" value="Female" >Female</option>
                            <option className="field" value="transgender">TransGender</option>
                        </select>
                        <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.gender}</div>
                    </div>
                </div>
                <div class="container">
                    <p class="field">Email Address</p><br />
                    <input class="textField" name="email" type="text" value={this.state.fields.email} onChange={this.handleChange} placeholder="example@email.com" required></input><br />
                    <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.email}</div>
                </div><br />
                <div class="container">
                    <p class="field">Password</p><br />
                    <input class="textField" type="password" name="password" value={this.state.fields.password} onChange={this.handleChange} placeholder="Password" required></input>
                    <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.password}</div>
                </div><br /><br /><br /><br />
                <div class="container">
                    <p class="field">Confirm Password</p><br />
                    <input class="textField" name="confirmPassword" type="password" value={this.state.fields.confirmPassword} onChange={this.handleChange} placeholder="Confirm Password" required></input><br />
                    <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.confirmPassword}</div>
                </div><br />
                <div class="container">
                    <p class="field">phone Number</p><br />
                    <input class="textField" type="text" name="phone" value={this.state.fields.phone} onChange={this.handleChange} placeholder="phone Number" required></input>
                    <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.phone}</div></div><br /><br /><br /><br />
                <div class="container">
                    <p class="field">City</p><br />
                    <input class="textField" name="city" type="text" value={this.state.fields.city} onChange={this.handleChange} placeholder="City" required></input><br />
                    <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.city}</div>
                </div><br />
                <div class="container">
                    <p class="field">State</p><br />
                    <input class="textField" type="text" name="state" value={this.state.fields.state} onChange={this.handleChange} placeholder="State" required></input>
                    <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.state}</div>
                </div><br /><br /><br /><br />
                <div class="container">
                    <p class="field">Country</p><br />
                    <input class="textField" name="country" type="text" value={this.state.fields.country} onChange={this.handleChange} placeholder="Country" required></input><br />
                    <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.country}</div>
                </div><br />
                <div class="container">
                    <p class="field">Religion</p><br />
                    <input class="textField" type="text" name="religion" value={this.state.fields.religion} onChange={this.handleChange} placeholder="Religion" required></input>
                    <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.religion}</div>
                </div><br /><br /><br /><br />
                <div class="container">
                    <p className="field">Security Question</p><br />
                    <div class="form-group">
                        <select className="textField" name="securityQuestion" onChange={this.handleChange}>
                            <option value="">Select Security Question</option>
                            <option className="field" className="field" value="What is your Pet Name?" >What is your Pet Name?</option>
                            <option className="field" value="What is your Childhood SchoolName?" >What is your Childhood SchoolName?</option>
                            <option className="field" value="What is your Favourite Dish?">What is your Favourite Dish?</option>
                        </select>
                        <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.securityQuestion}</div>
                    </div>
                </div><br />
                <div class="container">
                    <p class="field">Security Answer</p><br />
                    <input class="textField" name="securityAnswer" type="text" onChange={this.handleChange} placeholder="Enter Answer" onKeyDown={this.onKeyDown}  required></input><br />
                    <div className="errorMsg" style={{ color: "red" }}>{this.state.errors.securityAnswer}</div>
                </div><br />
                <br /><br /><br /><br />
                <div class="container" style={{ marginLeft: "10%" }}>
                    <button type="button" class="btn btn-danger" onClick={this.cancel}>Cancel</button> <span><button type="button" class="btn btn-success" onClick={this.doSignUp}>Sign Up</button></span>
                </div>
            </div>
        );
    }
}
function mapStateToProps(state) {
    console.log(state.signUpReducer.statuscode)
    return {
        is_sign_up: state.signUpReducer.is_sign_up,
        statuscode: state.signUpReducer.statuscode
    };

}

function mapDispatchToProps(dispatch) {
    return {
        signUpActions: bindActionCreators(signUpActions, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
