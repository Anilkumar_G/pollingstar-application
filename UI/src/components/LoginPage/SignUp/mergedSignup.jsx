import React, { Component } from 'react';
import SignBoard from './signBoard';
import SignUp from './signUp';
import Header from '../Login/Header';

class mergedSignUp extends Component {
    render() { 
        return ( 
            <div>
                <Header />
                <div className = "row">
                    <div className = "col-6">
                        <SignBoard /> 
                    </div>
                    <div className = "col-6">
                        <SignUp /> 
                    </div>
                </div>
            </div>
         );
    }
}
 
export default mergedSignUp;