import React, { Component } from 'react';
import '../../../Assets/css/loginBoard.css';

class SignBoard extends Component {
    render() {
        return (
            <div className = "pollBoard">
                <h1 id = "poll">Welcome to Online Polls</h1>
                <p id = "tag">See why everyone is switching to PollingStar.com.</p>
                <p><span className = "checkmark"><i class="far fa-check-circle"></i></span>Easy to Conduct poll Surveys</p>
                <p><span className = "checkmark"><i class="far fa-check-circle"></i></span>Real time analytics</p>
                <p><span className = "checkmark"><i class="far fa-check-circle"></i></span>And of course, our fantastic, super-helpful customer Service.</p>
            </div>
        );
    }
}

export default SignBoard;