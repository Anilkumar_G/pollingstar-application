import React, { Component } from 'react';
import '../../../Assets/css/LoginPage.css';
import '../../../Assets/css/forgot.css';
import { Link, Redirect } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as authActions from '../../../store/actions/authActions';
import Header from '../Login/Header';

class securityQuestion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            securityQuestion: '',
            securityAnswer: '',
            isSubmitted: false,
            fields: {},
            errors: {},
            precount : 0,
            email: ""

        }
        this.handleChange = this.handleChange.bind(this);
        this.dosecurity = this.dosecurity.bind(this);
    }


    handleChange = e => {
        console.log("handlechange method in login.js");
        this.setState({
            [e.target.name]: e.target.value,
        
        });
    

        e.preventDefault();
    }


    /*
      form validation 
    */
    // validateForm() {

    //     let fields = this.state.fields;
    //     let errors = {};
    //     let formIsValid = true;
    //     if (typeof fields["email"] !== "undefined") {
    //         //regular expression for email validation
    //         var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    //         if (!pattern.test(fields["email"])) {
    //             formIsValid = false;
    //             errors["email"] = "*Please Enter Valid Email Address";
    //         }
    //     }
    //     this.setState({
    //         errors: errors
    //     });
    //     return formIsValid;
    // }

    dosecurity = e => {
    console.log("dosecurity method is called");
        this.props.authActions.SecurityQuestionCheck({
            email: this.state.email,
            securityQuestion: this.state.securityQuestion,
            securityAnswer: this.state.securityAnswer
        })

        this.setState({ 
            isSubmitted: true
         })
    }


    render() {
        if(this.state.isSubmitted){
            console.log(this.props.statusCode)
            
            if(this.state.email==''){
                return(
                    <div class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={()=>{window.location.reload()}}>&times;</a>
                    <strong>Please Enter Valid Email ID</strong>
                    </div> 
                ) 
            }
            else if (this.props.statusCode == -2 ) {
                return(
                    <div class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={()=>{window.location.reload()}}>&times;</a>
                    <strong>Oops!</strong>No User Found With this Email ID
                    </div> 
                )    
            }
            if(this.state.securityQuestion==''){
                return(
                    <div class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={()=>{window.location.reload()}}>&times;</a>
                    <strong>Please Select Security Question</strong>
                    </div> 
                ) 
            }
            if(this.state.securityAnswer==''){
                return(
                    <div class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={()=>{window.location.reload()}}>&times;</a>
                    <strong>Please Enter Valid Security Answer</strong>
                    </div> 
                ) 
            }
            if (this.props.data.response) {
                console.log("render method is called with data");
               
                if (this.props.data.statuscode == 1) {
                   
                    return <Redirect to='/forgotpassword' />

                }
                else {
                  
                    if (this.props.count >= 3) {
                        this.setState({
                            count: 0,
                            precount : 0  
                        })
                        window.location.reload();
                        return <Redirect to='/' />
                    }
                    else{
                        if(this.props.count > this.state.precount){
                         this.setState({precount : this.props.count})
                           alert("Wrong Attempts:" + this.props.count);
                            // return (   
                            //     <div>                
                            //         <div class="alert alert-danger alert-dismissable">
                            //         <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={()=>{window.location.reload()}}>&times;</a>
                            //         <strong>Wrong Attempts:{this.props.count}</strong>
                            //         </div>                           
                            //     </div>                
                            // )
                        }
                    }
                }
            }
           
        }

        return (
            <div>
                <Header />
            <div>
                <div className="container" style={{marginTop:"3%",width:"500px"}}>
                    <p className="field">Email Address</p><br />
                    <input className="textField" name="email" type="text" value={this.state.fields.email} onChange={this.handleChange} placeholder="Email Address" required></input><br /><br />
                    <div className="errorMsg" style={{ color: "white" }}>{this.state.errors.email}</div>
                </div><br />
                <div className="container" style={{width : "500px"}}>
                    <div className="">
                        <select className="questions" name="securityQuestion" value={this.state.securityQuestion} onChange={this.handleChange}>security questions
                            <option>security questions</option>
                            <option value="What is your Pet Name?">What is your Pet Name?</option>
                            <option value="What is your Childhood SchoolName?">What is your Childhood SchoolName?</option>
                            <option value="What is your Favourite Dish?">What is your Favourite Dish?</option>
                        </select>
                    </div>
                    <p className="field">Answer</p><br />
                    <input className="textField" name="securityAnswer" value={this.state.securityAnswer} type="text" onChange={this.handleChange} placeholder="answer?" required></input><br /><br />

                </div>
                <div className="container" style={{ marginLeft: "31%" }}>
                    {/* <label><Link className="link" to="/forgotpassword">Forgot Password</Link></label><br /> */}
                    <input type="button" value="Submit" onClick={this.dosecurity} className="login1" style={{width:"15%",marginLeft:"12%"}}></input>

                </div>

            </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    console.log(state.authReducer.statuscode);
    return {
        data: state.authReducer.data,
        count: state.authReducer.count,
        isLoggedIn: state.authReducer.is_logged_in,
        statusCode: state.authReducer.statuscode
    };

}


function mapDispatchToProps(dispatch) {
    return {
        authActions: bindActionCreators(authActions, dispatch),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(securityQuestion);