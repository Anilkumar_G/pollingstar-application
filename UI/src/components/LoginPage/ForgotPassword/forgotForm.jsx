import React, { Component } from 'react';
import '../../../Assets/css/forgot.css'
import '../../../Assets/css/LoginPage.css';
import { Link, Redirect } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as authActions from '../../../store/actions/authActions';
import Header from '../Login/Header';

class ForgotForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fields: {},
            errors: {},
            isSubmitted: false,
        }


        this.handleChange = this.handleChange.bind(this);
        this.doforgetUpdatePassword = this.doforgetUpdatePassword.bind(this);
    }

    componentWillMount() {
        this.setState({ email: this.props.data.response});
        console.log("triger the data in securityQuestion");
    }

    // handleChange = (e) => {
    //     this.setState({
    //         [e.target.name]: e.target.value
    //     });
    //     e.preventDefault();
    // }
    handleChange = (event) => {
        let fields = this.state.fields;
        fields[event.target.name] = event.target.value;
        this.setState({
            fields
        });

    }


    doforgetUpdatePassword = (e) => {
        // if (this.state.newpassword === this.state.confirmpassword) {
        //     this.setState({
        //         password: this.state.newpassword
        //     })

        //     this.props.authActions.FORGET_UPDATE_PASSWORD({
        //         email: this.state.email,
        //         password: this.state.newpassword
        //     })
        // }
        e.preventDefault()
        if (this.validateForm()) {
            let fields = {};
            fields["newpassword"] = "";
            fields["confirmpassword"] = "";
            this.props.authActions.FORGET_UPDATE_PASSWORD({
                // email: this.state.fields.email,
                password: this.state.fields.newpassword,
                confirmpassword:this.state.fields.confirmpassword
            })
            this.setState({
                isSubmitted:true
            })
        }
        else {
            return(
                <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={()=>{window.location.reload()}}>&times;</a>
                <strong>Updation Failed..Password Mismatched</strong>
                </div>   
            )    
            /*
               have to implement for negative case
               mismatch with the new and confirm password
            */
        }
    }
    validateForm() {

        let fields = this.state.fields;
        let errors = {};
        var formValid = true;

        if (!fields["newpassword"]) {
            formValid = false;
            errors["newpassword"] = "*Please Enter Password";
        }

        if (typeof fields["newpassword"] !== "undefined") {
            if (!fields["newpassword"].match(/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&]).*$/)) {
                formValid = false;
                errors["newpassword"] = "*Password should contain atleast one capital letter,small letter,special character and digits";
            }
        }
        if (!fields["confirmpassword"]) {
            formValid = false;
            errors["newpassword"] = "*Please Enter Password";
        }

        if (fields["confirmpassword"] !== fields['newpassword']) {           
                formValid = false;
                errors["confirmpassword"] = "*password not matched";
            
        }
        this.setState({
            errors: errors
        });
        return formValid;
    }


    render() {

        if (this.props.forgetUpadatePassword.statuscode == 1) {
            // alert("new password is updated");
            return(
                <div class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={()=>{window.location.reload()}}>&times;</a>
                <strong>Succesfully Upadated New Password..</strong>
                </div>   
            )    
        }

        if (this.props.forgetUpadatePassword.statuscode == -1) {
        //   alert("update password faild try again");
            return(
                <div class="alert alert-danger alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={()=>{window.location.reload()}}>&times;</a>
                <strong>Updation Failed..Please Try Again</strong>
                </div>   
            )    
        }
        
        return (
            <div>
             <Header/>
            <div className="forgot-password">
                <div className="row">

                </div>
                <div id="forms" >
                    {/* <form method = "post" > */}
                    <h1 className="field" style={{ fontSize: "170%" }}>Reset your Password</h1>
                    <div className="container">
                        <p className="field">NewPassword</p><br />
                        <input className="textField" name="newpassword" type="text" onChange={this.handleChange} placeholder="Enter New Password" required></input>
                        <div className="errorMsg"  style={{ color: "red",marginLeft:"-6%" }}>{this.state.errors.newpassword}</div>

                    </div><br />
                    <div className="container">
                        <p className="field">ConfirmPassword</p><br />
                        <input className="textField" type="password" name="confirmpassword" onChange={this.handleChange} placeholder="Confirm Password" required></input>
                        <div className="errorMsg" style={{ color: "red",marginLeft:"-6%" }}>{this.state.errors.confirmpassword}</div>

                    </div><br /><br /><br /><br /><br />

                    <div className="container" style={{ marginLeft: "10%" }}>
                        <input type="button" value="submit" onClick={this.doforgetUpdatePassword} className="login" ></input>
                        {/* <button class="login" onClick = {this.dologin}>LOGIN</button> */}
                    </div>
                    {/* </form> */}
                </div>
            </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
 
    return {
        data: state.authReducer.data,
        forgetUpadatePassword: state.authReducer.forgetUpadatePassword

    };

}

function mapDispatchToProps(dispatch) {
    return {
        authActions: bindActionCreators(authActions, dispatch),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(ForgotForm);