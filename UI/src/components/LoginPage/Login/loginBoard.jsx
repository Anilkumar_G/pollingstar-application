import React, { Component } from 'react';
import '../../../Assets/css/loginBoard.css';

class loginQuotes extends Component {
    render() {
        return (
            <div className = "pollBoard">
                <h1 id = "poll">Online Polls</h1>
                <p id = "tag">Get instant feedback with online polls.</p>
                <p><span className = "checkmark"><i class="far fa-check-circle"></i></span>Ask unlimited Questions</p>
                <p><span className = "checkmark"><i class="far fa-check-circle"></i></span>Post your poll anywhere online</p>
                <p><span className = "checkmark"><i class="far fa-check-circle"></i></span>Get notified in real as results start rolling in</p>
            </div>
        );
    }
}

export default loginQuotes;