//npm Dependencies
import React, { Component } from 'react';
import { Redirect, Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import $ from 'jquery';
import { Alert } from 'reactstrap';

//Styling Dependencies
import '../../../Assets/css/LoginPage.css';

//Store Dependencies
import * as authActions from '../../../store/actions/authActions';


// $(function () {
//     $('[data-toggle="tooltip"]').tooltip()
//   })
/**
 * Purpose of this Component is to provide
 * the login form to the user
 */
class Loginpage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            isSubmitted: false,
            emailError: false,
            passwordError: false,
            invalidError : false,
            blockedError : false
        }
        var userError = false;

        localStorage.removeItem('jwt-token');
        localStorage.removeItem('user_id');
        localStorage.removeItem('email');
        localStorage.removeItem('first_name');
        localStorage.removeItem('last_name');
        localStorage.removeItem('city');
        localStorage.removeItem('state');
        localStorage.removeItem('country');
        localStorage.removeItem('religion');
        localStorage.removeItem('phone');

        this.handleChange = this.handleChange.bind(this);
        this.doLogin = this.doLogin.bind(this);
    }


    handleChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
        console.log(this.state.email)
        e.preventDefault();
    }
    onKeyDown = (event) => {
        // 'keypress' event misbehaves on mobile so we track 'Enter' key via 'keydown' event
        if (event.key === 'Enter') {
            event.preventDefault();
            event.stopPropagation();
            this.doLogin();
        }
    }
    toggle() {
        // this.userError = false;
        // this.invalidError = false;
        // this.blockedError = false;
        this.setState({
            emailError: false,
            passwordError: false,
            invalidError : false,
            userError :  false,
            blockedError : false
        })
    }

    errorFun(){
        this.setState({
            userError : true
        })
    }
    doLogin = e => {
        if (this.state.email == '') {
            this.setState({
                emailError: true
            })
        } else if (this.state.password == '') {
            this.setState({
                passwordError: true
            })
        }
        else {
            this.props.authActions.doLogInUser({
                email: this.state.email,
                password: this.state.password
            });
            this.setState({
                isSubmitted: true
            });
        }
    }

    render() {
        if (this.state.isSubmitted) {

            if (this.props.isLoggedIn === true) {
                window.location.reload();
                return <Redirect to='/home' />
            }

            else {

                if (this.props.statusCode == -1) {
                    // alert(" No Such User Found ")
                    return (
                        <div>                
                            <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" onClick={()=>{window.location.reload()}}>&times;</a>
                            <strong>No User Found</strong>
                            </div>   
                        </div>   
                    )
                }

                else if (this.props.statusCode == -2) {
                    this.invalidError = true
                }

                else if (this.props.statusCode == -3) {
                    this.blockedError = true
                }

            }
        }
        return (
            <div>
                {
                    this.blockedError ?
                        <Alert>
                            <a class="close" data-dismiss="alert" aria-label="close" onClick={this.toggle.bind(this)}>&times;</a>
                            <strong>Oops!</strong>Your account is blocked. Try After 10 minutes.
                        </Alert>
                        : <div></div>
                }
                {
                    this.invalidError ?
                        <Alert>
                            <a class="close" data-dismiss="alert" aria-label="close" onClick={this.toggle.bind(this)}>&times;</a>
                            <strong>Oops!</strong>Invalid EmailId/Password.Please Enter Valid Credentials
                    </Alert>
                        : <div></div>
                }
                {
                    this.userError ?
                    <Alert>
                        <a class="close" data-dismiss="alert" color="danger" aria-label="close" onClick={this.toggle.bind(this)}>&times;</a>
                        <strong>Oops!</strong>No such user found
                </Alert>
                    : null
                }
                {
                    this.state.emailError ?
                    <Alert>
                        <a class="close" data-dismiss="alert" aria-label="close" onClick={this.toggle.bind(this)}>&times;</a>
                        <strong>Oops!</strong>Please Enter Valid Email Address
                </Alert>
                    : <div></div>
                }
                {
                    this.state.passwordError ?
                    <Alert>
                        <a class="close" data-dismiss="alert" aria-label="close" onClick={this.toggle.bind(this)}>&times;</a>
                        <strong>Oops!</strong>Please Enter Your password
                </Alert>
                    : <div></div>
                }
                <div id="forms">
                    <div className="container">
                        {/* <p class="field" style={{color:"white"}}>Wanna Add/Answer Questions?SignIn/SignUp</p> */}
                    </div>
                    <div class="container">
                        <p class="field">Email Address</p><br />
                        <input class="textField" name="email" type="text" data-toggle="tooltip" data-placement="top" onChange={this.handleChange} placeholder="Email Address" required></input><br /><br />
                    </div><br />
                    <div class="container">
                        <p class="field">Password</p><br />
                        <input class="textField" type="password" name="password" onChange={this.handleChange} placeholder="Password" onKeyDown={this.onKeyDown} required></input>
                    </div><br /><br /><br /><br /><br />
                    <div class="container">
                        <label><Link className="link" to="/securityQuestion">Forgot Password</Link></label><br />
                        <label style={{ color: "white" }}>New User?Create an Account    <Link to="/signup" className="link">Sign Up</Link></label>
                    </div><br /><br /><br />
                    <div class="container" style={{ marginLeft: "10%" }}>
                        <input type="submit" value="Login" onClick={this.doLogin} className="login" ></input>
                        {/* <button class="login" onClick = {this.dologin}>LOGIN</button> */}
                    </div>

                    {/* </form> */}
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    console.log(state.authReducer.statuscode);
    return {
        isLoggedIn: state.authReducer.is_logged_in,
        statusCode: state.authReducer.statuscode
    };

}

function mapDispatchToProps(dispatch) {
    return {
        authActions: bindActionCreators(authActions, dispatch),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Loginpage);