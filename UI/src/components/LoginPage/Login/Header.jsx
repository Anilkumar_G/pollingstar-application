import React, { Component } from 'react';
import '../../../Assets/css/header.css';
import logo from '../../../Assets/images/questa.png';
import {Link } from 'react-router-dom'

class Header extends Component {

    render() {
        return (
            <div>
                <nav className="navbar navbar-expand" id="headerStyle"> 
                    <div className="container-fluid" >
                        <div className="navbar-header">
                            <img src={logo} style={{width:"20%",height:"10%",marginTop:"-1%"}}/>
                            <Link to="navbar-brand" to="/" id="logo" style={{marginLeft:"-5%",marginTop:"1%"}}>Questa</Link>
                        </div>
                        <ul className="navbar-nav navbar-right"  >
                            <li className="nav-item"><Link className="nav-link" to="/howItWorks">How It works</Link></li>
                            <li className="nav-item" ><Link className="nav-link" to="/"> Sign In</Link></li>
                            <li className="nav-item"><Link className="nav-link" to="/signup"> Sign Up</Link></li>
                            <li className="nav-item"><a className="nav-link" href="#"> Contact</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        );
    }
}

export default Header;