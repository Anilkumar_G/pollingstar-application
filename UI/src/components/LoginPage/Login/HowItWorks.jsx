//npm Dependencies
import React, { Component } from 'react';
import {Link} from 'react-router-dom';

//Styling Dependencies
import '../../../Assets/css/HowItWorks.css';

class HowItWorks extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        return (
            <div>
                <div>
                    <div className="container" style={{borderLeft:"4px solid #00ba9d",backgroundColor:"rgba(0,0,0,0.3)"}}>
                    <h1 className="headhow">How It Works</h1><br/>
                        <p className="desc">
                            An online poll is a survey in which participants communicate responses via the Internet,
                            typically by completing a questionnaire in a web page. Online polls may allow anyone to
                            participate.
                        </p>
                        <p className="desc">
                            The Main aspects of our Application and How it Works is..<br/><br/>
                            <p><span className="checkmark"><i class="far fa-check-circle"></i></span>User has to Sign Up in Questa.</p>
                            <p><span className="checkmark"><i class="far fa-check-circle"></i></span>User has to login with his Unique Mail ID.</p>
                            <p><span className="checkmark"><i class="far fa-check-circle"></i></span>In HomePage..User Can able to see all the Posted by Other Users.</p>
                            <p><span className="checkmark"><i class="far fa-check-circle"></i></span>In AddQuestion Page..User Can asks a Question while Asking the Question he/she has to select the Category and also he/she Can able to Post the Answer For it. </p>
                            <p><span className="checkmark"><i class="far fa-check-circle"></i></span>In MyQuestions Page..User Can able to See all the Questions Which were Posted by Themselves.</p>
                            <p><span className="checkmark"><i class="far fa-check-circle"></i></span>In RecentQuestions Page..User Can able to See all the Questions According to the TimeStamp.</p>
                            <p><span className="checkmark"><i class="far fa-check-circle"></i></span>In TrendingQuestions Page..User Can able to See all the Questions Based On the Top Most Votes of Answers for the Respective Questions.</p>
                            <p><span className="checkmark"><i class="far fa-check-circle"></i></span>In CategoryQuestions Page..User Can able to See all the Questions According to the Selected Categories.</p>
                        </p>
                        <center><Link className="login" to="/">Back</Link></center>
                    </div>
                </div>
            </div>
        );
    }
}

export default HowItWorks;