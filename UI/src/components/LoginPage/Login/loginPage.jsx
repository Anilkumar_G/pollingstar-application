import React, { Component } from 'react';
import Header from '../Login/Header';
import Board from '../Login/loginBoard';
import Login from '../Login/login';

class LoginPage extends Component {
    render() { 
        return ( 
            <div>
                <Header />
                <div className = "row">
                    <div className = "col-5">
                        <Board /> 
                    </div>
                    <div className = "col-7">
                        <Login /> 
                    </div>
                </div>
            </div>
         );
    }
}
 
export default LoginPage;