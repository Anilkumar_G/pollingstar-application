//npm Dependencies
import  React                       from 'react';
import { Switch, Route }            from 'react-router-dom';
import { BrowserRouter }            from 'react-router-dom';

//Component Dependencies
import LoginPage                    from './components/LoginPage/Login/loginPage';
import SignIn                       from './components/LoginPage/SignUp/mergedSignup';
import ForgetForm                   from './components/LoginPage/ForgotPassword/forgotForm';
import securityQuestion             from './components/LoginPage/ForgotPassword/securityQuestion';
import UserProfile                  from './components/User Details/userDetails';
import DashBoard                    from './components/MainPage/DashBoard';
import addQuestion                  from './components/MainPage/addQuestion';
import recentQuestions              from './components/MainPage/recentQuestions';
import trendingQuestions            from './components/MainPage/trendingQuestions';
import categoryQuestions            from './components/MainPage/categoryQuestions';
import searchQuestion               from './components/MainPage/searchQuestion';
// import UserQuestions             from './components/MainPage/UserQuestions';
import MyQuestions                  from './components/MainPage/myQuestions'
import HowItWorks                   from './components/LoginPage/Login/HowItWorks';
import UpdateDetails                from './components/User Details/userDetailsUpdate';
import analatics                    from './components/MainPage/analatics';


/**
 * This Component is to Provide the routes to link
 * the pages
 */
const Routes = () => (
    <BrowserRouter>
        <Switch>
            <div>
                <Route path='/'                                     component={LoginPage} exact></Route>
                <Route path="/howItWorks"                           component={HowItWorks} exact></Route>
                <Route path='/signup'                               component={SignIn} exact></Route>
                <Route path='/home'                                 component={DashBoard} exact></Route>
                <Route path='/home/details'                         component={UserProfile} exact></Route>
                <Route path='/forgotpassword'                       component={ForgetForm} exact></Route>
                <Route path='/securityQuestion'                     component={securityQuestion} exact></Route>
                <Route path= '/addquestion'                         component={addQuestion} exact></Route>
                <Route path ="/recent"                              component = {recentQuestions} exact></Route>
                <Route path ="/trending"                            component = {trendingQuestions} exact></Route>
                <Route path ="/category"                            component = {categoryQuestions} exact></Route>
                <Route path ="/searchQuestion/:search"              component = {searchQuestion} exact></Route>
                <Route path = "/myQuestions"                        component={MyQuestions} exact></Route>
                <Route path ="/updateDetails"                       component={UpdateDetails} exact></Route>
                <Route path = "/analatics/:id"                      component={analatics} exact></Route>
            </div>
        </Switch>
    </BrowserRouter>
);

export default Routes;